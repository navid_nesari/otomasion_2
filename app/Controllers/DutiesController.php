<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Constant;
use App\Helpers\Format\Date;
use App\Http\Controllers\Controller;
use App\Models\Duty;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule ;

class DutiesController extends BaseController
{
    public function all(Project $project)
    {
        $duties = $project->duties()->latest()->paginate();
        return view('admin.duties.all', compact('project', 'duties'));
    }

    public function secondAll(Project $project)
    {
        $duties = $project->duties()->latest()->paginate();
        return view('admin.duties.second-all', compact('project', 'duties'));
    }

    public function create(Project $project)
    {
        $activityStatuses = Constant::getDutyStatusesViewer();
        $admins = $project->admins()->get();
        $projectAdmins = [];
        foreach($admins as $admin) {
            $projectAdmins[]= [
                'id' => $admin->id,
                'title' => $admin->webPresent()->fullName(),
                'image' => $admin->webPresent()->image(),
                'email' => $admin->email
            ];
        }
        return view('admin.duties.create', compact('project', 'activityStatuses', 'projectAdmins'));
    }

    public function store(Request $request, Project $project)
    {
        $this->validateStoreform($request);
        $data = $this->getData($request, $project);
        $duty = Duty::create($data);
        if ($duty instanceof Duty) {
            $duty->admins()->sync($request->input('admins'));
            return redirect()->route('admin.project-duties.all', $project)->with("store", "عملیات شما با موفقیت انجام شد ");
        }
        return redirect()->back();
    }

    public function edit(Project $project, Duty $duty)
    {
        $activityStatuses = Constant::getDutyStatusesViewer();
        $dutyAdmins = $duty->admins()->get();
        $dutyAdminsList = [];
        foreach($dutyAdmins as $dutyAdmin) {
            $dutyAdminsList[]=[
                'id' => $dutyAdmin->id,
            ];
        }

        $admins = $project->admins()->get();
        $projectAdmins = [];
        foreach($admins as $admin) {
            $projectAdmins[]= [
                'id' => $admin->id,
                'title' => $admin->webPresent()->fullName(),
                'image' => $admin->webPresent()->image(),
                'email' => $admin->email
            ];
        }
        return view('admin.duties.edit', compact('project', 'duty', 'activityStatuses', 'projectAdmins', 'dutyAdminsList'));
    }

    public function update(Request $request, Project $project, Duty $duty)
    {
        $this->validateupdateform($request);
        $data = $this->getData($request, $project);
        $result = $duty->update($data);
        if($result) {
            $duty->admins()->sync($request->input('admins'));
            return redirect()->route('admin.project-duties.all', $project)->with("store", "ویرایش شما با موفقیت انجام شد  ");
        }
        return redirect()->back();
    }

    public function logicalDeletion(Project $project, Duty $duty)
    {
        $duty->admins()->detach($duty->admins);
        $duty->delete();
        return redirect()->back()->with("delete", 'مدیر شما با موفقیت حذف شد ');
    }

    public function usersDuty(Project $project, Duty $duty)
    {
        dd($duty->admins->all());
        $admins = $duty->admins()->get();
        return view('admin.duties.users-duty', compact('project', 'duty', 'admins'));
    }

    private function validateStoreform($request)
    {
        $request->validate([
            'title' => ['required'],
            'weight' => ['required'],
            'start_date' => ['required'],
            'end_date' => ['required'],
            'description' => ['required'],
            'status' => ['required', Rule::in(
                Constant::TO_DO,
                Constant::IN_PROGRESS,
                Constant::COMPLETED,
                Constant::AWAITING_CONFIRMATION,
                Constant::CONFIRMED,
            )],
            'image' => ['required', 'mimes:jpg,png,bmp,jpeg']
        ],[
            "*.required" => "وارد کردن این فیلد الزامیست ",
            "status.in" => "فیلد مورد نظر معتبر نمی باشد",
            "image.mimes" => "فرمت تصویر مربوطه معتبر نمی باشد",
        ]);
    }

    private function validateupdateform($request)
    {
        $request->validate([
            'title' => ['required'],
            'weight' => ['required'],
            'start_date' => ['required'],
            'end_date' => ['required'],
            'description' => ['required'],
            'status' => ['required', Rule::in(
                Constant::TO_DO,
                Constant::IN_PROGRESS,
                Constant::COMPLETED,
                Constant::AWAITING_CONFIRMATION,
                Constant::CONFIRMED,
            )],
            'image' => ['mimes:jpg,png,bmp,jpeg']
        ], [
            "*.required" => "وارد کردن این فیلد الزامیست ",
            "status.in" => "فیلد مورد نظر معتبر نمی باشد",
            "image.mimes" => "فرمت تصویر مربوطه معتبر نمی باشد",
        ]);
    }

    private function getData(Request $request, $project): array
    {
        $data = [
            'project_id' => $project->id,
            'title' => $request->input('title'),
            'weight' => $request->input('weight'),
            'start_date' => Date::toCarbonDateFormat($request->input('start_date')),
            'end_date' => Date::toCarbonDateFormat($request->input('end_date')),
            'description' => $request->input('description'),
            'status' => $request->input('status'),
        ];
        if ($request->hasFile('image')) {
            $data['image'] = $this->uploadFile($request->file('image'), Constant::DUTIES_IMAGE_PATH);
        }
        return $data;
    }
}
