<?php


namespace App\Constants;
class Constant
{

    //LETTER
    const LETTER = 'letter';
    //test
    const FREE = "free";
    const MONY = "mony";

    //correct
    const CORRECT = "correct";
    const FALSE = "false";
    //questions
    const DESCRIPTIVE = "descriptive";
    const TEST = "test";

    //removable
    const REMOVABLE = "removable";
    const UNABLETODELETE = "unable_to_delete";

    //like
    const LIKE = "like";
    const DIS_LIKE = "dis_like";
    const HAS_IT = "has_it";
    const HAS_NO = "has_no";


    // Global Constant
    const UN_DEFINED = "un-defined";
    const ACTIVE = 'active';
    const IN_ACTIVE = 'in-active';
    const MALE = 'male';
    const FEMALE = 'female';
    const REJECTED = 'rejected';
    const ACCEPTED = 'accepted';
    const DRAFT = 'draft';
    const PUBLISHED = 'published';
    const SCHEDULED = 'scheduled';

    const DELETED = 'deleted';
    const UN_DELETED = 'un-deleted';

    const TO_DO = 'to-do';
    const IN_PROGRESS = 'in-progress';
    const COMPLETED = 'completed';
    const AWAITING_CONFIRMATION = 'awaiting-confirmation';
    const CONFIRMED = 'confirmed';

    const SINGLE = 'single';
    const MARRIED = 'married';
    const ENGAGEMENT = 'engagement';
    const DIVORCED = 'divorced';
    const DECEASED_SPOUSE = 'deceased-spouse';

    const INCLUDED = 'included';
    const IN_MILITARY_SERVICE = 'in-military-service';
    const EXEMPT = 'exempt';

    const PERSONAL_HOUSE = 'personal-house';
    const LIVING_WITH_PARENTS = 'living-with-parents';
    const TENANT = 'tenant';

    const I_HAVE = 'i-have';
    const I_HAVENT = 'i-havent';
    const RETIRED = 'retired';

    const SEARCHABLE = true;

    // categories entities
    const SIGNS = "signs";


    // File Constant
    const IMAGE = 'image';
    const SOUND = 'sound';
    const VIDEO = 'video';

    const WHITE_MIME_TYPE_LIST = [
        'image/jpeg', 'image/png', 'image/jpg', 'audio/mpeg', 'video/mp4',
        'application/zip', 'application/vnd.ms-powerpoint',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        'application/vnd.rar', 'text/plain', 'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    ];


    const PRODUCT_AVATAR_SIZE = [
        '100' => ['w' => '100', 'h' => '100'],
        '300' => ['w' => '300', 'h' => '300']
    ];
    const USER_AVATAR_SIZE = [
        '100' => ['w' => '200', 'h' => '200'],
    ];


    // admins
    const ADMINS_IMAGE_PATH = 'uploads' . DIRECTORY_SEPARATOR . 'admins' . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . '';

    // signs
    const SIGNS_AVATAR_PATH = 'uploads' . DIRECTORY_SEPARATOR . 'signs' . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . '';


    // users
    const USERS_AVATAR_PATH = 'uploads' . DIRECTORY_SEPARATOR . 'users' . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . '';

    // tests
    const TESTS_IMAGE_PATH = 'uploads' . DIRECTORY_SEPARATOR . 'tests' . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . '';

    // questions
    const QUESTIONS_SOUND_PATH = 'uploads' . DIRECTORY_SEPARATOR . 'questions' . DIRECTORY_SEPARATOR . 'sound' . DIRECTORY_SEPARATOR . '';
    const QUESTIONS_IMAGE_PATH = 'uploads' . DIRECTORY_SEPARATOR . 'questions' . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . '';

    // categories
    const CATEGORIES_IMAGE_PATH = 'uploads' . DIRECTORY_SEPARATOR . 'categories' . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . '';

    // projects
    const PROJECTS_IMAGE_PATH = 'uploads' . DIRECTORY_SEPARATOR . 'projects' . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . '';


    public static function getStatuses($status = null)
    {
        if (is_null($status)) {
            return [
                Constant::ACTIVE => 'فعال',
                Constant::IN_ACTIVE => ' غیر فعال',
            ];
        }
        if (in_array($status, array_keys(self::getStatuses()))) {
            return self::getStatuses()[$status];
        }
        return Constant::UN_DEFINED;
    }

    public static function getStatusesViewer()
    {
        $activeStatuses = self::getStatuses();
        $activeStatusesViewer = [];
        foreach ($activeStatuses as $key => $value) {
            $activeStatusesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $activeStatusesViewer;
    }


    //tests
    public static function getTestTypes($status = null)
    {
        if (is_null($status)) {
            return [
                Constant::MONY => 'پولی',
                Constant::FREE => ' رایگان ',
            ];
        }
        if (in_array($status, array_keys(self::getTestTypes()))) {
            return self::getTestTypes()[$status];
        }
        return Constant::UN_DEFINED;
    }

    public static function getTestTypesViewer()
    {
        $getTestTypes = self::getTestTypes();
        $getTestTypesViewer = [];
        foreach ($getTestTypes as $key => $value) {
            $getTestTypesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $getTestTypesViewer;
    }

    // category entities
    public static function getCategoryEntities($status = null)
    {
        $categoryEntities = [

            self::SIGNS => 'تابلو راهنمایی',

        ];
        if (is_null($status)) {
            return $categoryEntities;
        }
        if (in_array($status, array_keys($categoryEntities))) {
            return $categoryEntities[$status];
        }
    }

    public static function getCategoryEntitiesView(): array
    {
        $categoryEntities = self::getCategoryEntities();
        $categoryEntitiesView = [];
        foreach ($categoryEntities as $key => $value) {
            $categoryEntitiesView[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $categoryEntitiesView;
    }

    // project
    public static function getProjectStatuses($status = null)
    {
        if (is_null($status)) {
            return [
                Constant::TO_DO => 'برای انجام',
                Constant::IN_PROGRESS => 'در حال انجام',
                Constant::COMPLETED => 'انجام شده',
            ];
        }
        if (in_array($status, array_keys(self::getProjectStatuses()))) {
            return self::getProjectStatuses()[$status];
        }
        return Constant::UN_DEFINED;
    }

    public static function getProjectStatusesViewer()
    {
        $activeStatuses = self::getProjectStatuses();
        $activeStatusesViewer = [];
        foreach ($activeStatuses as $key => $value) {
            $activeStatusesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $activeStatusesViewer;
    }

    // duty
    public static function getDutyStatuses($status = null)
    {
        if (is_null($status)) {
            return [
                Constant::TO_DO => 'برای انجام',
                Constant::IN_PROGRESS => 'در حال انجام',
                Constant::COMPLETED => 'انجام شده',
                Constant::AWAITING_CONFIRMATION => 'منتظر تایید',
                Constant::CONFIRMED => 'تایید شده',
            ];
        }
        if (in_array($status, array_keys(self::getDutyStatuses()))) {
            return self::getDutyStatuses()[$status];
        }
        return Constant::UN_DEFINED;
    }

    public static function getDutyStatusesViewer()
    {
        $activeStatuses = self::getDutyStatuses();
        $activeStatusesViewer = [];
        foreach ($activeStatuses as $key => $value) {
            $activeStatusesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $activeStatusesViewer;
    }

    // admin's marital
    public static function getAdminMaritalStatuses($status = null)
    {
        if (is_null($status)) {
            return [
                Constant::SINGLE => 'مجرد',
                Constant::MARRIED => 'متاهل',
                Constant::ENGAGEMENT => 'نامزد',
                Constant::DIVORCED => 'جدا شده',
                Constant::DECEASED_SPOUSE => 'همسر فوت شده',
            ];
        }
        if (in_array($status, array_keys(self::getAdminMaritalStatuses()))) {
            return self::getAdminMaritalStatuses()[$status];
        }
        return Constant::UN_DEFINED;
    }

    public static function getAdminMaritalStatusesViewer()
    {
        $activeStatuses = self::getAdminMaritalStatuses();
        $activeStatusesViewer = [];
        foreach ($activeStatuses as $key => $value) {
            $activeStatusesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $activeStatusesViewer;
    }

    // admin's military
    public static function getAdminMilitaryStatuses($status = null)
    {
        if (is_null($status)) {
            return [
                Constant::INCLUDED => 'مشمول',
                Constant::IN_MILITARY_SERVICE => 'در حال خدمت',
                Constant::EXEMPT => 'معاف',
            ];
        }
        if (in_array($status, array_keys(self::getAdminMilitaryStatuses()))) {
            return self::getAdminMilitaryStatuses()[$status];
        }
        return Constant::UN_DEFINED;
    }

    public static function getAdminMilitaryStatusesViewer()
    {
        $activeStatuses = self::getAdminMilitaryStatuses();
        $activeStatusesViewer = [];
        foreach ($activeStatuses as $key => $value) {
            $activeStatusesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $activeStatusesViewer;
    }

    // admin's housing situation
    public static function getAdminHousingStatuses($status = null)
    {
        if (is_null($status)) {
            return [
                Constant::PERSONAL_HOUSE => 'شخصی',
                Constant::LIVING_WITH_PARENTS => 'زندگی با والدین',
                Constant::TENANT => 'اجاره ای',
            ];
        }
        if (in_array($status, array_keys(self::getAdminHousingStatuses()))) {
            return self::getAdminHousingStatuses()[$status];
        }
        return Constant::UN_DEFINED;
    }

    public static function getAdminHousingStatusesViewer()
    {
        $activeStatuses = self::getAdminHousingStatuses();
        $activeStatusesViewer = [];
        foreach ($activeStatuses as $key => $value) {
            $activeStatusesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $activeStatusesViewer;
    }

    // admins second job
    public static function getAdminJobStatuses($status = null)
    {
        if (is_null($status)) {
            return [
                Constant::I_HAVE => 'دارم',
                Constant::I_HAVENT => 'ندارم',
                Constant::RETIRED => 'بازنشسته',
            ];
        }
        if (in_array($status, array_keys(self::getAdminJobStatuses()))) {
            return self::getAdminJobStatuses()[$status];
        }
        return Constant::UN_DEFINED;
    }

    public static function getAdminJobStatusesViewer()
    {
        $activeStatuses = self::getAdminJobStatuses();
        $activeStatusesViewer = [];
        foreach ($activeStatuses as $key => $value) {
            $activeStatusesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $activeStatusesViewer;
    }

    //request-for-help
    public static function getRequestForHelpStatuses($status = null)
    {
        if (is_null($status)) {
            return [
                Constant::REJECTED => 'رد شده',
                Constant::ACCEPTED => 'تایید شده',
            ];
        }
        if (in_array($status, array_keys(self::getAdminJobStatuses()))) {
            return self::getAdminJobStatuses()[$status];
        }
        return Constant::UN_DEFINED;
    }

    public static function getRequestForHelpStatusesViewer()
    {
        $activeStatuses = self::getRequestForHelpStatuses();
        $activeStatusesViewer = [];
        foreach ($activeStatuses as $key => $value) {
            $activeStatusesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $activeStatusesViewer;
    }

}
