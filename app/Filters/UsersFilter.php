<?php


namespace App\Filters;
use App\Filters\Contracts\QueryFilter;

class UsersFilter extends QueryFilter
{
    public function  full_name ($value = null)
    {
        if(!is_null($value)){
            return $this->builder->where('full_name', 'like', '%'.$value.'%');
        }
        return $this->builder;
    }
    public function email($value = null)
    {
        if(!is_null($value)){
            return $this->builder->where('email', 'like', '%'.$value.'%');
        }
        return $this->builder;
    }


}
