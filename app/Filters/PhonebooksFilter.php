<?php


namespace App\Filters;
use App\Filters\Contracts\QueryFilter;

class PhonebooksFilter extends QueryFilter
{
    public function  full_name($value = null)
    {
        if(!is_null($value)){
            return $this->builder->where('full_name', 'like', '%'.$value.'%');
        }
        return $this->builder;
    }

    public function mobile($value = null)
    {
        if(!is_null($value)){
            return $this->builder->where('mobile', 'like', '%'.$value.'%');
        }
        return $this->builder;
    }

    public function phone($value = null)
    {
        if(!is_null($value)){
            return $this->builder->where('phone', 'like', '%'.$value.'%');
        }
        return $this->builder;
    }

    public function account_number($value = null)
    {
        if(!is_null($value)){
            return $this->builder->where('account_number', 'like', '%'.$value.'%');
        }
        return $this->builder;
    }

}
