<?php


namespace App\Filters;
use App\Filters\Contracts\QueryFilter;
use App\Helpers\Format\Date;

class ProjectsFilter extends QueryFilter
{
    public function title($value = null)
    {
        if(!is_null($value)){
            return $this->builder->where('title', 'like', '%'.$value.'%');
        }
        return $this->builder;
    }

    public function status($value = null)
    {
        if(!is_null($value)){
            return $this->builder->where('status', $value);
        }
        return $this->builder;
    }

    public function start_date($value = null)
    {
        if(!is_null($value)){
            $dateValue = Date::toCarbonDateFormat($value);
            return $this->builder->where('start_date', $dateValue);
        }
        return $this->builder;
    }

    public function end_date($value = null)
    {
        if(!is_null($value)){
            $dateValue = Date::toCarbonDateFormat($value);
            return $this->builder->where('end_date', $dateValue);
        }
        return $this->builder;
    }
}
