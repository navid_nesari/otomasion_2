<?php

namespace App\Http\Requests\Category;

use App\Constants\CategoryConstant;
use App\Constants\GlobalConstant;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Symfony\Component\Console\Input\Input;

class UpdateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    protected function prepareForValidation()
    {
        $this->merge(['entity' => $this->route('entity')]);
    }
   /* public function all($keys = NULL)
    {
        //$request = request()->all();
        $request['entity'] = request('entity');
        return $request;
    }*/

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => ['required', 'string'],
//            'description' => ['required'],




        ];
        if(!is_null(request('parent'))){
            $rules['parent'] =  ['string', 'exists:categories,id'];
        }
        return  $rules;
    }



    public function messages()
    {
        return  [
            'title.required' => 'عنوان الزامی است',
            'title.string' => 'عنوان باید رشته ای از حروف باشد',


        ];
    }
}
