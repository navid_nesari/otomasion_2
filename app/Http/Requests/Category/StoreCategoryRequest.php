<?php

namespace App\Http\Requests\Category;

use App\Constants\CategoryConstant;
use App\Constants\GlobalConstant;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class StoreCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string'],
//            'description' => ['required'],
//            'avatar' => ['required'],
            'parent' => ['nullable', 'string', 'exists:categories,id'],


        ];

    }



    public function messages()
    {
        return [
            "title.required"=>"وارد کردن این فیلد الزامیاست ",
//            "description.required"=>"وارد کردن این فیلد الزامیاست ",
//            "avatar.required"=>"وارد کردن این فیلد الزامیاست ",

        ];
    }




}
