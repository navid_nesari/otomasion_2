<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;

class BordController extends Controller
{
    public function index(Project $project)
    {
        $admins = $project->admins()->get();
        return view('admin.projects.bord.index', compact('project', 'admins'));
    }
}
