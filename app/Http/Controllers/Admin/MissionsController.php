<?php

namespace App\Http\Controllers\Admin;
use App\Constants\Constant;
use App\Helpers\Format\Date;
use App\Http\Controllers\Controller;
use App\Models\Mission;
use Illuminate\Http\Request;

class MissionsController extends Controller
{
    public function all()
    {
        $data = Mission::all();
        return view('admin.missions.all',compact('data'));
    }

    public function index()
    {
        $adminId = auth()->guard('admin')->user();
        $data = Mission::Where('admin_id', $adminId->id)->get();
        return view('admin.missions.index',compact('data'));
    }

    public function create()
    {
        $status = Constant::getRequestForHelpStatusesViewer();
        return view('admin.missions.create', compact('status'));
    }

    public function store(Request $request)
    {
        $this->validateStoreForm($request);

        $data = $this->getDateForm($request);
        $model = Mission::create($data);
        if ($model instanceof Mission) {
            return redirect()->route('admin.missions.index')->with("store", "درخواست مساعده شما با موفقیت ثبت شد ");
        }
        return redirect()->back();

    }

    public function answer(Mission $mission)
    {
        $status = Constant::getRequestForHelpStatusesViewer();
        return view('admin.missions.answer',compact('mission','status'));
    }
    public function answerStore(Request $request ,Mission $mission)
    {
        $mission->update([
            'status'=>$request->input('status'),
            'answer'=>$request->input('answer')
        ]);
        return redirect()->route('admin.missions.all')->with('store','عملیات شما با موفیت انجام شد');
    }
    private function validateStoreForm($request)
    {
        $request->validate([
            'full_name' => ['required'],
            'part' => ['required'],
            'mission' => ['required'],
            'date_start' => ['required'],
            'date_end' => ['required'],
        ], [
            "*.required" => "وارد کردن این فیلد الزامیست ",
        ]);
    }
    private function getDateForm($request)
    {
        $admin = auth()->guard('admin')->user();
        return [
            'admin_id' => $admin->id,
            'full_name' => $request->full_name,
            'part' => $request->part,
            'mission' => $request->mission,
            'date' => Date::toCarbonDateFormat($request->input('date')),
            'date_start' => $request->date_start,
            'date_end' => $request->date_end,
        ];
    }
}
