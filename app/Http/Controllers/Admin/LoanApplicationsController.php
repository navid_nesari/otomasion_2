<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\LoanApplication;
use Illuminate\Http\Request;
use App\Constants\Constant;


class LoanApplicationsController extends Controller
{
    public function all()
    {
        $data = LoanApplication::all();
        return view('admin.Loan-application.all', compact('data'));
    }

    public function index()
    {
        $adminId = auth()->guard('admin')->user();
        $data = LoanApplication::Where('admin_id', $adminId->id)->get();
        return view('admin.Loan-application.index', compact('data'));
    }

    public function create()
    {
        $status = Constant::getRequestForHelpStatusesViewer();
        return view('admin.Loan-application.create', compact('status'));
    }

    public function store(Request $request)
    {
        $this->validateStoreForm($request);

        $statusPrice = $this->validatePrice($request);
        if ($statusPrice == false) {
            return redirect()->back()->with('error', 'مبلغ درخواستی شما بیشتر از حد مجاز است');
        }
        $data = $this->getDateForm($request);
        $model = LoanApplication::create($data);
        if ($model instanceof LoanApplication) {
            return redirect()->route('admin.Loan-application.index')->with("store", "درخواست مساعده شما با موفقیت ثبت شد ");
        }
        return redirect()->back();

    }

    public function answer(LoanApplication $LoanApplication)
    {
        $status = Constant::getRequestForHelpStatusesViewer();
        return view('admin.Loan-application.answer', compact('LoanApplication', 'status'));
    }

    public function answerStore(Request $request, LoanApplication $LoanApplication)
    {
        $LoanApplication->update([
            'status' => $request->input('status'),
            'answer' => $request->input('answer')
        ]);
        return redirect()->route('admin.Loan-application.all')->with('store', 'عملیات شما با موفیت انجام شد');
    }

    private function validateStoreForm($request)
    {
        $request->validate([
            'title' => ['required'],
            'price' => ['required'],
        ], [
            "*.required" => "وارد کردن این فیلد الزامیست ",
        ]);
    }

    private function validatePrice($request)
    {
        if ($request->price > 20000000) {
            return false;
        }
        return true;
    }

    private function getDateForm($request)
    {
        $adminId = auth()->guard('admin')->user();
        return [
            'title' => $request->title,
            'price' => $request->price,
            'admin_id' => $adminId->id,
            'date' => date('Y-m-d H:i:s'),
        ];
    }
}
