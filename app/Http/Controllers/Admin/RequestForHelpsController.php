<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Constant;
use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\RequestForHelp;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RequestForHelpsController extends Controller
{
    public function all()
    {
        $data = RequestForHelp::all();
        return view('admin.request-for-help.all',compact('data'));
    }

    public function index()
    {
        $adminId = auth()->guard('admin')->user();
        $data = RequestForHelp::Where('admin_id', $adminId->id)->get();
        return view('admin.request-for-help.index',compact('data'));
    }

    public function create()
    {
        $status = Constant::getRequestForHelpStatusesViewer();
        return view('admin.request-for-help.create', compact('status'));
    }

    public function store(Request $request)
    {
        $this->validateStoreForm($request);

        $statusPrice = $this->validatePrice($request);
        if ($statusPrice == false) {
            return redirect()->back()->with('error', 'مبلغ درخواستی شما بیشتر از حد مجاز است');
        }
        $data = $this->getDateForm($request);
        $model = RequestForHelp::create($data);
        if ($model instanceof RequestForHelp) {
            return redirect()->route('admin.request-for-help.index')->with("store", "درخواست مساعده شما با موفقیت ثبت شد ");
        }
        return redirect()->back();

    }

    public function answer(RequestForHelp $requestForHelp)
    {
        $status = Constant::getRequestForHelpStatusesViewer();
        return view('admin.request-for-help.answer',compact('requestForHelp','status'));
    }
    public function answerStore(Request $request ,RequestForHelp $requestForHelp)
    {
        $requestForHelp->update([
            'status'=>$request->input('status'),
            'answer'=>$request->input('answer')
        ]);
        return redirect()->route('admin.request-for-help.all')->with('store','عملیات شما با موفیت انجام شد');
    }
    private function validateStoreForm($request)
    {
        $request->validate([
            'title' => ['required'],
            'price' => ['required'],
        ], [
            "*.required" => "وارد کردن این فیلد الزامیست ",
        ]);
    }
    private function validatePrice($request)
    {
        if ($request->price > 7000000) {
            return false;
        }
        return true;
    }
    private function getDateForm($request)
    {
        $adminId = auth()->guard('admin')->user();
        return [
            'title' => $request->title,
            'price' => $request->price,
            'admin_id' => $adminId->id,
            'date' => date('Y-m-d H:i:s'),
        ];
    }
}
