<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminProjectGroup;
use App\Models\Phonebook;
use App\Models\Project;
use Illuminate\Http\Request;

class GroupChatController extends Controller
{
    public function index(Project $project)
    {
        $admins = $project->admins->all();
        $massages = AdminProjectGroup::where('project_id',$project->id)->get();
        return view('admin.projects.group-chat.index', compact('admins', 'project','massages'));
    }

    public function store(Request $request, Project $project)
    {
        $request->validate([
            'massage' => ['required'],
        ], [
            'massage.required' => 'وارد کردن پیغام الزامی است '
        ]);
        $adminId = auth()->guard('admin')->user()->id;
        $status = AdminProjectGroup::create([
            'admin_id' => $adminId,
            'project_id' => $project->id,
            'massage' => $request->input('massage'),
        ]);
        if ($status instanceof AdminProjectGroup) {
            return redirect()->back()->with("store", "پیام شمابا موفقیت ارسال شد ");
        }
        return redirect()->back();
    }
}
