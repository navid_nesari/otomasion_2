<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Constant;
use App\Filters\CategoriesFilter;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CategoriesController extends BaseController
{
    public function all()
    {
        $categories = Category::filter(new CategoriesFilter())->paginate();
        return view('admin.categories.all', compact('categories'));
    }

    public function create()
    {
        $activityStatuses = Constant::getStatusesViewer();
        return view('admin.categories.create', compact('activityStatuses'));
    }

    public function store(Request $request)
    {
        $this->validateStoreform($request);
        $data = $this->getData($request);
        $category = Category::create($data);
        if($category instanceof Category){
            return redirect()->route('admin.categories.all')->with("store", "عملیات شما با موفقیت انجام شد ");
        }
        return redirect()->back();
    }

    public function edit(Category $category)
    {
        $activityStatuses = Constant::getStatusesViewer();
        return view('admin.categories.edit', compact('category', 'activityStatuses'));
    }

    public function update(Request $request, Category $category)
    {
        $this->validateupdateform($request);
        $data = $this->getData($request);
        $result = $category->update($data);
        if ($result) {
            return redirect()->route('admin.categories.all')->with("update", "ویرایش شما با موفقیت انجام شد  ");
        }
        return redirect()->back();
    }

    public function logicalDeletion(Category $category)
    {
        $category->delete();

        return redirect()->back()->with("delete", 'مدیر شما با موفقیت حذف شد ');
    }

    private function validateStoreform($request)
    {
        $request->validate([
            'title' => ['required'],
            'description' => ['required', 'string'],
            'status' => ['required', Rule::in(Constant::ACTIVE, Constant::IN_ACTIVE)],
            'image' => ['nullable']
        ], [
            "*.required" => "وارد کردن این فیلد الزامی است ",
            "description.string" => " فیلد مربوطه باید به صورت رشته باشد "
        ]);

    }

    private function validateupdateform($request)
    {
        $request->validate([
            'title' => ['required'],
            'description' => ['required', 'string'],
            'status' => ['required', Rule::in(Constant::ACTIVE, Constant::IN_ACTIVE)],
            'image' => ['nullable']
        ], [
            "*.required" => "وارد کردن این فیلد الزامی است ",
            "description.string" => " فیلد مربوطه باید به صورت رشته باشد "
        ]);
    }

    private function getData(Request $request): array
    {
        $data = [
            'title' => request('title'),
            'description' => request('description'),
            'status' => request('status'),
        ];
        if ($request->hasFile('image')) {
            $data['image'] = $this->uploadFile($request->file('image'), Constant::CATEGORIES_IMAGE_PATH);
        }
        return $data;
    }
}
