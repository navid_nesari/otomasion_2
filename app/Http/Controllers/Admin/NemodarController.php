<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;

class NemodarController extends Controller
{
    public function index(Project $project)
    {
        return view('admin.projects.nemodar.index',compact('project'));
    }
}
