<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use App\Models\Project;
use App\Constants\Constant;
use App\Helpers\Format\Date;
use Illuminate\Http\Request;
use App\Filters\projectsFilter;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class ProjectsController extends BaseController
{
    public function all()
    {
        $projects = Project::filter(new projectsFilter())->latest()->paginate();
        $activityStatuses = Constant::getProjectStatusesViewer();
        return view('admin.projects.all', compact('projects', 'activityStatuses'));
    }

    public function secondAll()
    {
        $projects = Project::filter(new projectsFilter())->latest()->paginate();
        $activityStatuses = Constant::getProjectStatusesViewer();
        return view('admin.projects.second-all', compact('projects', 'activityStatuses'));
    }

    public function create()
    {
        $activityStatuses = Constant::getProjectStatusesViewer();
        return view('admin.projects.create', compact('activityStatuses'));
    }

    public function store(Request $request)
    {
        $this->validateStoreform($request);
        $data = $this->getData($request);
        $project = Project::create($data);
        if ($project instanceof Project) {
            $project->admins()->sync($request->input('admins'));
            return redirect()->route('admin.projects.all')->with("store", "عملیات شما با موفقیت انجام شد ");
        }
        return redirect()->back();
    }

    public function edit(Project $project)
    {
        $activityStatuses = Constant::getProjectStatusesViewer();
        $admins = $project->admins->all();
        $selectedAdminItems = [];
        foreach($admins as $admin) {
            $selectedAdminItems[]= [
                'id' => $admin->id,
                'title' => $admin->webPresent()->fullName()
            ];
        }
        return view('admin.projects.edit', compact('project', 'activityStatuses', 'selectedAdminItems'));
    }

    public function update(Request $request, Project $project)
    {
        $this->validateupdateform($request);
        $data = $this->getData($request);
        $result = $project->update($data);
        if($result) {
            $project->admins()->sync($request->input('admins'));
            return redirect()->route('admin.projects.all')->with("store", "ویرایش شما با موفقیت انجام شد  ");
        }
        return redirect()->back();
    }

    // public function logicalDeletion(Project $project)
    // {
    //     $project->admins()->detach($project->admins);
    //     $project->delete();
    //     return redirect()->back()->with("delete", 'مدیر شما با موفقیت حذف شد ');
    // }

    public function usersProject(Project $project)
    {
        // dd($project->admins->all());
        $admins = $project->admins()->get();
        return view('admin.projects.users-project', compact('project', 'admins'));
    }

    public function searchWithAjax(Request $request)
    {
        $search = $request->input('q');
        $admins = Admin::where('first_name', 'like', '%'.$search.'%')
        ->orWhere('last_name', 'like', '%'.$search.'%')
        ->select('id', 'first_name', 'last_name')->get();

        $output = [];
        foreach($admins as $admin) {
            $output[]= [
                'id' => $admin->id,
                'name' => $admin->webPresent()->fullName()
            ];
        }
        return response()->json(['data' => $output]);
    }

    private function validateStoreform($request)
    {
        $request->validate([
            'title' => ['required'],
            'start_date' => ['required'],
            'end_date' => ['required'],
            'description' => ['required'],
            'status' => ['required', Rule::in(Constant::TO_DO, Constant::IN_PROGRESS, Constant::COMPLETED)],
            'image' => ['required', 'mimes:jpg,png,bmp,jpeg']
        ],[
            "*.required" => "وارد کردن این فیلد الزامیست ",
            "status.in" => "فیلد مورد نظر معتبر نمی باشد",
            "image.mimes" => "فرمت تصویر مربوطه معتبر نمی باشد",
        ]);
    }

    private function validateupdateform($request)
    {
        $request->validate([
            'title' => ['required'],
            'start_date' => ['required'],
            'end_date' => ['required'],
            'description' => ['required'],
            'status' => ['required', Rule::in(Constant::TO_DO, Constant::IN_PROGRESS, Constant::COMPLETED)],
            'image' => ['mimes:jpg,png,bmp,jpeg']
        ], [
            "*.required" => "وارد کردن این فیلد الزامیست ",
            "status.in" => "فیلد مورد نظر معتبر نمی باشد",
            "image.mimes" => "فرمت تصویر مربوطه معتبر نمی باشد",
        ]);
    }

    private function getData(Request $request): array
    {
        $data = [
            'title' => $request->input('title'),
            'start_date' => Date::toCarbonDateFormat($request->input('start_date')),
            'end_date' => Date::toCarbonDateFormat($request->input('end_date')),
            'description' => $request->input('description'),
            'status' => $request->input('status'),
        ];
        if ($request->hasFile('image')) {
            $data['image'] = $this->uploadFile($request->file('image'), Constant::PROJECTS_IMAGE_PATH);
        }
        return $data;
    }
}
