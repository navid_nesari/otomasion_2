<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Filters\PhonebooksFilter;
use App\Models\Phonebook;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PhonebooksController extends Controller
{
    public function all()
    {
        $phonebooks = Phonebook::filter(new PhonebooksFilter())->latest()->paginate();
        return view('admin.phonebooks.all', compact('phonebooks'));
    }

    public function create()
    {
        return view('admin.phonebooks.create');
    }

    public function store(Request $request)
    {
        $this->validateStoreform($request);
        $data = $this->getDataStore($request);
        $phonebook = Phonebook::create($data);
        if ($phonebook instanceof Phonebook) {
            return redirect()->route('admin.phonebooks.all')->with("store", "عملیات شما با موفقیت انجام شد ");
        }
        return redirect()->back();
    }

    public function edit(Phonebook $phonebook)
    {
        return view('admin.phonebooks.edit', compact('phonebook'));
    }

    public function update(Request $request, Phonebook $phonebook)
    {
        $this->validateupdateform($request, $phonebook);
        $data = $this->getDataUpdate($request);
        $result = $phonebook->update($data);
        if ($result) {
            return redirect()->route('admin.phonebooks.all')->with("store", "ویرایش شما با موفقیت انجام شد  ");
        }
        return redirect()->back();
    }

    public function logicalDeletion(Phonebook $phonebook)
    {
        $phonebook->delete();
        return redirect()->back()->with("delete", 'مدیر شما با موفقیت حذف شد ');
    }

    private function validateStoreform($request)
    {
        $request->validate([
            'full_name' => ['required'],
            'mobile' => ['nullable', 'unique:phonebooks'],
            'phone' => ['nullable', 'unique:phonebooks'],
            'account_number' => ['nullable', 'unique:phonebooks'],
            'address' => ['nullable'],
            'description' => ['nullable'],

        ], [
            "*.required" => "وارد کردن این فیلد الزامیست ",
            "*.unique" => "فیلد مورد نظر باید یکتا باشد.",
        ]);
    }

    private function validateupdateform($request, $phonebook)
    {
        $request->validate([
            'full_name' => ['required'],
            'mobile' => ['nullable', Rule::unique('phonebooks', 'mobile')->ignore($phonebook)],
            'phone' => ['nullable', Rule::unique('phonebooks', 'phone')->ignore($phonebook)],
            'account_number' => ['nullable', Rule::unique('phonebooks', 'account_number')->ignore($phonebook)],
            'address' => ['nullable'],
            'description' => ['nullable'],

        ], [
            "*.required" => "وارد کردن این فیلد الزامیست ",
            "*.unique" => "فیلد مورد نظر باید یکتا باشد.",
        ]);
    }

    private function getDataStore(Request $request): array
    {
        $data = [
            'admin_id' => auth()->guard('admin')->user()->id,
            'full_name' => $request->input('full_name'),
            'mobile' => $request->input('mobile'),
            'phone' => $request->input('phone'),
            'account_number' => $request->input('account_number'),
            'address' => $request->input('address'),
            'description' => $request->input('description'),
        ];
        return $data;
    }

    private function getDataUpdate(Request $request): array
    {
        $data = [
            'full_name' => $request->input('full_name'),
            'mobile' => $request->input('mobile'),
            'phone' => $request->input('phone'),
            'account_number' => $request->input('account_number'),
            'address' => $request->input('address'),
            'description' => $request->input('description'),
        ];
        return $data;
    }
}
