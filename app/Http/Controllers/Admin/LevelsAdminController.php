<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LevelsAdminController extends Controller
{
    public function all()
    {
        $roles = Role::with('admins')->latest()->paginate();
        return view('admin.levels-admin.all', compact('roles'));
    }

    public function create()
    {
        $admins = Admin::all();
        $roles = Role::all();
        return view('admin.levels-admin.create', compact('admins', 'roles'));
    }

    public function store(Request $request)
    {
        $this->validateStoreform($request);
        $admin = Admin::find(request('admins'));
        $admin->roles()->attach(request('roles'));
        return redirect()->route('admin.levels.all')->with("store", "عملیات شما با موفقیت انجام شد ");
    }

    public function edit(Admin $level)
    {
        $admin = $level;
        $roles = Role::all();
        return view('admin.levels-admin.edit', compact('admin', 'roles'));
    }

    public function update(Request $request, Admin $level)
    {
        $admin = $level;
        $this->validateupdateform($request);
        $admin->roles()->attach(request('roles'));
        return redirect()->route('admin.levels.all')->with("store", "عملیات شما با موفقیت انجام شد ");
    }

    public function delete(Admin $level, Role $role)
    {
        $admin = $level;
        $admin->roles()->detach($role->id);
        return redirect()->back();
    }

    private function validateStoreform($request)
    {
        $request->validate([
            'admins' => ['required'],
            'roles' => ['required'],
         ],[
             '*.required' => 'فیلد مورد نظر الزامی است.',
         ]);
    }

    private function validateupdateform($request)
    {
        $request->validate([
            'roles' => ['required'],
        ], [
            "*.required" => "وارد کردن این فیلد الزامیست ",
        ]);
    }

}
