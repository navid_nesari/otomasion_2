<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use App\Constants\Constant;
use App\Helpers\Format\Date;
use Illuminate\Http\Request;
use App\Filters\AdminsFilter;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use App\Presenters\Web\Admin\UserPresenter as WebAdminPresenter;

class AdminsController extends BaseController
{

    public function index()
    {
        $admins = Admin::filter(new AdminsFilter())->latest()->paginate();
        return view('admin.admins.all', compact('admins'));
    }

    public function create()
    {
        $activityStatuses = Constant::getStatusesViewer();
        $activityMaritalStatuses = Constant::getAdminMaritalStatusesViewer();
        $activityMilitaryStatuses = Constant::getAdminMilitaryStatusesViewer();
        $activityHousingStatuses = Constant::getAdminHousingStatusesViewer();
        $activityJobStatuses = Constant::getAdminJobStatusesViewer();
        return view('admin.admins.create', compact([
            'activityStatuses',
            'activityMaritalStatuses',
            'activityMilitaryStatuses',
            'activityHousingStatuses',
            'activityJobStatuses'
        ]));
    }

    public function store(Request $request)
    {
        $this->validateStoreform($request);
        $data = $this->getData($request);
        $admin = Admin::create($data);
        if ($admin instanceof Admin) {
            return redirect()->route('admin.admins.all')->with("store", "عملیات شما با موفقیت انجام شد ");
        }
        return redirect()->back();
    }

    public function edit(Admin $admin)
    {
        $activityStatuses = Constant::getStatusesViewer();
        $activityMaritalStatuses = Constant::getAdminMaritalStatusesViewer();
        $activityMilitaryStatuses = Constant::getAdminMilitaryStatusesViewer();
        $activityHousingStatuses = Constant::getAdminHousingStatusesViewer();
        $activityJobStatuses = Constant::getAdminJobStatusesViewer();
        return view('admin.admins.edit', compact(
            'admin',
            'activityStatuses',
            'activityMaritalStatuses',
            'activityMilitaryStatuses',
            'activityHousingStatuses',
            'activityJobStatuses'
        ));
    }

    public function update(Request $request, Admin $admin)
    {

        $this->validateupdateform($request,$admin);
        $data = $this->getData($request);
        $result = $admin->update($data);
        if ($result) {
            return redirect()->route('admin.admins.all')->with("store", "ویرایش شما با موفقیت انجام شد  ");
        }
        return redirect()->back();

    }


    public function logicalDeletion(Admin $admin)
    {
        $admin->delete();
        return redirect()->back()->with("delete", 'مدیر شما با موفقیت حذف شد ');
    }

    private function validateStoreform($request)
    {
        $request->validate([
            'f_name' => ['required'],
            'l_name' => ['required'],
            'mobile' => ['required', 'unique:admins', 'numeric'],
            'email' => ['required', 'unique:admins'],
            'username' => ['required','unique:admins'],
            'password' => ['required'],
            'father_name' => ['required'],
            'birth_certificate_number' => ['required', 'unique:admins', 'numeric'],
            'issued_location' => ['required'],
            'birth_date' => ['required'],
            'religion' => ['required'],
            'religious_branch' => ['required'],
            'nationality' => ['required'],
            'postal_code' => ['required', 'numeric'],
            'address' => ['required'],
            'marital_status' => ['required', Rule::in(
                Constant::SINGLE, Constant::MARRIED, Constant::ENGAGEMENT,
                Constant::DIVORCED, Constant::DECEASED_SPOUSE
            )],
            'children_number' => ['nullable', 'numeric'],
            'military_service' => ['required', Rule::in(
                Constant::INCLUDED,
                Constant::IN_MILITARY_SERVICE,
                Constant::EXEMPT
            )],
            'details_military_location' => ['nullable'],
            'until_date' => ['nullable'],
            'exemption_type' => ['nullable'],
            'medical_exemption_type' => ['nullable'],
            'last_educational_certificate' => ['required'],
            'study_field' => ['required'],
            'education_place' => ['required'],
            'school_name' => ['nullable'],
            'specialization_type' => ['nullable'],
            'getting_degree_date' => ['nullable'],
            'language_type' => ['nullable'],
            'mastery_level' => ['nullable'],
            'workplace_name' => ['nullable'],
            'start_date' => ['nullable'],
            'end_date' => ['nullable'],
            'office_phone' => ['nullable', 'numeric'],
            'job_title' => ['nullable'],
            'amount_last_salary' => ['nullable', 'numeric'],
            'reason_leaving_work' => ['nullable'],
            'housing_situation' => ['required', Rule::in(
                Constant::PERSONAL_HOUSE,
                Constant::LIVING_WITH_PARENTS,
                Constant::TENANT
            )],
            'rent_paid' => ['nullable', 'numeric'],
            'full_name' => ['nullable'],
            'guarantor_job' => ['nullable'],
            'ratio' => ['nullable'],
            'guarantor_phone' => ['nullable', 'numeric'],
            'guarantor_address' => ['nullable'],
            'second_job' => ['required', Rule::in(
                Constant::I_HAVE,
                Constant::I_HAVENT,
                Constant::RETIRED
            )],
            'organization' => ['nullable'],
            'second_job_type' => ['nullable'],
            'salary_amount' => ['nullable', 'numeric'],
            'working_hours' => ['nullable', 'numeric'],
            'type_work_shift' => ['nullable'],
            'work_phone' => ['nullable', 'numeric'],
            'work_address' => ['nullable'],
            'status' => ['required', Rule::in(
                Constant::ACTIVE,
                Constant::IN_ACTIVE
            )],
            'birth_certificate_image' => ['required', 'mimes:jpg,png,bmp,jpeg'],
            'national_card_image' => ['required', 'mimes:jpg,png,bmp,jpeg'],
            'image' => ['required', 'mimes:jpg,png,bmp,jpeg'],

        ], [
            "*.required" => "وارد کردن این فیلد الزامی است ",
            "*.unique" => "فیلد مورد نظر باید یکتا باشد.",
            "*.numeric" => "فیلد مورد نظر باید عدد باشد.",
            "image.mimes" => "فرمت تصویر مربوطه معتبر نمی باشد",
        ]);
    }

    private function validateupdateform($request,$admin)
    {
        $request->validate([
            'f_name' => ['required'],
            'l_name' => ['required'],
            'mobile' => ['required', 'numeric', Rule::unique('admins', 'mobile')->ignore($admin)],
            'email' => ['required', 'email', Rule::unique('admins', 'email')->ignore($admin)],
            'username' => ['required', Rule::unique('admins', 'username')->ignore($admin)],
            'password' => ['required'],
            'father_name' => ['required'],
            'birth_certificate_number' => ['required', 'numeric', Rule::unique('admins', 'birth_certificate_number')->ignore($admin)],
            'issued_location' => ['required'],
            'birth_date' => ['required'],
            'religion' => ['required'],
            'religious_branch' => ['required'],
            'nationality' => ['required'],
            'postal_code' => ['required', 'numeric'],
            'address' => ['required'],
            'marital_status' => ['required', Rule::in(
                Constant::SINGLE, Constant::MARRIED, Constant::ENGAGEMENT,
                Constant::DIVORCED, Constant::DECEASED_SPOUSE
            )],
            'children_number' => ['nullable', 'numeric'],
            'military_service' => ['required', Rule::in(
                Constant::INCLUDED,
                Constant::IN_MILITARY_SERVICE,
                Constant::EXEMPT
            )],
            'details_military_location' => ['nullable'],
            'until_date' => ['nullable'],
            'exemption_type' => ['nullable'],
            'medical_exemption_type' => ['nullable'],
            'last_educational_certificate' => ['required'],
            'study_field' => ['required'],
            'education_place' => ['required'],
            'school_name' => ['nullable'],
            'specialization_type' => ['nullable'],
            'getting_degree_date' => ['nullable'],
            'language_type' => ['nullable'],
            'mastery_level' => ['nullable'],
            'workplace_name' => ['nullable'],
            'start_date' => ['nullable'],
            'end_date' => ['nullable'],
            'office_phone' => ['nullable', 'numeric'],
            'job_title' => ['nullable'],
            'amount_last_salary' => ['nullable', 'numeric'],
            'reason_leaving_work' => ['nullable'],
            'housing_situation' => ['required', Rule::in(
                Constant::PERSONAL_HOUSE,
                Constant::LIVING_WITH_PARENTS,
                Constant::TENANT
            )],
            'rent_paid' => ['nullable', 'numeric'],
            'full_name' => ['nullable'],
            'guarantor_job' => ['nullable'],
            'ratio' => ['nullable'],
            'guarantor_phone' => ['nullable', 'numeric'],
            'guarantor_address' => ['nullable'],
            'second_job' => ['required', Rule::in(
                Constant::I_HAVE,
                Constant::I_HAVENT,
                Constant::RETIRED
            )],
            'organization' => ['nullable'],
            'second_job_type' => ['nullable'],
            'salary_amount' => ['nullable', 'numeric'],
            'working_hours' => ['nullable', 'numeric'],
            'type_work_shift' => ['nullable'],
            'work_phone' => ['nullable', 'numeric'],
            'work_address' => ['nullable'],
            'status' => ['required', Rule::in(
                Constant::ACTIVE,
                Constant::IN_ACTIVE
            )],
            'birth_certificate_image' => ['mimes:jpg,png,bmp,jpeg'],
            'national_card_image' => ['mimes:jpg,png,bmp,jpeg'],
            'image' => ['mimes:jpg,png,bmp,jpeg'],

        ], [
            "*.required" => "وارد کردن این فیلد الزامی است ",
            "*.unique" => "فیلد مورد نظر باید یکتا باشد.",
            "*.numeric" => "فیلد مورد نظر باید عدد باشد.",
            "image.mimes" => "فرمت تصویر مربوطه معتبر نمی باشد",
        ]);
    }

    private function getData(Request $request): array
    {
        $professional_documents = [
            'school_name' => $request->school_name,
            'specialization_type' => $request->specialization_type,
            'getting_degree_date' => Date::toCarbonDateFormat($request->getting_degree_date),
        ];
        $foreign_language_proficiency = [
            'language_type' => $request->language_type,
            'mastery_level' => $request->mastery_level,
        ];
        $resume = [
            'workplace_name' => $request->workplace_name,
            'start_date' => Date::toCarbonDateFormat($request->start_date),
            'end_date' => Date::toCarbonDateFormat($request->end_date),
            'office_phone' => $request->office_phone,
            'job_title' => $request->job_title,
            'amount_last_salary' => $request->amount_last_salary,
            'reason_leaving_work' => $request->reason_leaving_work,
        ];
        $guarantor = [
            'full_name' => $request->full_name,
            'guarantor_job' => $request->guarantor_job,
            'ratio' => $request->ratio,
            'guarantor_phone' => $request->guarantor_phone,
            'guarantor_address' => $request->guarantor_address,
        ];
        $data = [
            'first_name' => $request->input('f_name'),
            'last_name' => $request->input('l_name'),
            'mobile' => $request->input('mobile'),
            'email' => $request->input('email'),
            'username' => $request->input('username'),
            'password' => Hash::make($request->input('password')),
            'father_name' => $request->input('father_name'),
            'birth_certificate_number' => $request->input('birth_certificate_number'),
            'issued_location' => $request->input('issued_location'),
            'birth_date' => Date::toCarbonDateFormat($request->input('birth_date')),
            'religion' => $request->input('religion'),
            'religious_branch' => $request->input('religious_branch'),
            'nationality' => $request->input('nationality'),
            'postal_code' => $request->input('postal_code'),
            'address' => $request->input('address'),
            'marital_status' => $request->input('marital_status'),
            'children_number' => $request->input('children_number'),
            'military_service' => $request->input('military_service'),
            'details_military_location' => $request->input('details_military_location'),
            'until_date' => Date::toCarbonDateFormat($request->input('until_date')),
            'exemption_type' => $request->input('exemption_type'),
            'medical_exemption_type' => $request->input('medical_exemption_type'),
            'last_educational_certificate' => $request->input('last_educational_certificate'),
            'study_field' => $request->input('study_field'),
            'education_place' => $request->input('education_place'),
            'professional_documents' => json_encode($professional_documents),
            'foreign_language_proficiency' => json_encode($foreign_language_proficiency),
            'resume' => json_encode($resume),
            'housing_situation' => $request->input('housing_situation'),
            'rent_paid' => $request->input('rent_paid'),
            'guarantor' => $guarantor,
            'second_job' => $request->input('second_job'),
            'organization' => $request->input('organization'),
            'second_job_type' => $request->input('second_job_type'),
            'salary_amount' => $request->input('salary_amount'),
            'working_hours' => $request->input('working_hours'),
            'type_work_shift' => $request->input('type_work_shift'),
            'work_phone' => $request->input('work_phone'),
            'work_address' => $request->input('work_address'),
            'status' => $request->input('status'),
        ];
        if ($request->hasFile('birth_certificate_image') && $request->hasFile('national_card_image') && $request->hasFile('image')) {
            $data['birth_certificate_image'] = $this->uploadFile($request->file('birth_certificate_image'), Constant::ADMINS_IMAGE_PATH);
            $data['national_card_image'] = $this->uploadFile($request->file('national_card_image'), Constant::ADMINS_IMAGE_PATH);
            $data['image'] = $this->uploadFile($request->file('image'), Constant::ADMINS_IMAGE_PATH);
        }
        return $data;
    }


}
