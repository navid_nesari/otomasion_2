<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    public function all()
    {
        // dd(auth()->guard('admin')->user()->hasRole(Permission::whereName('show-AC')->first()->roles));
        // dd(auth()->guard('admin')->user()->hasRole('writer'));
        $roles = Role::latest()->paginate();
        return view('admin.roles.all', compact('roles'));
    }

    public function create()
    {
        $permissions = Permission::get();
        return view('admin.roles.create', compact('permissions'));
    }

    public function store(Request $request)
    {
        $this->validateStoreform($request);
        $data = $this->getData($request);
        $role = Role::create($data);
        if ($role instanceof Role) {
            $role->permissions()->sync(request('permissions'));
            return redirect()->route('admin.roles.all')->with("store", "عملیات شما با موفقیت انجام شد ");
        }
        return redirect()->back();
    }

    public function edit(Role $role)
    {
        $permissions = Permission::get();
        return view('admin.roles.edit', compact('role', 'permissions'));
    }

    public function update(Request $request, Role $role)
    {
        // dd($request->input('permissions'));
        $this->validateupdateform($request);
        $data = $this->getData($request);
        $result = $role->update($data);
        if($result) {
            $role->permissions()->sync($request->input('permissions'));
            return redirect()->route('admin.roles.all')->with("store", "ویرایش شما با موفقیت انجام شد  ");
        }
        return redirect()->back();
    }

    public function logicalDeletion(Role $role)
    {
        $role->permissions()->detach();
        $role->delete();
        return redirect()->back();
    }

    private function validateStoreform($request)
    {
        $request->validate([
            'name' => ['required'],
            'permissions' => ['required'],
            'label' => ['nullable'],
        ],[
            "*.required" => "وارد کردن این فیلد الزامیست ",
        ]);
    }

    private function validateupdateform($request)
    {
        $request->validate([
            'name' => ['required'],
            'permissions' => ['required'],
            'label' => ['nullable'],
        ], [
            "*.required" => "وارد کردن این فیلد الزامیست ",
        ]);
    }

    private function getData($request): array
    {
        $data = [
            'name' => request('name'),
             'label' => request('label'),
        ];
        return $data;
    }
}
