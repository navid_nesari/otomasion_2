<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use Illuminate\Http\Request;

class PermissionsController extends Controller
{
    public function all()
    {
        $permissions = Permission::latest()->paginate();
        return view('admin.permissions.all', compact('permissions'));
    }

    public function create()
    {
        return view('admin.permissions.create');
    }

    public function store(Request $request)
    {
        $this->validateStoreform($request);
        $data = $this->getData($request);
        $permission = Permission::create($data);
        if ($permission instanceof Permission) {
            return redirect()->route('admin.permissions.all')->with("store", "عملیات شما با موفقیت انجام شد ");
        }
        return redirect()->back();
    }

    public function edit(Permission $permission)
    {
        return view('admin.permissions.edit', compact('permission'));
    }

    public function update(Request $request, Permission $permission)
    {
        $this->validateupdateform($request);
        $data = $this->getData($request);
        $result = $permission->update($data);
        if($result) {
            return redirect()->route('admin.permissions.all')->with("store", "ویرایش شما با موفقیت انجام شد  ");
        }
        return redirect()->back();
    }

    public function logicalDeletion(Permission $permission)
    {
        $permission->delete();
        return redirect()->back();
    }

    private function validateStoreform($request)
    {
        $request->validate([
            'name' => ['required'],
            'label' => ['nullable'],
        ],[
            "*.required" => "وارد کردن این فیلد الزامیست ",
        ]);
    }

    private function validateupdateform($request)
    {
        $request->validate([
            'name' => ['required'],
            'label' => ['nullable'],
        ], [
            "*.required" => "وارد کردن این فیلد الزامیست ",
        ]);
    }

    private function getData($request): array
    {
        $data = [
            'name' => request('name'),
            'label' => request('label'),
        ];
        return $data;
    }
}
