<?php

namespace App\Models;

use App\Filters\Contracts\Filterable;
use App\Presenters\Contracts\Presentable;
use App\Presenters\Web\Project\ProjectPresenter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Project extends Model
{
    use HasFactory, Filterable, Presentable;

    protected $webPresenter = ProjectPresenter::class;

    protected $guarded = [
        'id',
    ];

    public function admins(): BelongsToMany
    {
        return $this->belongsToMany(Admin::class);
    }

    public function duties(): HasMany
    {
        return $this->hasMany(Duty::class);
    }
}
