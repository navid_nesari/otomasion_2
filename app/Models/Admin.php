<?php

namespace App\Models;

use App\Filters\Contracts\Filterable;
use Illuminate\Database\Eloquent\Model;
use App\Presenters\Contracts\Presentable;
use App\Presenters\Web\Admin\AdminPresenter;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Admin extends Authenticatable
{
    use HasFactory, Filterable, Presentable, SoftDeletes;

    protected $guarded = ['id'];
    protected $webPresenter = AdminPresenter::class;

    protected $casts = [
        'guarantor' => 'array'
    ];

    public function phonebooks(): HasMany
    {
        return $this->hasMany(Phonebook::class);
    }

    public function projects(): BelongsToMany
    {
        return $this->belongsToMany(Project::class);
    }

    public function duties():BelongsToMany
    {
        return $this->belongsToMany(Duty::class);
    }

    // protected function professional_documents(): Attribute
    // {
    //     return Attribute::make(
    //         get: fn ($value) => json_decode($value, true),
    //         set: fn ($value) => json_encode($value),
    //     );
    // }

    public function roles():BelongsToMany
    {
        return $this->belongsToMany(Role::class);
    }

    public function hasRole($role)
    {
        if( is_string($role) ) {
            return $this->roles->contains('name', $role);
        }

        foreach($role as $r) {
            if($this->hasRole($r->name)){
                return true;
            }
        }
        return false;
        // return !! $role->intersect($this->roles)->count();
    }
}
