<?php

namespace App\Models;

use App\Presenters\Contracts\Presentable;
use App\Presenters\Web\RequestForHelp\RequestForHelpPresenter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestForHelp extends Model
{
    use HasFactory, Presentable;

    protected $guarded = ['id'];
    protected $webPresenter = RequestForHelpPresenter::class;


}
