<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function admins():BelongsToMany
    {
        return $this->belongsToMany(Admin::class);
    }

    public function permissions():BelongsToMany
    {
        return $this->belongsToMany(Permission::class);
    }
}
