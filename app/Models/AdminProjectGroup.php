<?php

namespace App\Models;

use App\Presenters\Contracts\Presentable;
use App\Presenters\Web\AdminProjectGroup\AdminProjectGroupPresenter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class AdminProjectGroup extends Model
{
    use HasFactory,Presentable;

    protected $guarded = ['id'];
    protected $webPresenter = AdminProjectGroupPresenter::class;



}
