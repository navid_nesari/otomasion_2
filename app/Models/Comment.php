<?php

namespace App\Models;

use App\Constants\Constant;
use App\Presenters\Contracts\Presentable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory,Presentable;

    protected $guarded = ['id'];

    public function commentable()
    {
        return $this->morphTo();
    }
    public function getEntity($entity = null)
    {

        if(!is_null($entity) && in_array($entity,['shop','post']))
        {
            return [
                Constant::LETTER => Letter::class,

            ][$entity];
        }

    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
