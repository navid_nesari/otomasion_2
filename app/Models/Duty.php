<?php

namespace App\Models;

use App\Presenters\Contracts\Presentable;
use App\Presenters\Web\Duty\DutyPresenter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Duty extends Model
{
    use HasFactory, Presentable, SoftDeletes;

    protected $webPresenter = DutyPresenter::class;

    protected $guarded = [
        'id',
    ];

    public function admins(): BelongsToMany
    {
        return $this->belongsToMany(Admin::class);
    }

    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class);
    }
}
