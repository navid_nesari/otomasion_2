<?php


namespace App\Presenters\Web\AdminProjectGroup;


use App\Constants\Constant;
use App\Models\Admin;
use App\Presenters\Contracts\Presenter;

class AdminProjectGroupPresenter extends Presenter
{
    public function image()
    {
        return str_replace('\\','/',asset(Constant::ADMINS_IMAGE_PATH . DIRECTORY_SEPARATOR . $this->entity->image));
    }

    public function fullName($id)
    {
        $admin = Admin::whhere('id',$id)->first();
        return $admin->first_name .' '. $admin->last_name;
    }

}
