<?php


namespace App\Presenters\Web\Project;


use App\Constants\Constant;
use App\Presenters\Contracts\Presenter;

class ProjectPresenter extends Presenter
{
    public function image()
    {
        if (is_null($this->entity->image) || $this->entity->image == "") {
            return asset('admin-assets/media/avatars/blank.png');
        }
        return str_replace('\\','/',asset(Constant::PROJECTS_IMAGE_PATH . $this->entity->image));
    }

    public function status()
    {
        if ($this->entity->status == "to-do") {
            return "<span class='badge badge-light-warning'>برای انجام</span>";
        } elseif ($this->entity->status == "in-progress") {
            return "<span class='badge badge-light-primary'>در حال انجام</span>";
        } elseif ($this->entity->status == "completed") {
            return "<span class='badge badge-light-success'>انجام شده</span>";
        }
    }

    public function fullName()
    {
        return $this->entity->first_name .' '. $this->entity->last_name;
    }

}
