<?php

namespace App\Presenters\Web\RequestForHelp;

use App\Constants\Constant;
use App\Presenters\Contracts\Presenter;

class RequestForHelpPresenter extends Presenter
{
    public function status()
    {
        if ($this->entity->status == Constant::ACCEPTED) {
            return " <div class='badge badge-light-success'>مورد تایید میباشد</div>";
        } elseif ($this->entity->status == Constant::REJECTED) {
            return "  <div class='badge badge-light-danger'>مورد تایید نمیباشد </div>";
        }elseif ($this->entity->status == Constant::AWAITING_CONFIRMATION)
        {
            return "  <div class='badge badge-light-warning'>درحال انتظار  </div>";
        }
    }
}
