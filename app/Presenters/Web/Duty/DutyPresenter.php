<?php


namespace App\Presenters\Web\Duty;


use App\Constants\Constant;
use App\Presenters\Contracts\Presenter;

class DutyPresenter extends Presenter
{
    public function status()
    {
        if ($this->entity->status == "to-do") {
            return "<span class='badge badge-light-danger'>برای انجام</span>";
        } elseif ($this->entity->status == "in-progress") {
            return "<span class='badge badge-light-primary'>در حال انجام</span>";
        } elseif ($this->entity->status == "completed") {
            return "<span class='badge badge-light-info'>انجام شده</span>";
        } elseif ($this->entity->status == "awaiting-confirmation") {
            return "<span class='badge badge-light-warning'>منتظر تایید</span>";
        } elseif ($this->entity->status == "confirmed") {
            return "<span class='badge badge-light-success'>تایید شده</span>";
        }
    }

}
