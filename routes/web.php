<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Admin\DashboardController;
use \App\Http\Controllers\Admin\AdminsController;
use \App\Http\Controllers\Admin\profileController;
use \App\Http\Controllers\Admin\CleaningsController;
use \App\Http\Controllers\Admin\MiniBarsController;
use \App\Http\Controllers\Admin\ServicesRoomsController;
use \App\Http\Controllers\Admin\ServicesRoomsVipController;
use \App\Http\Controllers\Admin\WheelchairsController;
use \App\Http\Controllers\Admin\SouvenirsController;
use \App\Http\Controllers\Admin\CategoriesController;
use \App\Http\Controllers\Admin\RoomsGameController;
use \App\Http\Controllers\Admin\RestaurantsController;
use \App\Http\Controllers\Admin\CoffeeShopsController;
use \App\Http\Controllers\Admin\PostsController;
use \App\Http\Controllers\Admin\HouseKeepingController;
use App\Http\Controllers\Admin\phonebooksController;

/*----------admin-----------*/

Route::group(['prefix' => '', 'middleware' => 'auth.admin'], function () {

    //dashboard
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');

    //Profile
    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', [ProfileController::class, 'index'])->name('admin.profile.index');
        Route::post('/update', [ProfileController::class, 'update'])->name('admin.profile.update');
    });

    //Admins
    Route::group(['prefix' => 'admins'], function () {
        Route::get('/', [AdminsController::class, 'index'])->name('admin.admins.all');
        Route::get('/create', [AdminsController::class, 'create'])->name('admin.admins.create');
        Route::post('/store', [AdminsController::class, 'store'])->name('admin.admins.store');
        Route::get('/edit/{admin}', [AdminsController::class, 'edit'])->name('admin.admins.edit');
        Route::post('/update/{admin}', [AdminsController::class, 'update'])->name('admin.admins.update');
        Route::get('/logical-deletion/{admin}', [AdminsController::class, 'logicalDeletion'])->name('admin.admins.logicalDeletion');
    });

    // categories
    Route::group(['prefix' => 'categories'], function () {
        Route::get('/all', [CategoriesController::class, 'all'])->name('admin.categories.all');
        Route::get('/create', [CategoriesController::class, 'create'])->name('admin.categories.create');
        Route::post('/store', [CategoriesController::class, 'store'])->name('admin.categories.store');
        Route::get('/edit/{category}', [CategoriesController::class, 'edit'])->name('admin.categories.edit');
        Route::post('/update/{category}', [CategoriesController::class, 'update'])->name('admin.categories.update');
        Route::get('/logical-deletion/{category}', [CategoriesController::class, 'logicalDeletion'])->name('admin.categories.logicalDeletion');
    });

     // phonebooks
     Route::group(['prefix' => 'phonebooks'], function () {
        Route::get('/all', [phonebooksController::class, 'all'])->name('admin.phonebooks.all');
        Route::get('/create', [phonebooksController::class, 'create'])->name('admin.phonebooks.create');
        Route::post('/store', [phonebooksController::class, 'store'])->name('admin.phonebooks.store');
        Route::get('/edit/{phonebook}', [phonebooksController::class, 'edit'])->name('admin.phonebooks.edit');
        Route::post('/update/{phonebook}', [phonebooksController::class, 'update'])->name('admin.phonebooks.update');
        Route::get('/logical-deletion/{phonebook}', [phonebooksController::class, 'logicalDeletion'])->name('admin.phonebooks.logicalDeletion');
    });
});
/*---------- app ------------*/
