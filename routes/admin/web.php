<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\DutiesController;
use \App\Http\Controllers\Admin\AdminsController;
use \App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\phonebooksController;
use App\Http\Controllers\Admin\ProjectsController;
use \App\Http\Controllers\Admin\CategoriesController;
use \App\Http\Controllers\Admin\GroupChatController;
use \App\Http\Controllers\Admin\BordController;
use App\Http\Controllers\Admin\LevelsAdminController;
use \App\Http\Controllers\Admin\NemodarController;
use \App\Http\Controllers\Admin\RequestForHelpsController;
use \App\Http\Controllers\Admin\LoanApplicationsController;
use \App\Http\Controllers\Admin\MissionsController;
use App\Http\Controllers\Admin\PermissionsController;
use App\Http\Controllers\Admin\RolesController;

/*----------admin-----------*/

Route::group(['prefix' => '', 'middleware' => 'auth.admin'], function () {

    //dashboard
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');

    //Profile
    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', [ProfileController::class, 'index'])->name('admin.profile.index');
        Route::post('/update', [ProfileController::class, 'update'])->name('admin.profile.update');
    });

    //Admins
    Route::group(['prefix' => 'admins'], function () {
        Route::get('/', [AdminsController::class, 'index'])->name('admin.admins.all');
        Route::get('/create', [AdminsController::class, 'create'])->name('admin.admins.create');
        Route::post('/store', [AdminsController::class, 'store'])->name('admin.admins.store');
        Route::get('/edit/{admin}', [AdminsController::class, 'edit'])->name('admin.admins.edit');
        Route::post('/update/{admin}', [AdminsController::class, 'update'])->name('admin.admins.update');
        Route::get('/logical-deletion/{admin}', [AdminsController::class, 'logicalDeletion'])->name('admin.admins.logicalDeletion');
        Route::get('/phone-books/{admin}', [AdminsController::class, 'phonebooks'])->name('admin.admins.phonebooks');
    });

    // categories
    Route::group(['prefix' => 'categories'], function () {
        Route::get('/all', [CategoriesController::class, 'all'])->name('admin.categories.all');
        Route::get('/create', [CategoriesController::class, 'create'])->name('admin.categories.create');
        Route::post('/store', [CategoriesController::class, 'store'])->name('admin.categories.store');
        Route::get('/edit/{category}', [CategoriesController::class, 'edit'])->name('admin.categories.edit');
        Route::post('/update/{category}', [CategoriesController::class, 'update'])->name('admin.categories.update');
        Route::get('/logical-deletion/{category}', [CategoriesController::class, 'logicalDeletion'])->name('admin.categories.logicalDeletion');
    });

    // phonebooks
    Route::group(['prefix' => 'phonebooks'], function () {
        Route::get('/all', [phonebooksController::class, 'all'])->name('admin.phonebooks.all');
        Route::get('/create', [phonebooksController::class, 'create'])->name('admin.phonebooks.create');
        Route::post('/store', [phonebooksController::class, 'store'])->name('admin.phonebooks.store');
        Route::get('/edit/{phonebook}', [phonebooksController::class, 'edit'])->name('admin.phonebooks.edit');
        Route::post('/update/{phonebook}', [phonebooksController::class, 'update'])->name('admin.phonebooks.update');
        Route::get('/logical-deletion/{phonebook}', [phonebooksController::class, 'logicalDeletion'])->name('admin.phonebooks.logicalDeletion');
    });

    // projects
    Route::group(['prefix' => 'projects'], function () {
        Route::get('/all', [ProjectsController::class, 'all'])->name('admin.projects.all');
        Route::get('/second-all', [ProjectsController::class, 'secondAll'])->name('admin.projects.second-all');
        Route::get('/create', [ProjectsController::class, 'create'])->name('admin.projects.create');
        Route::post('/store', [ProjectsController::class, 'store'])->name('admin.projects.store');
        Route::get('/edit/{project}', [ProjectsController::class, 'edit'])->name('admin.projects.edit');
        Route::post('/update/{project}', [ProjectsController::class, 'update'])->name('admin.projects.update');
        // Route::get('/logical-deletion/{project}', [ProjectsController::class, 'logicalDeletion'])->name('admin.projects.logicalDeletion');
        Route::get('/{project}/users-project', [ProjectsController::class, 'usersProject'])->name('admin.projects.users-project');
        Route::get('/ajax-search', [ProjectsController::class, 'searchWithAjax'])->name('admin.projects.ajax-search');

        // duties
        Route::group(['prefix' => '{project}/duties'], function () {
            Route::get('/all', [DutiesController::class, 'all'])->name('admin.project-duties.all');

            Route::get('/second-all', [DutiesController::class, 'secondAll'])->name('admin.project-duties.second-all');
            Route::get('/create', [DutiesController::class, 'create'])->name('admin.project-duties.create');
            Route::post('/store', [DutiesController::class, 'store'])->name('admin.project-duties.store');
            Route::get('/edit/{duty}', [DutiesController::class, 'edit'])->name('admin.project-duties.edit');
            Route::post('/update/{duty}', [DutiesController::class, 'update'])->name('admin.project-duties.update');
            Route::get('/logical-deletion/{duty}', [DutiesController::class, 'logicalDeletion'])->name('admin.project-duties.logicalDeletion');
            Route::get('/{duty}/users-duty', [DutiesController::class, 'usersDuty'])->name('admin.project-duties.users');
        });

        // group-chat
        Route::group(['prefix' => 'group-chat'], function () {
            Route::get('/{project}', [GroupChatController::class, 'index'])->name('admin.project.group-chat.all');
            Route::post('/store/{project}', [GroupChatController::class, 'store'])->name('admin.project.group-chat.store');
        });
        // bord-project
        Route::group(['prefix' => 'bord-project'], function () {
            Route::get('/{project}', [BordController::class, 'index'])->name('admin.project.bord-project.all');
        });
        // nemodar-project
        Route::group(['prefix' => 'nemodar-project'], function () {
            Route::get('/{project}', [NemodarController::class, 'index'])->name('admin.project.nemodar.all');
        });
    });
    // request-for-help
    Route::group(['prefix' => 'request-for-help'], function () {
        Route::get('/all', [RequestForHelpsController::class, 'all'])->name('admin.request-for-help.all');
        Route::get('/index', [RequestForHelpsController::class, 'index'])->name('admin.request-for-help.index');
        Route::get('/create', [RequestForHelpsController::class, 'create'])->name('admin.request-for-help.create');
        Route::post('/store', [RequestForHelpsController::class, 'store'])->name('admin.request-for-help.store');
        Route::get('/answer/{requestForHelp}', [RequestForHelpsController::class, 'answer'])->name('admin.request-for-help.answer');
        Route::post('/answer/store/{requestForHelp}', [RequestForHelpsController::class, 'answerStore'])->name('admin.request-for-help.answer.store');
    });
    // Loan-application
    Route::group(['prefix' => 'Loan-application'], function () {
        Route::get('/all', [LoanApplicationsController::class, 'all'])->name('admin.Loan-application.all');
        Route::get('/index', [LoanApplicationsController::class, 'index'])->name('admin.Loan-application.index');
        Route::get('/create', [LoanApplicationsController::class, 'create'])->name('admin.Loan-application.create');
        Route::post('/store', [LoanApplicationsController::class, 'store'])->name('admin.Loan-application.store');
        Route::get('/answer/{LoanApplication}', [LoanApplicationsController::class, 'answer'])->name('admin.Loan-application.answer');
        Route::post('/answer/store/{LoanApplication}', [LoanApplicationsController::class, 'answerStore'])->name('admin.Loan-application.answer.store');
    });
    // missions
    Route::group(['prefix' => 'missions'], function () {
        Route::get('/all', [MissionsController::class, 'all'])->name('admin.missions.all');
        Route::get('/index', [MissionsController::class, 'index'])->name('admin.missions.index');
        Route::get('/create', [MissionsController::class, 'create'])->name('admin.missions.create');
        Route::post('/store', [MissionsController::class, 'store'])->name('admin.missions.store');
        Route::get('/answer/{mission}', [MissionsController::class, 'answer'])->name('admin.missions.answer');
        Route::post('/answer/store/{mission}', [MissionsController::class, 'answerStore'])->name('admin.missions.answer.store');
    });
    // roles
    Route::group(['prefix' => 'roles'], function () {
        Route::get('/all', [RolesController::class, 'all'])->name('admin.roles.all');
        Route::get('/create', [RolesController::class, 'create'])->name('admin.roles.create');
        Route::post('/store', [RolesController::class, 'store'])->name('admin.roles.store');
        Route::get('/edit/{role}', [RolesController::class, 'edit'])->name('admin.roles.edit');
        Route::post('/update/{role}', [RolesController::class, 'update'])->name('admin.roles.update');
        Route::get('/logical-deletion/{role}', [RolesController::class, 'logicalDeletion'])->name('admin.roles.logicalDeletion');
    });
    // permissions
    Route::group(['prefix' => 'permissions'], function () {
        Route::get('/all', [PermissionsController::class, 'all'])->name('admin.permissions.all');
        Route::get('/create', [PermissionsController::class, 'create'])->name('admin.permissions.create');
        Route::post('/store', [PermissionsController::class, 'store'])->name('admin.permissions.store');
        Route::get('/edit/{permission}', [PermissionsController::class, 'edit'])->name('admin.permissions.edit')->middleware('can:show-item,create-article');
        // Route::get('/edit/{permission}', [PermissionsController::class, 'edit'])->name('admin.permissions.edit')->middleware('can:show-users,show-comment,edit-article');
        Route::post('/update/{permission}', [PermissionsController::class, 'update'])->name('admin.permissions.update');
        Route::get('/logical-deletion/{permission}', [PermissionsController::class, 'logicalDeletion'])->name('admin.permissions.logicalDeletion');
    });
    // levels
    Route::group(['prefix' => 'levels'], function () {
        Route::get('/all', [LevelsAdminController::class, 'all'])->name('admin.levels.all');
        Route::get('/create', [LevelsAdminController::class, 'create'])->name('admin.levels.create');
        Route::post('/store', [LevelsAdminController::class, 'store'])->name('admin.levels.store');
        Route::get('/edit/{level}', [LevelsAdminController::class, 'edit'])->name('admin.levels.edit');
        Route::post('/update/{level}', [LevelsAdminController::class, 'update'])->name('admin.levels.update');
        Route::get('/delete/{level}/{role}', [LevelsAdminController::class, 'delete'])->name('admin.levels.delete');
    });

});
/*---------- app ------------*/
