@if ($paginator->hasPages())
<div class="pagination">
    @if ($paginator->onFirstPage())
    <a href="#" class="prev"><i class="mdi mdi-chevron-double-right"></i></a>
    @else
        <a href="#" class="next"><i class="mdi mdi-chevron-double-left"></i></a>
    @endif
    <a href="#">1</a>
    <a href="#" class="active-page">2</a>
    <a href="#">3</a>

</div>
@endif
