<!DOCTYPE html>
<!-- saved from url=(0076)chrome-extension://ebilacdhmebcihmbjgibcbeaihbecapj/dist/override/index.html -->
<html lang="fa" dir="rtl" class="dark">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <!--<base target="_blank">-->
    <base href="." target="_blank"/>
    <title>New Tab</title>
    <link rel="stylesheet" href="{{asset('dast/css/override.css')}}">
    <link rel="stylesheet" href="{{asset('dast/css/plugin.css')}}">
    <link rel="stylesheet" href="{{asset('dast/css/jalalidatepicker.css')}}">
</head>
<body
    class="override-body"
    onload="startTime(),autoplayHand()"
    style="
      background: url('https://liara-s3.dastyar.io/1662ad40-9f11-4423-98dc-45fa9a510260.png')
        center center / cover no-repeat;
      font-family: Estedad;
    "
>
<br/>
<br/>
<div id="app" style="overflow: hidden;">
    <div class="w-screen h-full overflow-y-scroll no-scrollbar" style="margin-top: 10px;">
        <div class="container mx-auto" dir="rtl">
            <div class="container mx-auto grid-cols-12 gap-2 grid px-2 lg:px-1">
                <div class="col-span-12 md:col-span-3">
                    <div
                        class="bg-white min-w-3sq dark:bg-d-black-10 dark:border-none rounded-xl text-center"
                    >
                        <div
                            class="relative h-4sq flex flex-col justify-between lg:h-7sq"
                        >
                            <div class="absolute left-2 top-3">
                                <button
                                    onclick="showBlur()"
                                    class="btnBlur flex items-center gap-x-2 text-xs rounded-lg w-max h-8 px-2 py-1 filter blur-none justify-around p-2 bg-gray-100 text-blue-darkest dark:bg-[#1C1F2B] dark:text-white"
                                    style="pointer-events: auto !important"
                                >
                                    <svg
                                        width="15"
                                        height="14"
                                        viewBox="0 0 15 14"
                                        fill="none"
                                        xmlns="http://www.w3.org/2000/svg"
                                    >
                                        <path
                                            d="M5.08577 7.28791L2.45466 4.65679C1.41884 5.78597 1.02631 6.91719 1.0195 6.94036L0.947266 7.1557L1.01882 7.37105C1.03381 7.4167 2.59708 11.9259 7.79867 11.9259C8.43174 11.9259 9.00826 11.8564 9.53775 11.7399L7.66646 9.8686C6.99269 9.83557 6.3553 9.55307 5.8783 9.07607C5.4013 8.59907 5.1188 7.96168 5.08577 7.28791ZM7.79867 2.38548C6.53456 2.38548 5.49874 2.66079 4.63533 3.06558L2.11052 0.540771L1.14693 1.50436L13.4132 13.7706L14.3768 12.8071L12.1293 10.5596C13.927 9.22871 14.5683 7.39967 14.5778 7.37105L14.6494 7.1557L14.5778 6.94036C14.5628 6.89471 13.0003 2.38548 7.79867 2.38548ZM9.09753 7.52778C9.22496 7.06643 9.11661 6.54716 8.76225 6.19212C8.40789 5.83708 7.88794 5.72941 7.42659 5.85684L6.39895 4.8292C6.82007 4.57023 7.3043 4.43208 7.79867 4.42986C9.30197 4.42986 10.5245 5.6524 10.5245 7.1557C10.5225 7.64998 10.3841 8.13411 10.1245 8.55474L9.09753 7.52778Z"
                                            class="fill-[#3D518F] dark:fill-[#D5D8E3]"
                                        ></path>
                                    </svg>
                                </button>
                            </div>
                            <div
                                class="basis-1/8 mb-1 border-b pb-1 dark:border-[#EEF0F512]"
                            >
                                <div class="flex justify-between p-3">
                                    <h1
                                        class="font-bold text-lg dark:text-d-white-70"
                                    >
                                        وظایف من
                                    </h1>
                                </div>
                            </div>
                            <div
                                class="basis-full mb-1 overflow-y-auto boxBlur"
                            >
                                <ul>
                                    @foreach($duties as $duty)
                                        <li class="list">
                                            <div
                                                class="bg-white flex flex-col my-2 mx-2 gap-y-2 border dark:bg-d-black-30 dark:border-d-black-50 rounded rounded-xl text-center"
                                            >
                                                <div
                                                    class="flex justify-between items-center w-full px-2 mt-2"
                                                >
                                                    <div
                                                        class="flex flex-col items-start mx-2 w-full overflow-hidden"
                                                    >
                              <span
                                  class="max-w-50 font-semibold text-blue-darkest text-ellipsis dark:text-d-black-80 text-right"
                              >{!! $duty->title !!}</span
                              >
                                                    </div>

                                                </div>
                                                <div class="flex items-center gap-x-2 px-2 mb-1">
                                                    <div
                                                        class="flex gap-x-1 items-center"
                                                        style="color: rgb(227, 66, 52)"
                                                    >
                                                        <svg
                                                            width="10"
                                                            height="11"
                                                            viewBox="0 0 10 11"
                                                            fill="none"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            class="relative -mt-[2px]"
                                                        >
                                                            <path
                                                                d="M1.22222 4.20662H8.77778M3.22222 1.05554V2.38888M6.77778 1.05554V2.38888M9 3.94443V7.72221C9 9.05554 8.33333 9.94443 6.77778 9.94443H3.22222C1.66667 9.94443 1 9.05554 1 7.72221V3.94443C1 2.6111 1.66667 1.72221 3.22222 1.72221H6.77778C8.33333 1.72221 9 2.6111 9 3.94443Z"
                                                                stroke="currentColor"
                                                                stroke-width="0.76"
                                                                stroke-miterlimit="10"
                                                                stroke-linecap="round"
                                                                stroke-linejoin="round"
                                                            ></path>
                                                            <path
                                                                d="M6.64171 6.25549H6.6457M6.64171 7.58883H6.6457M4.99761 6.25549H5.0016M4.99761 7.58883H5.0016M3.35352 6.25549H3.35751M3.35352 7.58883H3.35751"
                                                                stroke="currentColor"
                                                                stroke-width="0.888889"
                                                                stroke-linecap="round"
                                                                stroke-linejoin="round"
                                                            ></path>
                                                        </svg>
                                                        <div class="rtl">{{ jdate($duty->end_date)->format('Y-m-d') }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                    @endforeach
                                </ul>
                            </div>
                            <div class="basis-1/8">
                                <div


                                    class="border-t flex p-3 justify-between gap-x-1 dark:border-none"
                                >
                                    <a href="#"
                                       class="w-full justify-center items-center flex py-2  border-white border rounded text-white">
                                        مشاهده همه
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-span-12 md:col-span-6">
                    <div class="mb-2">
                        <div class="relative z-50 mx-0">
                            <div class="relative z-auto">
                                <div

                                    class="bg-white min-w-3sq h-sq flex items-center justify-between p-2 rounded-xl dark:bg-d-black-10 dark:border-none rounded-xl text-center"
                                >
                                    <div
                                        class="w-full bg-gray-100 rounded-full p-2 m-4 flex flex-row gap-x-2 items-center ltr:direction-ltr dark:bg-d-black-30 dark:text-d-black-70"
                                    >
                                        <div>
                                            <svg
                                                width="25"
                                                height="25"
                                                viewBox="0 0 25 25"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M22.982 12.7421C22.982 11.8603 22.909 11.2168 22.751 10.5496H12.4766V14.5294H18.5074C18.3859 15.5185 17.7293 17.008 16.2702 18.0088L16.2497 18.1421L19.4983 20.6084L19.7234 20.6304C21.7904 18.7596 22.982 16.007 22.982 12.7421Z"
                                                    fill="#4285F4"
                                                ></path>
                                                <path
                                                    d="M12.4751 23.2281C15.4297 23.2281 17.9101 22.2748 19.7219 20.6304L16.2687 18.0088C15.3446 18.6404 14.1044 19.0813 12.4751 19.0813C9.58127 19.0813 7.12517 17.2105 6.24964 14.6248L6.1213 14.6354L2.74339 17.1974L2.69922 17.3177C4.49874 20.821 8.19509 23.2281 12.4751 23.2281Z"
                                                    fill="#34A853"
                                                ></path>
                                                <path
                                                    d="M6.25087 14.6251C6.01985 13.9578 5.88616 13.2428 5.88616 12.504C5.88616 11.7652 6.01985 11.0503 6.23872 10.383L6.2326 10.2409L2.81235 7.63782L2.70044 7.68998C1.95877 9.14374 1.5332 10.7762 1.5332 12.504C1.5332 14.2318 1.95877 15.8643 2.70044 17.318L6.25087 14.6251Z"
                                                    fill="#FBBC05"
                                                ></path>
                                                <path
                                                    d="M12.4752 5.92624C14.53 5.92624 15.9161 6.7961 16.7065 7.52302L19.7949 4.56786C17.8981 2.84006 15.4298 1.77954 12.4752 1.77954C8.19512 1.77954 4.49875 4.18653 2.69922 7.68978L6.2375 10.3828C7.12519 7.79706 9.58131 5.92624 12.4752 5.92624Z"
                                                    fill="#EB4335"
                                                ></path>
                                            </svg>
                                        </div>
                                        <input
                                            class="outline-0 bg-gray-100 w-full p-y-2 placeholder:text-grayish-500 text-blue-dark text-sm dark:bg-d-black-30 dark:placeholder:text-d-black-70 dark:text-d-black-80"
                                            placeholder="جستجو در گوگل"
                                        />
                                        <div class="flex gap-x-4 items-center">
                                            <div>
                                                <svg
                                                    width="10"
                                                    height="16"
                                                    viewBox="0 0 10 16"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                                    class="cursor-pointer"
                                                >
                                                    <rect
                                                        y="0.0714111"
                                                        width="10"
                                                        height="15"
                                                        fill="url(#pattern0)"
                                                    ></rect>
                                                    <defs>
                                                        <pattern
                                                            id="pattern0"
                                                            patternContentUnits="objectBoundingBox"
                                                            width="1"
                                                            height="1"
                                                        >
                                                            <use
                                                                xlink:href="#image0_1563_4203"
                                                                transform="matrix(0.00147203 0 0 0.000981354 -0.0225711 0)"
                                                            ></use>
                                                        </pattern>
                                                        <image
                                                            id="image0_1563_4203"
                                                            width="710"
                                                            height="1019"
                                                        ></image>
                                                    </defs>
                                                </svg>
                                            </div>
                                            <div>
                                                <svg
                                                    width="17"
                                                    height="16"
                                                    viewBox="0 0 17 16"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    class="cursor-pointer"
                                                >
                                                    <ellipse
                                                        cx="8.34382"
                                                        cy="8.77502"
                                                        rx="2.89655"
                                                        ry="2.95019"
                                                        fill="#4285F4"
                                                    ></ellipse>
                                                    <path
                                                        d="M14.9644 5.38757V9.61794V12.1309C14.9644 12.3089 14.9292 12.4775 14.8795 12.6387C14.7171 13.1571 14.3178 13.5638 13.8089 13.7292C13.6506 13.7809 13.4851 13.8156 13.3102 13.8156H5.24023L6.89541 15.5015H12.0678H13.3092C13.7664 15.5015 14.202 15.4066 14.5982 15.237C14.9944 15.0663 15.3513 14.8198 15.6502 14.5142C15.8002 14.3614 15.9358 14.1949 16.0547 14.0148C16.2926 13.6565 16.4644 13.2477 16.5523 12.8094C16.5968 12.5903 16.6195 12.3627 16.6195 12.1298V10.8654V7.93211V6.65194L15.9989 4.65002L14.8795 4.87972C14.9292 5.04092 14.9644 5.20951 14.9644 5.38757Z"
                                                        fill="#EA4335"
                                                    ></path>
                                                    <path
                                                        d="M1.72353 5.38755C1.72353 5.20948 1.7587 5.0409 1.80836 4.8797C1.97077 4.3613 2.37008 3.9546 2.87905 3.78918C3.03836 3.73755 3.20388 3.70173 3.3787 3.70173H13.3097C13.4846 3.70173 13.6501 3.73755 13.8084 3.78812L13.827 2.54272L12.0684 2.0159L10.4132 0.330078H9.02284H8.34422H7.6656H6.27526L4.62008 2.0159H3.3787C1.55077 2.0159 0.0683594 3.52577 0.0683594 5.38755V6.65192V8.7592L1.72353 10.445V5.38755Z"
                                                        fill="#4285F4"
                                                    ></path>
                                                    <path
                                                        d="M13.3097 2.01587H12.0684L13.8094 3.78914C13.9366 3.83024 14.0566 3.88713 14.1684 3.95667C14.3918 4.09575 14.579 4.28646 14.7156 4.51405C14.7839 4.62784 14.8397 4.75006 14.8801 4.87966L16.6201 6.65188V5.38752C16.6201 3.52574 15.1377 2.01587 13.3097 2.01587Z"
                                                        fill="#34A853"
                                                    ></path>
                                                    <path
                                                        d="M3.3787 15.5024H7.50526L5.85008 13.8165H3.3787C2.46629 13.8165 1.72353 13.06 1.72353 12.1307V9.61356L0.0683594 7.92773V12.1307C0.0683594 13.9925 1.55077 15.5024 3.3787 15.5024Z"
                                                        fill="#FBBC04"
                                                    ></path>
                                                </svg>
                                            </div>
                                            <button class="px-4 py-2 bg-blue-dark rounded-full">
                                                <div class="flex items-center gap-x-2">
                                                    <svg
                                                        width="15"
                                                        height="15"
                                                        viewBox="0 0 15 15"
                                                        fill="none"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                    >
                                                        <path
                                                            d="M14.0714 14.1428L10.7617 10.8334M12.4436 6.75792C12.4436 9.93793 9.86567 12.5158 6.68566 12.5158C3.50564 12.5158 0.927734 9.93793 0.927734 6.75792C0.927734 3.57791 3.50564 1 6.68566 1C9.86567 1 12.4436 3.57791 12.4436 6.75792Z"
                                                            stroke="white"
                                                            stroke-width="1.47012"
                                                            stroke-linecap="round"
                                                            stroke-linejoin="round"
                                                        ></path>
                                                    </svg>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                    <div>
                        <div>
                            <div class="rtl:direction-ltr ltr:direction-rtl">
                                <div>
                                    <div class="relative w-full z-20">

                                        <section class="flex flex-col">
                                            <div
                                                style="flex-wrap: wrap;"
                                                class="relative flex justify-between items-center  gap-2 w-full mb-2 z-[10] direction-rtl self-end md:grid md:grid-cols-6 md:grid-rows-1 md:gap-2 md:mb-0 md:z-1 lg:flex lg:gap-[.575rem] lg:mb-0 lg:w-fit"
                                            >
                                                <div
                                                    class="bg-white item dark:bg-d-black-10 w-sq rounded-xl text-center border"
                                                    style="border-color: rgb(0, 109, 166)"
                                                >
                                                    <div
                                                        class="group w-full h-full relative flex items-center justify-center"
                                                    >
                                                        <a
                                                            href="https://oi.dastyar.io/shabpin"
                                                            target="_self"
                                                        >
                                                            <div
                                                                class="flex flex-col items-center justify-center gap-y-2"
                                                            >
                                    <span
                                    >
                                        <img style="margin-top: 5px"
                                            src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxESEBUTEhIVFRUXERUVFhgYEhkYEhAXFRcaFhUSFRUYHyghGBolGxcVITEhJSkrLi4uGB8zODMtNygtLisBCgoKDg0OGxAQGi0lICUvLS0tLS0tKy0tLS0tLS0tLS0wLi0tLS0tLy0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIALcBEwMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAwQCBQYBB//EAD8QAAIBAgIFCQcCBgEEAwAAAAECAAMRBCEFEhMxUQYUIkFSYXGRkhUygaGxwdFTkwcjQmJy4fCCorLiJDNj/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECAwQFBv/EADARAAICAQMDAQYGAgMAAAAAAAABAhEDEiExBEFRoRNhgbHB8AUicZHR4RQyUoLx/9oADAMBAAIRAxEAPwD7jERAEREAREQBETB6gEAziVmxB6hMDWbjBGpFyJrqNVulmfeIkwrNKqVqyXs6LcSAV+IkquDuMsRZlERBIiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAnhNp7KdWpc90EN0DiNa4GVjb7/eYSKhvf8AzP0EllE7RElT+/AiIlipHh/6v8z9pJI8P/V/mftJJSHBfJ/sxI6fvt4L95JKmHxINZ1sbgDw6OR+sibScb8/Rkwi2pV2X1RfWsR3ywlQGU4mllLL8SolUjvkorjgYstaJokO3HfPDiO6LFoniVTWPhIySd5k2RqLbVAOuYbcStEiyLJjiDwnnOD3SKIFsl5we6ZDE90giBbLa1Qe6SShMkcjdFk6i7Eip1Qe4yWSWEREAREQBERAEREAirtYeMqybEnMeEhkFJEVDe/+Z+gksgwtQEvYg9Pj3D8GTykGmtvf8y2RVLf3fIRE5rSeIrGu66tQoNUKVViu4EnLfmT5TPPnWGKk1duvv9jXpuml1EnFOqV+qX1NnofGbTaZg2YHLq1r/ibKcsmHqjMI48FInQaOdjTGte+Yz35HK85+jzyktEluu/nf/wAOnr+njCWuPD7eNv6KWltLbKotIWuV1rndmSAo78jNbRxLLUaoD0mve+a5m+Q6t06CvgKNRtZ6asbWuR1Dq+ZmYwdECwpLIy9Nnnk1a9rtc7f2Ww9T0+PHp0O6qXG/rwRaNxZqJcixBseB75amFGiq5KAPCZzsxqSilJ2zz8ji5twVLwIiJcoIJiUai6zte5ta1mAt5yJOis5aS9rDiPONYcR5ylzZey3rWObL2W9aymt+DPXLx8/4LusOI84BlLmy9lvWshqrqkWuP+oH6Rra5QeVrlff7GziImhsIiIAiIgCTUa3UZDEkJl+JDQfqk0k0TEREAREQBERAKdY9IzXYrSiI2pmWABIH9N915fY3M1GL0Cr1TU2jKWtcAAjIAdfhOfqHmUV7JK77+PjsadOsLk/bNpU+L5+G/kp6NxpplywuGItq7xv338Zv6FYOoZdxlA6DS3/ANjE/wCIEuYTDimuqDfMnzmHSwzY/wAs+N/F82b9bPBkeuD/ADfGqqiaAYidxwGTOTvMxiIsCIiQBERAEExPKm4w3SsmKtpGAqiV61MhiQoYHjnaZROX2ra3OnJ0cZLZtepFZv0l8j+Ys36S+R/MmTfLMvBavv8Ao5MvSqDq/RFCzfpL5H8yJwSwBUKe4W3zaSGpSu6nxv8AaXcDCWHbb6ExiImh0CIiAIicBp7+KuEw9YU6aNiBbpOjAIMyCEJ9+1jmMu/fY3RDaXJ38TneSHLDD6RVzSDo1O2ujgawDX1WBBIIyPl4TooJTvg9RrEGXpQl1Nw8JKLRMoiJJYREQBIqzWX5SWVa7XPhBDZFERKlBERAEREARIKmMpq2qXAI6uuY8+pdsSSaLMStz+l2x84XH0iQA4uTYZGBRZnNcv8AlMdH4Taooao1QU6Ya+oGILazWzIAVsuvKdLNRyr0RSxWEqUqw6NtYHrVkzBH08CRKvhkU3suT5Ron+IGmmbaLT5whNtXm51O8KyWIPxM+0YN3akhqKEcopdQbhGIBZQeuxuLzjNDYSmrJRpKEUH3RkAL3Y/WdzMOnyvJbZvn6ZYKjqt9/wCu/nk8UWnsROgwe+7EREAREQBERAEREAq6UdVoVWY2UUahY9kBSSfKfMuVnJCjVw9Mo4pHD0tUMRdWRBezeGZv3njOv5acoaVFHw5TaNUpMrLewRXBXpHiQTl/qfPvabHCHCsCylCmuXu9juNyLXH2nL1Ck5Jx7FI/iPSYteLM+a7N7rtt97GH8DFbntYj3eam/iaiav0afbJ8G5GYsaNx9Ooz/wAirejUJy1A1irnqsGAN+F595m8ODLpskcmO4sS6m4eEpAS/NEdURERJLCInhNoBhWewlSZVHubzGQyjYiIkEFHSGN2ZThrgN4Nl/v4S9NRpjDhyVNwDY5d0uaLqs1JSxucwTxsxF/lKRe7RlCbc2mW4iJc1NFyloWK1B/ifqPvNXSqg+M6nSGH2lNl6yMvEZj5zjcfhKiMtMizOQALg3ubdRyzkosnsW0qA7s87d3w4ySKei8QosKeQ/uX8zJ8PVX3qZHxH5kktpG5OO1kFt5GfcesCVnz35+Mw0ZSJuGGqN+ds+4SKliemUORDEDvsfrObJd7nBmbUrvbsZ0sMFNwPmZbwtbVbuP/AC8injsALnIXtfqz3Skfy8EPLOUtcm2/L3NzEq4fFDVAY5/WWVqA7iD8Z0ppnXGafB7ERJLCIiAIiIAmu0/pVcLQaqczuRe2TuHh1nuBmxnKcvdD1cQtIockLBhew6VrN8rfGVnLTFsyzPIoP2SuXZe/g+cYqu9R2dySzEsSesmVqjWE6fTBw+2FOp0Vp0aajUzsxXWY/Ek598q4bRmCY54ot3ahX5m4nLKaR4C/Dsk8uhTjdtO5RTtc2m7e97q/fRy2nEvQbusfmPtefUP4RaaqYnR+rUJLUKmyDHeyBVZL94B1f+kTgeWOGp0adTZuChUap1rm+WWXXefR/wCF2gWwmj1FQEVKrms4O9NYAIh4HVUEjqJM2xO90el+H4Z4XPHNcNr3dvTwdlRW7S3IMMMryebo9VcCIiSSJXxD9XnJyZSY3kMiR5ERIKGFdyqkgXIG7j3TDCYlaiBl3HzU9YPfM6q3UjiJok/kOal2C63TXqscr24g5+cpKWl+4ynPRJXwW9N4sUyoI3pUa9+xY2+N/lNJorT5pUWtSuFNyxe2uztYZauWX0m25R6NqVwmpq5Bhmbe8VI6uCmcxpVNhhlRveNZta3FLjy3SWvzWWpKafd7fz6G8wfKvXYg0guV765Pw92XPb68PmfxOb0bowsoIZQSL2vdhfPNeqW6+j3UXFmA32yPkZHtIkLNjuja19OBlZR0SVIBF7rcbxlNFUpFmDNXYsLWJUkixuLG/GUm0igNjreUx9p0+/ympujaitVG7F1L9WRI+IJsfCbWrplG3jPiCfxOV9p0+/yj2nT7/KQ0nyHFNUzpfaNPv8/9SDE4tXy1rANfcSfOaH2pT7/KPadPv8pV4oNU0RCEYSUoqmu6s6KljlAtcnxvf6SanpVACCAwIsQb/icv7Tp9/lHtOn3+ULHFcIj2cdWqt/v4G+GIpDcWA4Bzb/xmaYxB/U58T/6znvadPv8AKPadP+7yj2cfBX2OP/idNg9JJSvqliOBa4HeOjlLXt9ez8z+Jx/tJP7vTPfaKcG9MmMFFUkbTbm9Ut2df7fXs/M/iXtHY0VVJAtY2+V/vOC9oJwb0zt9C4NqSENa5a+RvlYD7SSjSRsYiJBUTxlBFjmDPYEA+L6fa+LrcBUIHgpIHyE1zC87fSfI7Xqu6VrazM1jT3XJO8H7SgeRdbqqJ/3fic3B8tm6fPLJKVXbb7d2avkfhVbH0FqIHXXJsRcXVSym3cwB+E+yz59yU5PGni0Z3uU1jYDr1SMyerOfQZtj4PZ/DccoYmpKt/oi7TFgPCZRE1PUEREAixB6MqyxidwleQyj5EREggTW6boKaTZgEgjMgaxtlv65njaFZv6yF4KQD8Sc/K0oUtFapJ1WYntZ/WZzle1GGSVrTpN4BkvXu+OU4/l+VUU2A6Ws1uF7DpHvAt8uE2z4R1s6AqVNwB7rcVK7s5qP4iDo0f8AJ/oJaLvsXhLU91RxIcg3BN9975343nQ6H5REfy8R0lOWsd6/5cR375zsS7SfJtOKkqZ0mn9H6pDLuO493C/X1ec04od82YxhNE0mzsQUPDivhKcrBUqK4ouKpkWxHGe7ETJmA3mRnEDqBMuaGeyHCNmOEiOIPD5zzbNwHzgE+oOAnthK22bgJ7zhuAgFieyuMSOsESVKoO4wDOIiABPq4nygT6uJDKyEREgqJjVayk90ylXHv0bcT9P+CRJ0rKzdRbNZiauqpPXuHjPMJ7i+EhxCGpUSmOvf3cT8ADNhikAaw3AADwAtOetrOLTtZHo+n/8AIJ//AD+dwPpNxNbosdOoeARfqT9RNlNsf+p1YVUC5SN1EzkGGbePjJ5qdC4EREEkGJGQ8ZXlust1MqSGUfIiIkECImFWsii7MqjizADwzhtLklJt0iDHhtUhDYm1jwtvPjYTmuVKUkVOcF2uxAsb2IAvkbAbxu4ToKmkKLe7UQ7xkwO8H/U5fl3WFRaWowazOTbOwNrGZaoN3q9Sv+POU6qXqcpjNlf+Vr2/vA+VjK8RNzRKtjZTCq9pnMaiXgkrRPWUieQBERAEREATEoJlAgGdCqb2PwlmQ06WdzJoAE+rifKBPq4kMrIRESComvxz3a3AS+TNDjqx3D3mNgPGZ5XsYZ3SSJ9CprO9Tv1V+/2k+O9/4CWsFh9nTVeAz7yd5lXSBs1/7ZElUKInHTjoy0QOix41G+Vl+0vyroxLUU71v6jrfeWppFbI2gqikZU2sRLsoS6u4eEsjSJlERJLCUnSxtLsq4utTGTOqneNZgL+cEMiiV+f0f1af7i/mOf0f1af7i/mVM7RYmr5RpegT2WU+fR+8uc/o/q0/wBxfzK2ksXRai42tP3Db+Yu8Zjr7pj1EdeGcfKfyNunyaMsZeGvmciIZusyLnNPtL6hIMbi6Ypt0l90jeOvKfIY4rI1HzS/fY+unkUE5eN/2NCTfPjnPJhtl7S+Ynm2XtL5ifb2j4/UvJtp7IucJ219QjnCdtfUJFoWiSYmkJjzhO2vqEc4Ttr6hFoWgaI4zzYd/wAplzhO2vqEc4Ttr6hFoWjHYd/yjYd8y5wnbX1COcJ219Qi0LQFITMCYc4Ttr6hHOE7a+oRaFokiR84Ttr6hHOE7a+oRaFokE+rifJRiE7a+oT6lz+j+rT/AHF/MNorJosRK/P6P6tP9xfzK+P03hqNJqtSsgRBc2YMfAKuZPVYSLRW0WcfVCob9eUpaNwbF9rUFupAeocTOB0jylr6QZKmFqbDDpi6FLpWFWuXqDXNr9FVWxPHdxn0o4+j+rT/AHF/Myj+aWp8diJRxtpp2/T78+hYms0wCchvYao+Jt95b5/R/Vp/uL+ZzmmuV2GpY6hh2JZn1CGXVNMbRynSa+VtW58RJyv8u3lfP+C2nHPabpfqvh6nVKtgAOoW8p7K/P6P6tP9xfzHP6P6tP8AcX8zSxaLEu09w8JrExlEkDa0/wBxfzNrJRaIiIklhI3pqd4B8ReSRAItgnZX0iNgnZX0iSxAItgnZX0iNgnZX0iSxAItgnZX0iNinZXyEliTqZFLwRbFOyvkI2KdlfISWJBJHsU7K+QjYp2V8hJIgEexTsr5CQYp6NJS9QoijezWAHxMtzmuXGFD4dSamzCVVcsaTVFyBA1kUG4uRvFpEnSszyycIOSXHw/j5otaP03g65K06iFgCbHIkAXLAHqt5S+rUmXWQK41SRq6p1gOzx4T5zh9XE1BqYpWNMs1hhdQsFvclxTQKtsrG4HEyQ0q9MYfUwz7XDagZxWUAK7FtkyX67gX67zNZPccMOtk46qteV347R1cbvd9uUdzorG061FajU9kWLAI4Aa6kjK+/deXaQpsAyhCDuIsQfAz5zgsG4qVL6Od6auHSkcSCcOxFnub3IbI2PZE2+jNOVKVJKVHRzBNVmQDEKbgG7MCRc5nf3yVPyXxdXsta9JN7f8ATv8AVbJs6rE1qFP32ppkT0iouBvOfVKuKx6KaYSltg9QIxQAilxZyNwE5PS2Er4qlVr1VVGFBdjRFRSVVmF6jNe2YDAH8S5o7FV8EKqc32q7Ta6y10XV11ACMjG6m9vG8ai3+S9W6qPmnfflVtw/0/Y6qnXoM2qrUy2fRBUnI2OQ4Ge1a1FbKzIpJAAJUElvdAB6z1ThqOCxNBjWVF2mxqGwqIRSevU6Ckk2Jsb8DI8Do8mts0VatRcRhqtSoKus9RWUuztc21QSLWz3b73jW/A/yp7LTT+P8bvZ/pXnY+ibFOyvkI2CdlfSJLE0O0i2CdlfSI2CdlfSJLEAj2KdlfITzYJ2V9IksQCLYJ2V9InuxTsr5CSRAItgnZX0iNgnZX0iSxAItgnZX0iSxEAREQBERAEREAREQBERAEREAREQBERAI3QEEEAgixFsiD1WkL4Kkda6KdYgtdQdYr7pPG3VEQxSlySJQVSSqgFveIABbx47zMKeDprq2RRqghbKBqg7wvAGIgaUeNg6ZvempuoU9EZquaqe4XOU9fB021r01OtbWuoOtq2tfjaw8p5EUNEfB4+DpHWuinWtrXUHX1fd1ss7dUypYWmhJVFUkKpIUAkKLKCR1AZDhEQg4pO6LMREAREQBERAEREAREQBERAEREA//9k="
                                            class="w-7 h-7"
                                        />
                                    </span>
                                                                <div>
                                      <span
                                          class="text-blue-darkest dark:text-d-black-70 text-xs"
                                      >وظایف من</span
                                      >
                                                                </div>
                                                            </div>
                                                        </a
                                                        >
                                                    </div>
                                                </div>
                                                <div
                                                    class="bg-white item dark:bg-d-black-10 w-sq rounded-xl text-center border"
                                                    style="border-color: rgb(0, 109, 166)"
                                                >
                                                    <div
                                                        class="group w-full h-full relative flex items-center justify-center"
                                                    >
                                                        <a
                                                            href="https://oi.dastyar.io/shabpin"
                                                            target="_self"
                                                        >
                                                            <div
                                                                class="flex flex-col items-center justify-center gap-y-2"
                                                            >
                                    <span
                                    >
                                        <img style="margin-top: 5px"
                                             src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxESEBUTEhIVFRUXERUVFhgYEhkYEhAXFRcaFhUSFRUYHyghGBolGxcVITEhJSkrLi4uGB8zODMtNygtLisBCgoKDg0OGxAQGi0lICUvLS0tLS0tKy0tLS0tLS0tLS0wLi0tLS0tLy0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIALcBEwMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAwQCBQYBB//EAD8QAAIBAgIFCQcCBgEEAwAAAAECAAMRBCEFEhMxUQYUIkFSYXGRkhUygaGxwdFTkwcjQmJy4fCCorLiJDNj/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECAwQFBv/EADARAAICAQMDAQYGAgMAAAAAAAABAhEDEiExBEFRoRNhgbHB8AUicZHR4RQyUoLx/9oADAMBAAIRAxEAPwD7jERAEREAREQBETB6gEAziVmxB6hMDWbjBGpFyJrqNVulmfeIkwrNKqVqyXs6LcSAV+IkquDuMsRZlERBIiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAnhNp7KdWpc90EN0DiNa4GVjb7/eYSKhvf8AzP0EllE7RElT+/AiIlipHh/6v8z9pJI8P/V/mftJJSHBfJ/sxI6fvt4L95JKmHxINZ1sbgDw6OR+sibScb8/Rkwi2pV2X1RfWsR3ywlQGU4mllLL8SolUjvkorjgYstaJokO3HfPDiO6LFoniVTWPhIySd5k2RqLbVAOuYbcStEiyLJjiDwnnOD3SKIFsl5we6ZDE90giBbLa1Qe6SShMkcjdFk6i7Eip1Qe4yWSWEREAREQBERAEREAirtYeMqybEnMeEhkFJEVDe/+Z+gksgwtQEvYg9Pj3D8GTykGmtvf8y2RVLf3fIRE5rSeIrGu66tQoNUKVViu4EnLfmT5TPPnWGKk1duvv9jXpuml1EnFOqV+qX1NnofGbTaZg2YHLq1r/ibKcsmHqjMI48FInQaOdjTGte+Yz35HK85+jzyktEluu/nf/wAOnr+njCWuPD7eNv6KWltLbKotIWuV1rndmSAo78jNbRxLLUaoD0mve+a5m+Q6t06CvgKNRtZ6asbWuR1Dq+ZmYwdECwpLIy9Nnnk1a9rtc7f2Ww9T0+PHp0O6qXG/rwRaNxZqJcixBseB75amFGiq5KAPCZzsxqSilJ2zz8ji5twVLwIiJcoIJiUai6zte5ta1mAt5yJOis5aS9rDiPONYcR5ylzZey3rWObL2W9aymt+DPXLx8/4LusOI84BlLmy9lvWshqrqkWuP+oH6Rra5QeVrlff7GziImhsIiIAiIgCTUa3UZDEkJl+JDQfqk0k0TEREAREQBERAKdY9IzXYrSiI2pmWABIH9N915fY3M1GL0Cr1TU2jKWtcAAjIAdfhOfqHmUV7JK77+PjsadOsLk/bNpU+L5+G/kp6NxpplywuGItq7xv338Zv6FYOoZdxlA6DS3/ANjE/wCIEuYTDimuqDfMnzmHSwzY/wAs+N/F82b9bPBkeuD/ADfGqqiaAYidxwGTOTvMxiIsCIiQBERAEExPKm4w3SsmKtpGAqiV61MhiQoYHjnaZROX2ra3OnJ0cZLZtepFZv0l8j+Ys36S+R/MmTfLMvBavv8Ao5MvSqDq/RFCzfpL5H8yJwSwBUKe4W3zaSGpSu6nxv8AaXcDCWHbb6ExiImh0CIiAIicBp7+KuEw9YU6aNiBbpOjAIMyCEJ9+1jmMu/fY3RDaXJ38TneSHLDD6RVzSDo1O2ujgawDX1WBBIIyPl4TooJTvg9RrEGXpQl1Nw8JKLRMoiJJYREQBIqzWX5SWVa7XPhBDZFERKlBERAEREARIKmMpq2qXAI6uuY8+pdsSSaLMStz+l2x84XH0iQA4uTYZGBRZnNcv8AlMdH4Taooao1QU6Ya+oGILazWzIAVsuvKdLNRyr0RSxWEqUqw6NtYHrVkzBH08CRKvhkU3suT5Ron+IGmmbaLT5whNtXm51O8KyWIPxM+0YN3akhqKEcopdQbhGIBZQeuxuLzjNDYSmrJRpKEUH3RkAL3Y/WdzMOnyvJbZvn6ZYKjqt9/wCu/nk8UWnsROgwe+7EREAREQBERAEREAq6UdVoVWY2UUahY9kBSSfKfMuVnJCjVw9Mo4pHD0tUMRdWRBezeGZv3njOv5acoaVFHw5TaNUpMrLewRXBXpHiQTl/qfPvabHCHCsCylCmuXu9juNyLXH2nL1Ck5Jx7FI/iPSYteLM+a7N7rtt97GH8DFbntYj3eam/iaiav0afbJ8G5GYsaNx9Ooz/wAirejUJy1A1irnqsGAN+F595m8ODLpskcmO4sS6m4eEpAS/NEdURERJLCInhNoBhWewlSZVHubzGQyjYiIkEFHSGN2ZThrgN4Nl/v4S9NRpjDhyVNwDY5d0uaLqs1JSxucwTxsxF/lKRe7RlCbc2mW4iJc1NFyloWK1B/ifqPvNXSqg+M6nSGH2lNl6yMvEZj5zjcfhKiMtMizOQALg3ubdRyzkosnsW0qA7s87d3w4ySKei8QosKeQ/uX8zJ8PVX3qZHxH5kktpG5OO1kFt5GfcesCVnz35+Mw0ZSJuGGqN+ds+4SKliemUORDEDvsfrObJd7nBmbUrvbsZ0sMFNwPmZbwtbVbuP/AC8injsALnIXtfqz3Skfy8EPLOUtcm2/L3NzEq4fFDVAY5/WWVqA7iD8Z0ppnXGafB7ERJLCIiAIiIAmu0/pVcLQaqczuRe2TuHh1nuBmxnKcvdD1cQtIockLBhew6VrN8rfGVnLTFsyzPIoP2SuXZe/g+cYqu9R2dySzEsSesmVqjWE6fTBw+2FOp0Vp0aajUzsxXWY/Ek598q4bRmCY54ot3ahX5m4nLKaR4C/Dsk8uhTjdtO5RTtc2m7e97q/fRy2nEvQbusfmPtefUP4RaaqYnR+rUJLUKmyDHeyBVZL94B1f+kTgeWOGp0adTZuChUap1rm+WWXXefR/wCF2gWwmj1FQEVKrms4O9NYAIh4HVUEjqJM2xO90el+H4Z4XPHNcNr3dvTwdlRW7S3IMMMryebo9VcCIiSSJXxD9XnJyZSY3kMiR5ERIKGFdyqkgXIG7j3TDCYlaiBl3HzU9YPfM6q3UjiJok/kOal2C63TXqscr24g5+cpKWl+4ynPRJXwW9N4sUyoI3pUa9+xY2+N/lNJorT5pUWtSuFNyxe2uztYZauWX0m25R6NqVwmpq5Bhmbe8VI6uCmcxpVNhhlRveNZta3FLjy3SWvzWWpKafd7fz6G8wfKvXYg0guV765Pw92XPb68PmfxOb0bowsoIZQSL2vdhfPNeqW6+j3UXFmA32yPkZHtIkLNjuja19OBlZR0SVIBF7rcbxlNFUpFmDNXYsLWJUkixuLG/GUm0igNjreUx9p0+/ympujaitVG7F1L9WRI+IJsfCbWrplG3jPiCfxOV9p0+/yj2nT7/KQ0nyHFNUzpfaNPv8/9SDE4tXy1rANfcSfOaH2pT7/KPadPv8pV4oNU0RCEYSUoqmu6s6KljlAtcnxvf6SanpVACCAwIsQb/icv7Tp9/lHtOn3+ULHFcIj2cdWqt/v4G+GIpDcWA4Bzb/xmaYxB/U58T/6znvadPv8AKPadP+7yj2cfBX2OP/idNg9JJSvqliOBa4HeOjlLXt9ez8z+Jx/tJP7vTPfaKcG9MmMFFUkbTbm9Ut2df7fXs/M/iXtHY0VVJAtY2+V/vOC9oJwb0zt9C4NqSENa5a+RvlYD7SSjSRsYiJBUTxlBFjmDPYEA+L6fa+LrcBUIHgpIHyE1zC87fSfI7Xqu6VrazM1jT3XJO8H7SgeRdbqqJ/3fic3B8tm6fPLJKVXbb7d2avkfhVbH0FqIHXXJsRcXVSym3cwB+E+yz59yU5PGni0Z3uU1jYDr1SMyerOfQZtj4PZ/DccoYmpKt/oi7TFgPCZRE1PUEREAixB6MqyxidwleQyj5EREggTW6boKaTZgEgjMgaxtlv65njaFZv6yF4KQD8Sc/K0oUtFapJ1WYntZ/WZzle1GGSVrTpN4BkvXu+OU4/l+VUU2A6Ws1uF7DpHvAt8uE2z4R1s6AqVNwB7rcVK7s5qP4iDo0f8AJ/oJaLvsXhLU91RxIcg3BN9975343nQ6H5REfy8R0lOWsd6/5cR375zsS7SfJtOKkqZ0mn9H6pDLuO493C/X1ec04od82YxhNE0mzsQUPDivhKcrBUqK4ouKpkWxHGe7ETJmA3mRnEDqBMuaGeyHCNmOEiOIPD5zzbNwHzgE+oOAnthK22bgJ7zhuAgFieyuMSOsESVKoO4wDOIiABPq4nygT6uJDKyEREgqJjVayk90ylXHv0bcT9P+CRJ0rKzdRbNZiauqpPXuHjPMJ7i+EhxCGpUSmOvf3cT8ADNhikAaw3AADwAtOetrOLTtZHo+n/8AIJ//AD+dwPpNxNbosdOoeARfqT9RNlNsf+p1YVUC5SN1EzkGGbePjJ5qdC4EREEkGJGQ8ZXlust1MqSGUfIiIkECImFWsii7MqjizADwzhtLklJt0iDHhtUhDYm1jwtvPjYTmuVKUkVOcF2uxAsb2IAvkbAbxu4ToKmkKLe7UQ7xkwO8H/U5fl3WFRaWowazOTbOwNrGZaoN3q9Sv+POU6qXqcpjNlf+Vr2/vA+VjK8RNzRKtjZTCq9pnMaiXgkrRPWUieQBERAEREATEoJlAgGdCqb2PwlmQ06WdzJoAE+rifKBPq4kMrIRESComvxz3a3AS+TNDjqx3D3mNgPGZ5XsYZ3SSJ9CprO9Tv1V+/2k+O9/4CWsFh9nTVeAz7yd5lXSBs1/7ZElUKInHTjoy0QOix41G+Vl+0vyroxLUU71v6jrfeWppFbI2gqikZU2sRLsoS6u4eEsjSJlERJLCUnSxtLsq4utTGTOqneNZgL+cEMiiV+f0f1af7i/mOf0f1af7i/mVM7RYmr5RpegT2WU+fR+8uc/o/q0/wBxfzK2ksXRai42tP3Db+Yu8Zjr7pj1EdeGcfKfyNunyaMsZeGvmciIZusyLnNPtL6hIMbi6Ypt0l90jeOvKfIY4rI1HzS/fY+unkUE5eN/2NCTfPjnPJhtl7S+Ynm2XtL5ifb2j4/UvJtp7IucJ219QjnCdtfUJFoWiSYmkJjzhO2vqEc4Ttr6hFoWgaI4zzYd/wAplzhO2vqEc4Ttr6hFoWjHYd/yjYd8y5wnbX1COcJ219Qi0LQFITMCYc4Ttr6hHOE7a+oRaFokiR84Ttr6hHOE7a+oRaFokE+rifJRiE7a+oT6lz+j+rT/AHF/MNorJosRK/P6P6tP9xfzK+P03hqNJqtSsgRBc2YMfAKuZPVYSLRW0WcfVCob9eUpaNwbF9rUFupAeocTOB0jylr6QZKmFqbDDpi6FLpWFWuXqDXNr9FVWxPHdxn0o4+j+rT/AHF/Myj+aWp8diJRxtpp2/T78+hYms0wCchvYao+Jt95b5/R/Vp/uL+ZzmmuV2GpY6hh2JZn1CGXVNMbRynSa+VtW58RJyv8u3lfP+C2nHPabpfqvh6nVKtgAOoW8p7K/P6P6tP9xfzHP6P6tP8AcX8zSxaLEu09w8JrExlEkDa0/wBxfzNrJRaIiIklhI3pqd4B8ReSRAItgnZX0iNgnZX0iSxAItgnZX0iNgnZX0iSxAItgnZX0iNinZXyEliTqZFLwRbFOyvkI2KdlfISWJBJHsU7K+QjYp2V8hJIgEexTsr5CQYp6NJS9QoijezWAHxMtzmuXGFD4dSamzCVVcsaTVFyBA1kUG4uRvFpEnSszyycIOSXHw/j5otaP03g65K06iFgCbHIkAXLAHqt5S+rUmXWQK41SRq6p1gOzx4T5zh9XE1BqYpWNMs1hhdQsFvclxTQKtsrG4HEyQ0q9MYfUwz7XDagZxWUAK7FtkyX67gX67zNZPccMOtk46qteV347R1cbvd9uUdzorG061FajU9kWLAI4Aa6kjK+/deXaQpsAyhCDuIsQfAz5zgsG4qVL6Od6auHSkcSCcOxFnub3IbI2PZE2+jNOVKVJKVHRzBNVmQDEKbgG7MCRc5nf3yVPyXxdXsta9JN7f8ATv8AVbJs6rE1qFP32ppkT0iouBvOfVKuKx6KaYSltg9QIxQAilxZyNwE5PS2Er4qlVr1VVGFBdjRFRSVVmF6jNe2YDAH8S5o7FV8EKqc32q7Ta6y10XV11ACMjG6m9vG8ai3+S9W6qPmnfflVtw/0/Y6qnXoM2qrUy2fRBUnI2OQ4Ge1a1FbKzIpJAAJUElvdAB6z1ThqOCxNBjWVF2mxqGwqIRSevU6Ckk2Jsb8DI8Do8mts0VatRcRhqtSoKus9RWUuztc21QSLWz3b73jW/A/yp7LTT+P8bvZ/pXnY+ibFOyvkI2CdlfSJLE0O0i2CdlfSI2CdlfSJLEAj2KdlfITzYJ2V9IksQCLYJ2V9InuxTsr5CSRAItgnZX0iNgnZX0iSxAItgnZX0iSxEAREQBERAEREAREQBERAEREAREQBERAI3QEEEAgixFsiD1WkL4Kkda6KdYgtdQdYr7pPG3VEQxSlySJQVSSqgFveIABbx47zMKeDprq2RRqghbKBqg7wvAGIgaUeNg6ZvempuoU9EZquaqe4XOU9fB021r01OtbWuoOtq2tfjaw8p5EUNEfB4+DpHWuinWtrXUHX1fd1ss7dUypYWmhJVFUkKpIUAkKLKCR1AZDhEQg4pO6LMREAREQBERAEREAREQBERAEREA//9k="
                                             class="w-7 h-7"
                                        />
                                    </span>
                                                                <div>
                                      <span
                                          class="text-blue-darkest dark:text-d-black-70 text-xs"
                                      > پروژه های من</span
                                      >
                                                                </div>
                                                            </div>
                                                        </a
                                                        >
                                                    </div>
                                                </div>
                                                <div
                                                    class="bg-white item dark:bg-d-black-10 w-sq rounded-xl text-center border"
                                                    style="border-color: rgb(0, 109, 166)"
                                                >
                                                    <div
                                                        class="group w-full h-full relative flex items-center justify-center"
                                                    >
                                                        <a
                                                            href="https://oi.dastyar.io/shabpin"
                                                            target="_self"
                                                        >
                                                            <div
                                                                class="flex flex-col items-center justify-center gap-y-2"
                                                            >
                                    <span
                                    >
                                        <img style="margin-top: 5px"
                                             src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxESEBUTEhIVFRUXERUVFhgYEhkYEhAXFRcaFhUSFRUYHyghGBolGxcVITEhJSkrLi4uGB8zODMtNygtLisBCgoKDg0OGxAQGi0lICUvLS0tLS0tKy0tLS0tLS0tLS0wLi0tLS0tLy0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIALcBEwMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAwQCBQYBB//EAD8QAAIBAgIFCQcCBgEEAwAAAAECAAMRBCEFEhMxUQYUIkFSYXGRkhUygaGxwdFTkwcjQmJy4fCCorLiJDNj/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECAwQFBv/EADARAAICAQMDAQYGAgMAAAAAAAABAhEDEiExBEFRoRNhgbHB8AUicZHR4RQyUoLx/9oADAMBAAIRAxEAPwD7jERAEREAREQBETB6gEAziVmxB6hMDWbjBGpFyJrqNVulmfeIkwrNKqVqyXs6LcSAV+IkquDuMsRZlERBIiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAnhNp7KdWpc90EN0DiNa4GVjb7/eYSKhvf8AzP0EllE7RElT+/AiIlipHh/6v8z9pJI8P/V/mftJJSHBfJ/sxI6fvt4L95JKmHxINZ1sbgDw6OR+sibScb8/Rkwi2pV2X1RfWsR3ywlQGU4mllLL8SolUjvkorjgYstaJokO3HfPDiO6LFoniVTWPhIySd5k2RqLbVAOuYbcStEiyLJjiDwnnOD3SKIFsl5we6ZDE90giBbLa1Qe6SShMkcjdFk6i7Eip1Qe4yWSWEREAREQBERAEREAirtYeMqybEnMeEhkFJEVDe/+Z+gksgwtQEvYg9Pj3D8GTykGmtvf8y2RVLf3fIRE5rSeIrGu66tQoNUKVViu4EnLfmT5TPPnWGKk1duvv9jXpuml1EnFOqV+qX1NnofGbTaZg2YHLq1r/ibKcsmHqjMI48FInQaOdjTGte+Yz35HK85+jzyktEluu/nf/wAOnr+njCWuPD7eNv6KWltLbKotIWuV1rndmSAo78jNbRxLLUaoD0mve+a5m+Q6t06CvgKNRtZ6asbWuR1Dq+ZmYwdECwpLIy9Nnnk1a9rtc7f2Ww9T0+PHp0O6qXG/rwRaNxZqJcixBseB75amFGiq5KAPCZzsxqSilJ2zz8ji5twVLwIiJcoIJiUai6zte5ta1mAt5yJOis5aS9rDiPONYcR5ylzZey3rWObL2W9aymt+DPXLx8/4LusOI84BlLmy9lvWshqrqkWuP+oH6Rra5QeVrlff7GziImhsIiIAiIgCTUa3UZDEkJl+JDQfqk0k0TEREAREQBERAKdY9IzXYrSiI2pmWABIH9N915fY3M1GL0Cr1TU2jKWtcAAjIAdfhOfqHmUV7JK77+PjsadOsLk/bNpU+L5+G/kp6NxpplywuGItq7xv338Zv6FYOoZdxlA6DS3/ANjE/wCIEuYTDimuqDfMnzmHSwzY/wAs+N/F82b9bPBkeuD/ADfGqqiaAYidxwGTOTvMxiIsCIiQBERAEExPKm4w3SsmKtpGAqiV61MhiQoYHjnaZROX2ra3OnJ0cZLZtepFZv0l8j+Ys36S+R/MmTfLMvBavv8Ao5MvSqDq/RFCzfpL5H8yJwSwBUKe4W3zaSGpSu6nxv8AaXcDCWHbb6ExiImh0CIiAIicBp7+KuEw9YU6aNiBbpOjAIMyCEJ9+1jmMu/fY3RDaXJ38TneSHLDD6RVzSDo1O2ujgawDX1WBBIIyPl4TooJTvg9RrEGXpQl1Nw8JKLRMoiJJYREQBIqzWX5SWVa7XPhBDZFERKlBERAEREARIKmMpq2qXAI6uuY8+pdsSSaLMStz+l2x84XH0iQA4uTYZGBRZnNcv8AlMdH4Taooao1QU6Ya+oGILazWzIAVsuvKdLNRyr0RSxWEqUqw6NtYHrVkzBH08CRKvhkU3suT5Ron+IGmmbaLT5whNtXm51O8KyWIPxM+0YN3akhqKEcopdQbhGIBZQeuxuLzjNDYSmrJRpKEUH3RkAL3Y/WdzMOnyvJbZvn6ZYKjqt9/wCu/nk8UWnsROgwe+7EREAREQBERAEREAq6UdVoVWY2UUahY9kBSSfKfMuVnJCjVw9Mo4pHD0tUMRdWRBezeGZv3njOv5acoaVFHw5TaNUpMrLewRXBXpHiQTl/qfPvabHCHCsCylCmuXu9juNyLXH2nL1Ck5Jx7FI/iPSYteLM+a7N7rtt97GH8DFbntYj3eam/iaiav0afbJ8G5GYsaNx9Ooz/wAirejUJy1A1irnqsGAN+F595m8ODLpskcmO4sS6m4eEpAS/NEdURERJLCInhNoBhWewlSZVHubzGQyjYiIkEFHSGN2ZThrgN4Nl/v4S9NRpjDhyVNwDY5d0uaLqs1JSxucwTxsxF/lKRe7RlCbc2mW4iJc1NFyloWK1B/ifqPvNXSqg+M6nSGH2lNl6yMvEZj5zjcfhKiMtMizOQALg3ubdRyzkosnsW0qA7s87d3w4ySKei8QosKeQ/uX8zJ8PVX3qZHxH5kktpG5OO1kFt5GfcesCVnz35+Mw0ZSJuGGqN+ds+4SKliemUORDEDvsfrObJd7nBmbUrvbsZ0sMFNwPmZbwtbVbuP/AC8injsALnIXtfqz3Skfy8EPLOUtcm2/L3NzEq4fFDVAY5/WWVqA7iD8Z0ppnXGafB7ERJLCIiAIiIAmu0/pVcLQaqczuRe2TuHh1nuBmxnKcvdD1cQtIockLBhew6VrN8rfGVnLTFsyzPIoP2SuXZe/g+cYqu9R2dySzEsSesmVqjWE6fTBw+2FOp0Vp0aajUzsxXWY/Ek598q4bRmCY54ot3ahX5m4nLKaR4C/Dsk8uhTjdtO5RTtc2m7e97q/fRy2nEvQbusfmPtefUP4RaaqYnR+rUJLUKmyDHeyBVZL94B1f+kTgeWOGp0adTZuChUap1rm+WWXXefR/wCF2gWwmj1FQEVKrms4O9NYAIh4HVUEjqJM2xO90el+H4Z4XPHNcNr3dvTwdlRW7S3IMMMryebo9VcCIiSSJXxD9XnJyZSY3kMiR5ERIKGFdyqkgXIG7j3TDCYlaiBl3HzU9YPfM6q3UjiJok/kOal2C63TXqscr24g5+cpKWl+4ynPRJXwW9N4sUyoI3pUa9+xY2+N/lNJorT5pUWtSuFNyxe2uztYZauWX0m25R6NqVwmpq5Bhmbe8VI6uCmcxpVNhhlRveNZta3FLjy3SWvzWWpKafd7fz6G8wfKvXYg0guV765Pw92XPb68PmfxOb0bowsoIZQSL2vdhfPNeqW6+j3UXFmA32yPkZHtIkLNjuja19OBlZR0SVIBF7rcbxlNFUpFmDNXYsLWJUkixuLG/GUm0igNjreUx9p0+/ympujaitVG7F1L9WRI+IJsfCbWrplG3jPiCfxOV9p0+/yj2nT7/KQ0nyHFNUzpfaNPv8/9SDE4tXy1rANfcSfOaH2pT7/KPadPv8pV4oNU0RCEYSUoqmu6s6KljlAtcnxvf6SanpVACCAwIsQb/icv7Tp9/lHtOn3+ULHFcIj2cdWqt/v4G+GIpDcWA4Bzb/xmaYxB/U58T/6znvadPv8AKPadP+7yj2cfBX2OP/idNg9JJSvqliOBa4HeOjlLXt9ez8z+Jx/tJP7vTPfaKcG9MmMFFUkbTbm9Ut2df7fXs/M/iXtHY0VVJAtY2+V/vOC9oJwb0zt9C4NqSENa5a+RvlYD7SSjSRsYiJBUTxlBFjmDPYEA+L6fa+LrcBUIHgpIHyE1zC87fSfI7Xqu6VrazM1jT3XJO8H7SgeRdbqqJ/3fic3B8tm6fPLJKVXbb7d2avkfhVbH0FqIHXXJsRcXVSym3cwB+E+yz59yU5PGni0Z3uU1jYDr1SMyerOfQZtj4PZ/DccoYmpKt/oi7TFgPCZRE1PUEREAixB6MqyxidwleQyj5EREggTW6boKaTZgEgjMgaxtlv65njaFZv6yF4KQD8Sc/K0oUtFapJ1WYntZ/WZzle1GGSVrTpN4BkvXu+OU4/l+VUU2A6Ws1uF7DpHvAt8uE2z4R1s6AqVNwB7rcVK7s5qP4iDo0f8AJ/oJaLvsXhLU91RxIcg3BN9975343nQ6H5REfy8R0lOWsd6/5cR375zsS7SfJtOKkqZ0mn9H6pDLuO493C/X1ec04od82YxhNE0mzsQUPDivhKcrBUqK4ouKpkWxHGe7ETJmA3mRnEDqBMuaGeyHCNmOEiOIPD5zzbNwHzgE+oOAnthK22bgJ7zhuAgFieyuMSOsESVKoO4wDOIiABPq4nygT6uJDKyEREgqJjVayk90ylXHv0bcT9P+CRJ0rKzdRbNZiauqpPXuHjPMJ7i+EhxCGpUSmOvf3cT8ADNhikAaw3AADwAtOetrOLTtZHo+n/8AIJ//AD+dwPpNxNbosdOoeARfqT9RNlNsf+p1YVUC5SN1EzkGGbePjJ5qdC4EREEkGJGQ8ZXlust1MqSGUfIiIkECImFWsii7MqjizADwzhtLklJt0iDHhtUhDYm1jwtvPjYTmuVKUkVOcF2uxAsb2IAvkbAbxu4ToKmkKLe7UQ7xkwO8H/U5fl3WFRaWowazOTbOwNrGZaoN3q9Sv+POU6qXqcpjNlf+Vr2/vA+VjK8RNzRKtjZTCq9pnMaiXgkrRPWUieQBERAEREATEoJlAgGdCqb2PwlmQ06WdzJoAE+rifKBPq4kMrIRESComvxz3a3AS+TNDjqx3D3mNgPGZ5XsYZ3SSJ9CprO9Tv1V+/2k+O9/4CWsFh9nTVeAz7yd5lXSBs1/7ZElUKInHTjoy0QOix41G+Vl+0vyroxLUU71v6jrfeWppFbI2gqikZU2sRLsoS6u4eEsjSJlERJLCUnSxtLsq4utTGTOqneNZgL+cEMiiV+f0f1af7i/mOf0f1af7i/mVM7RYmr5RpegT2WU+fR+8uc/o/q0/wBxfzK2ksXRai42tP3Db+Yu8Zjr7pj1EdeGcfKfyNunyaMsZeGvmciIZusyLnNPtL6hIMbi6Ypt0l90jeOvKfIY4rI1HzS/fY+unkUE5eN/2NCTfPjnPJhtl7S+Ynm2XtL5ifb2j4/UvJtp7IucJ219QjnCdtfUJFoWiSYmkJjzhO2vqEc4Ttr6hFoWgaI4zzYd/wAplzhO2vqEc4Ttr6hFoWjHYd/yjYd8y5wnbX1COcJ219Qi0LQFITMCYc4Ttr6hHOE7a+oRaFokiR84Ttr6hHOE7a+oRaFokE+rifJRiE7a+oT6lz+j+rT/AHF/MNorJosRK/P6P6tP9xfzK+P03hqNJqtSsgRBc2YMfAKuZPVYSLRW0WcfVCob9eUpaNwbF9rUFupAeocTOB0jylr6QZKmFqbDDpi6FLpWFWuXqDXNr9FVWxPHdxn0o4+j+rT/AHF/Myj+aWp8diJRxtpp2/T78+hYms0wCchvYao+Jt95b5/R/Vp/uL+ZzmmuV2GpY6hh2JZn1CGXVNMbRynSa+VtW58RJyv8u3lfP+C2nHPabpfqvh6nVKtgAOoW8p7K/P6P6tP9xfzHP6P6tP8AcX8zSxaLEu09w8JrExlEkDa0/wBxfzNrJRaIiIklhI3pqd4B8ReSRAItgnZX0iNgnZX0iSxAItgnZX0iNgnZX0iSxAItgnZX0iNinZXyEliTqZFLwRbFOyvkI2KdlfISWJBJHsU7K+QjYp2V8hJIgEexTsr5CQYp6NJS9QoijezWAHxMtzmuXGFD4dSamzCVVcsaTVFyBA1kUG4uRvFpEnSszyycIOSXHw/j5otaP03g65K06iFgCbHIkAXLAHqt5S+rUmXWQK41SRq6p1gOzx4T5zh9XE1BqYpWNMs1hhdQsFvclxTQKtsrG4HEyQ0q9MYfUwz7XDagZxWUAK7FtkyX67gX67zNZPccMOtk46qteV347R1cbvd9uUdzorG061FajU9kWLAI4Aa6kjK+/deXaQpsAyhCDuIsQfAz5zgsG4qVL6Od6auHSkcSCcOxFnub3IbI2PZE2+jNOVKVJKVHRzBNVmQDEKbgG7MCRc5nf3yVPyXxdXsta9JN7f8ATv8AVbJs6rE1qFP32ppkT0iouBvOfVKuKx6KaYSltg9QIxQAilxZyNwE5PS2Er4qlVr1VVGFBdjRFRSVVmF6jNe2YDAH8S5o7FV8EKqc32q7Ta6y10XV11ACMjG6m9vG8ai3+S9W6qPmnfflVtw/0/Y6qnXoM2qrUy2fRBUnI2OQ4Ge1a1FbKzIpJAAJUElvdAB6z1ThqOCxNBjWVF2mxqGwqIRSevU6Ckk2Jsb8DI8Do8mts0VatRcRhqtSoKus9RWUuztc21QSLWz3b73jW/A/yp7LTT+P8bvZ/pXnY+ibFOyvkI2CdlfSJLE0O0i2CdlfSI2CdlfSJLEAj2KdlfITzYJ2V9IksQCLYJ2V9InuxTsr5CSRAItgnZX0iNgnZX0iSxAItgnZX0iSxEAREQBERAEREAREQBERAEREAREQBERAI3QEEEAgixFsiD1WkL4Kkda6KdYgtdQdYr7pPG3VEQxSlySJQVSSqgFveIABbx47zMKeDprq2RRqghbKBqg7wvAGIgaUeNg6ZvempuoU9EZquaqe4XOU9fB021r01OtbWuoOtq2tfjaw8p5EUNEfB4+DpHWuinWtrXUHX1fd1ss7dUypYWmhJVFUkKpIUAkKLKCR1AZDhEQg4pO6LMREAREQBERAEREAREQBERAEREA//9k="
                                             class="w-7 h-7"
                                        />
                                    </span>
                                                                <div>
                                      <span
                                          class="text-blue-darkest dark:text-d-black-70 text-xs"
                                      >وظایف من</span
                                      >
                                                                </div>
                                                            </div>
                                                        </a
                                                        >
                                                    </div>
                                                </div>
                                                <div
                                                    class="bg-white item dark:bg-d-black-10 w-sq rounded-xl text-center border"
                                                    style="border-color: rgb(0, 109, 166)"
                                                >
                                                    <div
                                                        class="group w-full h-full relative flex items-center justify-center"
                                                    >
                                                        <a
                                                            href="https://oi.dastyar.io/shabpin"
                                                            target="_self"
                                                        >
                                                            <div
                                                                class="flex flex-col items-center justify-center gap-y-2"
                                                            >
                                    <span
                                    >
                                        <img style="margin-top: 5px"
                                             src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxESEBUTEhIVFRUXERUVFhgYEhkYEhAXFRcaFhUSFRUYHyghGBolGxcVITEhJSkrLi4uGB8zODMtNygtLisBCgoKDg0OGxAQGi0lICUvLS0tLS0tKy0tLS0tLS0tLS0wLi0tLS0tLy0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIALcBEwMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAwQCBQYBB//EAD8QAAIBAgIFCQcCBgEEAwAAAAECAAMRBCEFEhMxUQYUIkFSYXGRkhUygaGxwdFTkwcjQmJy4fCCorLiJDNj/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECAwQFBv/EADARAAICAQMDAQYGAgMAAAAAAAABAhEDEiExBEFRoRNhgbHB8AUicZHR4RQyUoLx/9oADAMBAAIRAxEAPwD7jERAEREAREQBETB6gEAziVmxB6hMDWbjBGpFyJrqNVulmfeIkwrNKqVqyXs6LcSAV+IkquDuMsRZlERBIiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAnhNp7KdWpc90EN0DiNa4GVjb7/eYSKhvf8AzP0EllE7RElT+/AiIlipHh/6v8z9pJI8P/V/mftJJSHBfJ/sxI6fvt4L95JKmHxINZ1sbgDw6OR+sibScb8/Rkwi2pV2X1RfWsR3ywlQGU4mllLL8SolUjvkorjgYstaJokO3HfPDiO6LFoniVTWPhIySd5k2RqLbVAOuYbcStEiyLJjiDwnnOD3SKIFsl5we6ZDE90giBbLa1Qe6SShMkcjdFk6i7Eip1Qe4yWSWEREAREQBERAEREAirtYeMqybEnMeEhkFJEVDe/+Z+gksgwtQEvYg9Pj3D8GTykGmtvf8y2RVLf3fIRE5rSeIrGu66tQoNUKVViu4EnLfmT5TPPnWGKk1duvv9jXpuml1EnFOqV+qX1NnofGbTaZg2YHLq1r/ibKcsmHqjMI48FInQaOdjTGte+Yz35HK85+jzyktEluu/nf/wAOnr+njCWuPD7eNv6KWltLbKotIWuV1rndmSAo78jNbRxLLUaoD0mve+a5m+Q6t06CvgKNRtZ6asbWuR1Dq+ZmYwdECwpLIy9Nnnk1a9rtc7f2Ww9T0+PHp0O6qXG/rwRaNxZqJcixBseB75amFGiq5KAPCZzsxqSilJ2zz8ji5twVLwIiJcoIJiUai6zte5ta1mAt5yJOis5aS9rDiPONYcR5ylzZey3rWObL2W9aymt+DPXLx8/4LusOI84BlLmy9lvWshqrqkWuP+oH6Rra5QeVrlff7GziImhsIiIAiIgCTUa3UZDEkJl+JDQfqk0k0TEREAREQBERAKdY9IzXYrSiI2pmWABIH9N915fY3M1GL0Cr1TU2jKWtcAAjIAdfhOfqHmUV7JK77+PjsadOsLk/bNpU+L5+G/kp6NxpplywuGItq7xv338Zv6FYOoZdxlA6DS3/ANjE/wCIEuYTDimuqDfMnzmHSwzY/wAs+N/F82b9bPBkeuD/ADfGqqiaAYidxwGTOTvMxiIsCIiQBERAEExPKm4w3SsmKtpGAqiV61MhiQoYHjnaZROX2ra3OnJ0cZLZtepFZv0l8j+Ys36S+R/MmTfLMvBavv8Ao5MvSqDq/RFCzfpL5H8yJwSwBUKe4W3zaSGpSu6nxv8AaXcDCWHbb6ExiImh0CIiAIicBp7+KuEw9YU6aNiBbpOjAIMyCEJ9+1jmMu/fY3RDaXJ38TneSHLDD6RVzSDo1O2ujgawDX1WBBIIyPl4TooJTvg9RrEGXpQl1Nw8JKLRMoiJJYREQBIqzWX5SWVa7XPhBDZFERKlBERAEREARIKmMpq2qXAI6uuY8+pdsSSaLMStz+l2x84XH0iQA4uTYZGBRZnNcv8AlMdH4Taooao1QU6Ya+oGILazWzIAVsuvKdLNRyr0RSxWEqUqw6NtYHrVkzBH08CRKvhkU3suT5Ron+IGmmbaLT5whNtXm51O8KyWIPxM+0YN3akhqKEcopdQbhGIBZQeuxuLzjNDYSmrJRpKEUH3RkAL3Y/WdzMOnyvJbZvn6ZYKjqt9/wCu/nk8UWnsROgwe+7EREAREQBERAEREAq6UdVoVWY2UUahY9kBSSfKfMuVnJCjVw9Mo4pHD0tUMRdWRBezeGZv3njOv5acoaVFHw5TaNUpMrLewRXBXpHiQTl/qfPvabHCHCsCylCmuXu9juNyLXH2nL1Ck5Jx7FI/iPSYteLM+a7N7rtt97GH8DFbntYj3eam/iaiav0afbJ8G5GYsaNx9Ooz/wAirejUJy1A1irnqsGAN+F595m8ODLpskcmO4sS6m4eEpAS/NEdURERJLCInhNoBhWewlSZVHubzGQyjYiIkEFHSGN2ZThrgN4Nl/v4S9NRpjDhyVNwDY5d0uaLqs1JSxucwTxsxF/lKRe7RlCbc2mW4iJc1NFyloWK1B/ifqPvNXSqg+M6nSGH2lNl6yMvEZj5zjcfhKiMtMizOQALg3ubdRyzkosnsW0qA7s87d3w4ySKei8QosKeQ/uX8zJ8PVX3qZHxH5kktpG5OO1kFt5GfcesCVnz35+Mw0ZSJuGGqN+ds+4SKliemUORDEDvsfrObJd7nBmbUrvbsZ0sMFNwPmZbwtbVbuP/AC8injsALnIXtfqz3Skfy8EPLOUtcm2/L3NzEq4fFDVAY5/WWVqA7iD8Z0ppnXGafB7ERJLCIiAIiIAmu0/pVcLQaqczuRe2TuHh1nuBmxnKcvdD1cQtIockLBhew6VrN8rfGVnLTFsyzPIoP2SuXZe/g+cYqu9R2dySzEsSesmVqjWE6fTBw+2FOp0Vp0aajUzsxXWY/Ek598q4bRmCY54ot3ahX5m4nLKaR4C/Dsk8uhTjdtO5RTtc2m7e97q/fRy2nEvQbusfmPtefUP4RaaqYnR+rUJLUKmyDHeyBVZL94B1f+kTgeWOGp0adTZuChUap1rm+WWXXefR/wCF2gWwmj1FQEVKrms4O9NYAIh4HVUEjqJM2xO90el+H4Z4XPHNcNr3dvTwdlRW7S3IMMMryebo9VcCIiSSJXxD9XnJyZSY3kMiR5ERIKGFdyqkgXIG7j3TDCYlaiBl3HzU9YPfM6q3UjiJok/kOal2C63TXqscr24g5+cpKWl+4ynPRJXwW9N4sUyoI3pUa9+xY2+N/lNJorT5pUWtSuFNyxe2uztYZauWX0m25R6NqVwmpq5Bhmbe8VI6uCmcxpVNhhlRveNZta3FLjy3SWvzWWpKafd7fz6G8wfKvXYg0guV765Pw92XPb68PmfxOb0bowsoIZQSL2vdhfPNeqW6+j3UXFmA32yPkZHtIkLNjuja19OBlZR0SVIBF7rcbxlNFUpFmDNXYsLWJUkixuLG/GUm0igNjreUx9p0+/ympujaitVG7F1L9WRI+IJsfCbWrplG3jPiCfxOV9p0+/yj2nT7/KQ0nyHFNUzpfaNPv8/9SDE4tXy1rANfcSfOaH2pT7/KPadPv8pV4oNU0RCEYSUoqmu6s6KljlAtcnxvf6SanpVACCAwIsQb/icv7Tp9/lHtOn3+ULHFcIj2cdWqt/v4G+GIpDcWA4Bzb/xmaYxB/U58T/6znvadPv8AKPadP+7yj2cfBX2OP/idNg9JJSvqliOBa4HeOjlLXt9ez8z+Jx/tJP7vTPfaKcG9MmMFFUkbTbm9Ut2df7fXs/M/iXtHY0VVJAtY2+V/vOC9oJwb0zt9C4NqSENa5a+RvlYD7SSjSRsYiJBUTxlBFjmDPYEA+L6fa+LrcBUIHgpIHyE1zC87fSfI7Xqu6VrazM1jT3XJO8H7SgeRdbqqJ/3fic3B8tm6fPLJKVXbb7d2avkfhVbH0FqIHXXJsRcXVSym3cwB+E+yz59yU5PGni0Z3uU1jYDr1SMyerOfQZtj4PZ/DccoYmpKt/oi7TFgPCZRE1PUEREAixB6MqyxidwleQyj5EREggTW6boKaTZgEgjMgaxtlv65njaFZv6yF4KQD8Sc/K0oUtFapJ1WYntZ/WZzle1GGSVrTpN4BkvXu+OU4/l+VUU2A6Ws1uF7DpHvAt8uE2z4R1s6AqVNwB7rcVK7s5qP4iDo0f8AJ/oJaLvsXhLU91RxIcg3BN9975343nQ6H5REfy8R0lOWsd6/5cR375zsS7SfJtOKkqZ0mn9H6pDLuO493C/X1ec04od82YxhNE0mzsQUPDivhKcrBUqK4ouKpkWxHGe7ETJmA3mRnEDqBMuaGeyHCNmOEiOIPD5zzbNwHzgE+oOAnthK22bgJ7zhuAgFieyuMSOsESVKoO4wDOIiABPq4nygT6uJDKyEREgqJjVayk90ylXHv0bcT9P+CRJ0rKzdRbNZiauqpPXuHjPMJ7i+EhxCGpUSmOvf3cT8ADNhikAaw3AADwAtOetrOLTtZHo+n/8AIJ//AD+dwPpNxNbosdOoeARfqT9RNlNsf+p1YVUC5SN1EzkGGbePjJ5qdC4EREEkGJGQ8ZXlust1MqSGUfIiIkECImFWsii7MqjizADwzhtLklJt0iDHhtUhDYm1jwtvPjYTmuVKUkVOcF2uxAsb2IAvkbAbxu4ToKmkKLe7UQ7xkwO8H/U5fl3WFRaWowazOTbOwNrGZaoN3q9Sv+POU6qXqcpjNlf+Vr2/vA+VjK8RNzRKtjZTCq9pnMaiXgkrRPWUieQBERAEREATEoJlAgGdCqb2PwlmQ06WdzJoAE+rifKBPq4kMrIRESComvxz3a3AS+TNDjqx3D3mNgPGZ5XsYZ3SSJ9CprO9Tv1V+/2k+O9/4CWsFh9nTVeAz7yd5lXSBs1/7ZElUKInHTjoy0QOix41G+Vl+0vyroxLUU71v6jrfeWppFbI2gqikZU2sRLsoS6u4eEsjSJlERJLCUnSxtLsq4utTGTOqneNZgL+cEMiiV+f0f1af7i/mOf0f1af7i/mVM7RYmr5RpegT2WU+fR+8uc/o/q0/wBxfzK2ksXRai42tP3Db+Yu8Zjr7pj1EdeGcfKfyNunyaMsZeGvmciIZusyLnNPtL6hIMbi6Ypt0l90jeOvKfIY4rI1HzS/fY+unkUE5eN/2NCTfPjnPJhtl7S+Ynm2XtL5ifb2j4/UvJtp7IucJ219QjnCdtfUJFoWiSYmkJjzhO2vqEc4Ttr6hFoWgaI4zzYd/wAplzhO2vqEc4Ttr6hFoWjHYd/yjYd8y5wnbX1COcJ219Qi0LQFITMCYc4Ttr6hHOE7a+oRaFokiR84Ttr6hHOE7a+oRaFokE+rifJRiE7a+oT6lz+j+rT/AHF/MNorJosRK/P6P6tP9xfzK+P03hqNJqtSsgRBc2YMfAKuZPVYSLRW0WcfVCob9eUpaNwbF9rUFupAeocTOB0jylr6QZKmFqbDDpi6FLpWFWuXqDXNr9FVWxPHdxn0o4+j+rT/AHF/Myj+aWp8diJRxtpp2/T78+hYms0wCchvYao+Jt95b5/R/Vp/uL+ZzmmuV2GpY6hh2JZn1CGXVNMbRynSa+VtW58RJyv8u3lfP+C2nHPabpfqvh6nVKtgAOoW8p7K/P6P6tP9xfzHP6P6tP8AcX8zSxaLEu09w8JrExlEkDa0/wBxfzNrJRaIiIklhI3pqd4B8ReSRAItgnZX0iNgnZX0iSxAItgnZX0iNgnZX0iSxAItgnZX0iNinZXyEliTqZFLwRbFOyvkI2KdlfISWJBJHsU7K+QjYp2V8hJIgEexTsr5CQYp6NJS9QoijezWAHxMtzmuXGFD4dSamzCVVcsaTVFyBA1kUG4uRvFpEnSszyycIOSXHw/j5otaP03g65K06iFgCbHIkAXLAHqt5S+rUmXWQK41SRq6p1gOzx4T5zh9XE1BqYpWNMs1hhdQsFvclxTQKtsrG4HEyQ0q9MYfUwz7XDagZxWUAK7FtkyX67gX67zNZPccMOtk46qteV347R1cbvd9uUdzorG061FajU9kWLAI4Aa6kjK+/deXaQpsAyhCDuIsQfAz5zgsG4qVL6Od6auHSkcSCcOxFnub3IbI2PZE2+jNOVKVJKVHRzBNVmQDEKbgG7MCRc5nf3yVPyXxdXsta9JN7f8ATv8AVbJs6rE1qFP32ppkT0iouBvOfVKuKx6KaYSltg9QIxQAilxZyNwE5PS2Er4qlVr1VVGFBdjRFRSVVmF6jNe2YDAH8S5o7FV8EKqc32q7Ta6y10XV11ACMjG6m9vG8ai3+S9W6qPmnfflVtw/0/Y6qnXoM2qrUy2fRBUnI2OQ4Ge1a1FbKzIpJAAJUElvdAB6z1ThqOCxNBjWVF2mxqGwqIRSevU6Ckk2Jsb8DI8Do8mts0VatRcRhqtSoKus9RWUuztc21QSLWz3b73jW/A/yp7LTT+P8bvZ/pXnY+ibFOyvkI2CdlfSJLE0O0i2CdlfSI2CdlfSJLEAj2KdlfITzYJ2V9IksQCLYJ2V9InuxTsr5CSRAItgnZX0iNgnZX0iSxAItgnZX0iSxEAREQBERAEREAREQBERAEREAREQBERAI3QEEEAgixFsiD1WkL4Kkda6KdYgtdQdYr7pPG3VEQxSlySJQVSSqgFveIABbx47zMKeDprq2RRqghbKBqg7wvAGIgaUeNg6ZvempuoU9EZquaqe4XOU9fB021r01OtbWuoOtq2tfjaw8p5EUNEfB4+DpHWuinWtrXUHX1fd1ss7dUypYWmhJVFUkKpIUAkKLKCR1AZDhEQg4pO6LMREAREQBERAEREAREQBERAEREA//9k="
                                             class="w-7 h-7"
                                        />
                                    </span>
                                                                <div>
                                      <span
                                          class="text-blue-darkest dark:text-d-black-70 text-xs"
                                      >یاد داشت ها</span
                                      >
                                                                </div>
                                                            </div>
                                                        </a
                                                        >
                                                    </div>
                                                </div>
                                                <div
                                                    class="bg-white item dark:bg-d-black-10 w-sq rounded-xl text-center border"
                                                    style="border-color: rgb(0, 109, 166)"
                                                >
                                                    <div
                                                        class="group w-full h-full relative flex items-center justify-center"
                                                    >
                                                        <a
                                                            href="https://oi.dastyar.io/shabpin"
                                                            target="_self"
                                                        >
                                                            <div
                                                                class="flex flex-col items-center justify-center gap-y-2"
                                                            >
                                    <span
                                    >
                                        <img style="margin-top: 5px"
                                             src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxESEBUTEhIVFRUXERUVFhgYEhkYEhAXFRcaFhUSFRUYHyghGBolGxcVITEhJSkrLi4uGB8zODMtNygtLisBCgoKDg0OGxAQGi0lICUvLS0tLS0tKy0tLS0tLS0tLS0wLi0tLS0tLy0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIALcBEwMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAwQCBQYBB//EAD8QAAIBAgIFCQcCBgEEAwAAAAECAAMRBCEFEhMxUQYUIkFSYXGRkhUygaGxwdFTkwcjQmJy4fCCorLiJDNj/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECAwQFBv/EADARAAICAQMDAQYGAgMAAAAAAAABAhEDEiExBEFRoRNhgbHB8AUicZHR4RQyUoLx/9oADAMBAAIRAxEAPwD7jERAEREAREQBETB6gEAziVmxB6hMDWbjBGpFyJrqNVulmfeIkwrNKqVqyXs6LcSAV+IkquDuMsRZlERBIiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAnhNp7KdWpc90EN0DiNa4GVjb7/eYSKhvf8AzP0EllE7RElT+/AiIlipHh/6v8z9pJI8P/V/mftJJSHBfJ/sxI6fvt4L95JKmHxINZ1sbgDw6OR+sibScb8/Rkwi2pV2X1RfWsR3ywlQGU4mllLL8SolUjvkorjgYstaJokO3HfPDiO6LFoniVTWPhIySd5k2RqLbVAOuYbcStEiyLJjiDwnnOD3SKIFsl5we6ZDE90giBbLa1Qe6SShMkcjdFk6i7Eip1Qe4yWSWEREAREQBERAEREAirtYeMqybEnMeEhkFJEVDe/+Z+gksgwtQEvYg9Pj3D8GTykGmtvf8y2RVLf3fIRE5rSeIrGu66tQoNUKVViu4EnLfmT5TPPnWGKk1duvv9jXpuml1EnFOqV+qX1NnofGbTaZg2YHLq1r/ibKcsmHqjMI48FInQaOdjTGte+Yz35HK85+jzyktEluu/nf/wAOnr+njCWuPD7eNv6KWltLbKotIWuV1rndmSAo78jNbRxLLUaoD0mve+a5m+Q6t06CvgKNRtZ6asbWuR1Dq+ZmYwdECwpLIy9Nnnk1a9rtc7f2Ww9T0+PHp0O6qXG/rwRaNxZqJcixBseB75amFGiq5KAPCZzsxqSilJ2zz8ji5twVLwIiJcoIJiUai6zte5ta1mAt5yJOis5aS9rDiPONYcR5ylzZey3rWObL2W9aymt+DPXLx8/4LusOI84BlLmy9lvWshqrqkWuP+oH6Rra5QeVrlff7GziImhsIiIAiIgCTUa3UZDEkJl+JDQfqk0k0TEREAREQBERAKdY9IzXYrSiI2pmWABIH9N915fY3M1GL0Cr1TU2jKWtcAAjIAdfhOfqHmUV7JK77+PjsadOsLk/bNpU+L5+G/kp6NxpplywuGItq7xv338Zv6FYOoZdxlA6DS3/ANjE/wCIEuYTDimuqDfMnzmHSwzY/wAs+N/F82b9bPBkeuD/ADfGqqiaAYidxwGTOTvMxiIsCIiQBERAEExPKm4w3SsmKtpGAqiV61MhiQoYHjnaZROX2ra3OnJ0cZLZtepFZv0l8j+Ys36S+R/MmTfLMvBavv8Ao5MvSqDq/RFCzfpL5H8yJwSwBUKe4W3zaSGpSu6nxv8AaXcDCWHbb6ExiImh0CIiAIicBp7+KuEw9YU6aNiBbpOjAIMyCEJ9+1jmMu/fY3RDaXJ38TneSHLDD6RVzSDo1O2ujgawDX1WBBIIyPl4TooJTvg9RrEGXpQl1Nw8JKLRMoiJJYREQBIqzWX5SWVa7XPhBDZFERKlBERAEREARIKmMpq2qXAI6uuY8+pdsSSaLMStz+l2x84XH0iQA4uTYZGBRZnNcv8AlMdH4Taooao1QU6Ya+oGILazWzIAVsuvKdLNRyr0RSxWEqUqw6NtYHrVkzBH08CRKvhkU3suT5Ron+IGmmbaLT5whNtXm51O8KyWIPxM+0YN3akhqKEcopdQbhGIBZQeuxuLzjNDYSmrJRpKEUH3RkAL3Y/WdzMOnyvJbZvn6ZYKjqt9/wCu/nk8UWnsROgwe+7EREAREQBERAEREAq6UdVoVWY2UUahY9kBSSfKfMuVnJCjVw9Mo4pHD0tUMRdWRBezeGZv3njOv5acoaVFHw5TaNUpMrLewRXBXpHiQTl/qfPvabHCHCsCylCmuXu9juNyLXH2nL1Ck5Jx7FI/iPSYteLM+a7N7rtt97GH8DFbntYj3eam/iaiav0afbJ8G5GYsaNx9Ooz/wAirejUJy1A1irnqsGAN+F595m8ODLpskcmO4sS6m4eEpAS/NEdURERJLCInhNoBhWewlSZVHubzGQyjYiIkEFHSGN2ZThrgN4Nl/v4S9NRpjDhyVNwDY5d0uaLqs1JSxucwTxsxF/lKRe7RlCbc2mW4iJc1NFyloWK1B/ifqPvNXSqg+M6nSGH2lNl6yMvEZj5zjcfhKiMtMizOQALg3ubdRyzkosnsW0qA7s87d3w4ySKei8QosKeQ/uX8zJ8PVX3qZHxH5kktpG5OO1kFt5GfcesCVnz35+Mw0ZSJuGGqN+ds+4SKliemUORDEDvsfrObJd7nBmbUrvbsZ0sMFNwPmZbwtbVbuP/AC8injsALnIXtfqz3Skfy8EPLOUtcm2/L3NzEq4fFDVAY5/WWVqA7iD8Z0ppnXGafB7ERJLCIiAIiIAmu0/pVcLQaqczuRe2TuHh1nuBmxnKcvdD1cQtIockLBhew6VrN8rfGVnLTFsyzPIoP2SuXZe/g+cYqu9R2dySzEsSesmVqjWE6fTBw+2FOp0Vp0aajUzsxXWY/Ek598q4bRmCY54ot3ahX5m4nLKaR4C/Dsk8uhTjdtO5RTtc2m7e97q/fRy2nEvQbusfmPtefUP4RaaqYnR+rUJLUKmyDHeyBVZL94B1f+kTgeWOGp0adTZuChUap1rm+WWXXefR/wCF2gWwmj1FQEVKrms4O9NYAIh4HVUEjqJM2xO90el+H4Z4XPHNcNr3dvTwdlRW7S3IMMMryebo9VcCIiSSJXxD9XnJyZSY3kMiR5ERIKGFdyqkgXIG7j3TDCYlaiBl3HzU9YPfM6q3UjiJok/kOal2C63TXqscr24g5+cpKWl+4ynPRJXwW9N4sUyoI3pUa9+xY2+N/lNJorT5pUWtSuFNyxe2uztYZauWX0m25R6NqVwmpq5Bhmbe8VI6uCmcxpVNhhlRveNZta3FLjy3SWvzWWpKafd7fz6G8wfKvXYg0guV765Pw92XPb68PmfxOb0bowsoIZQSL2vdhfPNeqW6+j3UXFmA32yPkZHtIkLNjuja19OBlZR0SVIBF7rcbxlNFUpFmDNXYsLWJUkixuLG/GUm0igNjreUx9p0+/ympujaitVG7F1L9WRI+IJsfCbWrplG3jPiCfxOV9p0+/yj2nT7/KQ0nyHFNUzpfaNPv8/9SDE4tXy1rANfcSfOaH2pT7/KPadPv8pV4oNU0RCEYSUoqmu6s6KljlAtcnxvf6SanpVACCAwIsQb/icv7Tp9/lHtOn3+ULHFcIj2cdWqt/v4G+GIpDcWA4Bzb/xmaYxB/U58T/6znvadPv8AKPadP+7yj2cfBX2OP/idNg9JJSvqliOBa4HeOjlLXt9ez8z+Jx/tJP7vTPfaKcG9MmMFFUkbTbm9Ut2df7fXs/M/iXtHY0VVJAtY2+V/vOC9oJwb0zt9C4NqSENa5a+RvlYD7SSjSRsYiJBUTxlBFjmDPYEA+L6fa+LrcBUIHgpIHyE1zC87fSfI7Xqu6VrazM1jT3XJO8H7SgeRdbqqJ/3fic3B8tm6fPLJKVXbb7d2avkfhVbH0FqIHXXJsRcXVSym3cwB+E+yz59yU5PGni0Z3uU1jYDr1SMyerOfQZtj4PZ/DccoYmpKt/oi7TFgPCZRE1PUEREAixB6MqyxidwleQyj5EREggTW6boKaTZgEgjMgaxtlv65njaFZv6yF4KQD8Sc/K0oUtFapJ1WYntZ/WZzle1GGSVrTpN4BkvXu+OU4/l+VUU2A6Ws1uF7DpHvAt8uE2z4R1s6AqVNwB7rcVK7s5qP4iDo0f8AJ/oJaLvsXhLU91RxIcg3BN9975343nQ6H5REfy8R0lOWsd6/5cR375zsS7SfJtOKkqZ0mn9H6pDLuO493C/X1ec04od82YxhNE0mzsQUPDivhKcrBUqK4ouKpkWxHGe7ETJmA3mRnEDqBMuaGeyHCNmOEiOIPD5zzbNwHzgE+oOAnthK22bgJ7zhuAgFieyuMSOsESVKoO4wDOIiABPq4nygT6uJDKyEREgqJjVayk90ylXHv0bcT9P+CRJ0rKzdRbNZiauqpPXuHjPMJ7i+EhxCGpUSmOvf3cT8ADNhikAaw3AADwAtOetrOLTtZHo+n/8AIJ//AD+dwPpNxNbosdOoeARfqT9RNlNsf+p1YVUC5SN1EzkGGbePjJ5qdC4EREEkGJGQ8ZXlust1MqSGUfIiIkECImFWsii7MqjizADwzhtLklJt0iDHhtUhDYm1jwtvPjYTmuVKUkVOcF2uxAsb2IAvkbAbxu4ToKmkKLe7UQ7xkwO8H/U5fl3WFRaWowazOTbOwNrGZaoN3q9Sv+POU6qXqcpjNlf+Vr2/vA+VjK8RNzRKtjZTCq9pnMaiXgkrRPWUieQBERAEREATEoJlAgGdCqb2PwlmQ06WdzJoAE+rifKBPq4kMrIRESComvxz3a3AS+TNDjqx3D3mNgPGZ5XsYZ3SSJ9CprO9Tv1V+/2k+O9/4CWsFh9nTVeAz7yd5lXSBs1/7ZElUKInHTjoy0QOix41G+Vl+0vyroxLUU71v6jrfeWppFbI2gqikZU2sRLsoS6u4eEsjSJlERJLCUnSxtLsq4utTGTOqneNZgL+cEMiiV+f0f1af7i/mOf0f1af7i/mVM7RYmr5RpegT2WU+fR+8uc/o/q0/wBxfzK2ksXRai42tP3Db+Yu8Zjr7pj1EdeGcfKfyNunyaMsZeGvmciIZusyLnNPtL6hIMbi6Ypt0l90jeOvKfIY4rI1HzS/fY+unkUE5eN/2NCTfPjnPJhtl7S+Ynm2XtL5ifb2j4/UvJtp7IucJ219QjnCdtfUJFoWiSYmkJjzhO2vqEc4Ttr6hFoWgaI4zzYd/wAplzhO2vqEc4Ttr6hFoWjHYd/yjYd8y5wnbX1COcJ219Qi0LQFITMCYc4Ttr6hHOE7a+oRaFokiR84Ttr6hHOE7a+oRaFokE+rifJRiE7a+oT6lz+j+rT/AHF/MNorJosRK/P6P6tP9xfzK+P03hqNJqtSsgRBc2YMfAKuZPVYSLRW0WcfVCob9eUpaNwbF9rUFupAeocTOB0jylr6QZKmFqbDDpi6FLpWFWuXqDXNr9FVWxPHdxn0o4+j+rT/AHF/Myj+aWp8diJRxtpp2/T78+hYms0wCchvYao+Jt95b5/R/Vp/uL+ZzmmuV2GpY6hh2JZn1CGXVNMbRynSa+VtW58RJyv8u3lfP+C2nHPabpfqvh6nVKtgAOoW8p7K/P6P6tP9xfzHP6P6tP8AcX8zSxaLEu09w8JrExlEkDa0/wBxfzNrJRaIiIklhI3pqd4B8ReSRAItgnZX0iNgnZX0iSxAItgnZX0iNgnZX0iSxAItgnZX0iNinZXyEliTqZFLwRbFOyvkI2KdlfISWJBJHsU7K+QjYp2V8hJIgEexTsr5CQYp6NJS9QoijezWAHxMtzmuXGFD4dSamzCVVcsaTVFyBA1kUG4uRvFpEnSszyycIOSXHw/j5otaP03g65K06iFgCbHIkAXLAHqt5S+rUmXWQK41SRq6p1gOzx4T5zh9XE1BqYpWNMs1hhdQsFvclxTQKtsrG4HEyQ0q9MYfUwz7XDagZxWUAK7FtkyX67gX67zNZPccMOtk46qteV347R1cbvd9uUdzorG061FajU9kWLAI4Aa6kjK+/deXaQpsAyhCDuIsQfAz5zgsG4qVL6Od6auHSkcSCcOxFnub3IbI2PZE2+jNOVKVJKVHRzBNVmQDEKbgG7MCRc5nf3yVPyXxdXsta9JN7f8ATv8AVbJs6rE1qFP32ppkT0iouBvOfVKuKx6KaYSltg9QIxQAilxZyNwE5PS2Er4qlVr1VVGFBdjRFRSVVmF6jNe2YDAH8S5o7FV8EKqc32q7Ta6y10XV11ACMjG6m9vG8ai3+S9W6qPmnfflVtw/0/Y6qnXoM2qrUy2fRBUnI2OQ4Ge1a1FbKzIpJAAJUElvdAB6z1ThqOCxNBjWVF2mxqGwqIRSevU6Ckk2Jsb8DI8Do8mts0VatRcRhqtSoKus9RWUuztc21QSLWz3b73jW/A/yp7LTT+P8bvZ/pXnY+ibFOyvkI2CdlfSJLE0O0i2CdlfSI2CdlfSJLEAj2KdlfITzYJ2V9IksQCLYJ2V9InuxTsr5CSRAItgnZX0iNgnZX0iSxAItgnZX0iSxEAREQBERAEREAREQBERAEREAREQBERAI3QEEEAgixFsiD1WkL4Kkda6KdYgtdQdYr7pPG3VEQxSlySJQVSSqgFveIABbx47zMKeDprq2RRqghbKBqg7wvAGIgaUeNg6ZvempuoU9EZquaqe4XOU9fB021r01OtbWuoOtq2tfjaw8p5EUNEfB4+DpHWuinWtrXUHX1fd1ss7dUypYWmhJVFUkKpIUAkKLKCR1AZDhEQg4pO6LMREAREQBERAEREAREQBERAEREA//9k="
                                             class="w-7 h-7"
                                        />
                                    </span>
                                                                <div>
                                      <span
                                          class="text-blue-darkest dark:text-d-black-70 text-xs"
                                      > پیغام های من</span
                                      >
                                                                </div>
                                                            </div>
                                                        </a
                                                        >
                                                    </div>
                                                </div>
                                                <div
                                                    class="bg-white item dark:bg-d-black-10 w-sq rounded-xl text-center border"
                                                    style="border-color: rgb(0, 109, 166)"
                                                >
                                                    <div
                                                        class="group w-full h-full relative flex items-center justify-center"
                                                    >
                                                        <a
                                                            href="https://oi.dastyar.io/shabpin"
                                                            target="_self"
                                                        >
                                                            <div
                                                                class="flex flex-col items-center justify-center gap-y-2"
                                                            >
                                    <span
                                    >
                                        <img style="margin-top: 5px"
                                             src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxESEBUTEhIVFRUXERUVFhgYEhkYEhAXFRcaFhUSFRUYHyghGBolGxcVITEhJSkrLi4uGB8zODMtNygtLisBCgoKDg0OGxAQGi0lICUvLS0tLS0tKy0tLS0tLS0tLS0wLi0tLS0tLy0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIALcBEwMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAwQCBQYBB//EAD8QAAIBAgIFCQcCBgEEAwAAAAECAAMRBCEFEhMxUQYUIkFSYXGRkhUygaGxwdFTkwcjQmJy4fCCorLiJDNj/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECAwQFBv/EADARAAICAQMDAQYGAgMAAAAAAAABAhEDEiExBEFRoRNhgbHB8AUicZHR4RQyUoLx/9oADAMBAAIRAxEAPwD7jERAEREAREQBETB6gEAziVmxB6hMDWbjBGpFyJrqNVulmfeIkwrNKqVqyXs6LcSAV+IkquDuMsRZlERBIiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAnhNp7KdWpc90EN0DiNa4GVjb7/eYSKhvf8AzP0EllE7RElT+/AiIlipHh/6v8z9pJI8P/V/mftJJSHBfJ/sxI6fvt4L95JKmHxINZ1sbgDw6OR+sibScb8/Rkwi2pV2X1RfWsR3ywlQGU4mllLL8SolUjvkorjgYstaJokO3HfPDiO6LFoniVTWPhIySd5k2RqLbVAOuYbcStEiyLJjiDwnnOD3SKIFsl5we6ZDE90giBbLa1Qe6SShMkcjdFk6i7Eip1Qe4yWSWEREAREQBERAEREAirtYeMqybEnMeEhkFJEVDe/+Z+gksgwtQEvYg9Pj3D8GTykGmtvf8y2RVLf3fIRE5rSeIrGu66tQoNUKVViu4EnLfmT5TPPnWGKk1duvv9jXpuml1EnFOqV+qX1NnofGbTaZg2YHLq1r/ibKcsmHqjMI48FInQaOdjTGte+Yz35HK85+jzyktEluu/nf/wAOnr+njCWuPD7eNv6KWltLbKotIWuV1rndmSAo78jNbRxLLUaoD0mve+a5m+Q6t06CvgKNRtZ6asbWuR1Dq+ZmYwdECwpLIy9Nnnk1a9rtc7f2Ww9T0+PHp0O6qXG/rwRaNxZqJcixBseB75amFGiq5KAPCZzsxqSilJ2zz8ji5twVLwIiJcoIJiUai6zte5ta1mAt5yJOis5aS9rDiPONYcR5ylzZey3rWObL2W9aymt+DPXLx8/4LusOI84BlLmy9lvWshqrqkWuP+oH6Rra5QeVrlff7GziImhsIiIAiIgCTUa3UZDEkJl+JDQfqk0k0TEREAREQBERAKdY9IzXYrSiI2pmWABIH9N915fY3M1GL0Cr1TU2jKWtcAAjIAdfhOfqHmUV7JK77+PjsadOsLk/bNpU+L5+G/kp6NxpplywuGItq7xv338Zv6FYOoZdxlA6DS3/ANjE/wCIEuYTDimuqDfMnzmHSwzY/wAs+N/F82b9bPBkeuD/ADfGqqiaAYidxwGTOTvMxiIsCIiQBERAEExPKm4w3SsmKtpGAqiV61MhiQoYHjnaZROX2ra3OnJ0cZLZtepFZv0l8j+Ys36S+R/MmTfLMvBavv8Ao5MvSqDq/RFCzfpL5H8yJwSwBUKe4W3zaSGpSu6nxv8AaXcDCWHbb6ExiImh0CIiAIicBp7+KuEw9YU6aNiBbpOjAIMyCEJ9+1jmMu/fY3RDaXJ38TneSHLDD6RVzSDo1O2ujgawDX1WBBIIyPl4TooJTvg9RrEGXpQl1Nw8JKLRMoiJJYREQBIqzWX5SWVa7XPhBDZFERKlBERAEREARIKmMpq2qXAI6uuY8+pdsSSaLMStz+l2x84XH0iQA4uTYZGBRZnNcv8AlMdH4Taooao1QU6Ya+oGILazWzIAVsuvKdLNRyr0RSxWEqUqw6NtYHrVkzBH08CRKvhkU3suT5Ron+IGmmbaLT5whNtXm51O8KyWIPxM+0YN3akhqKEcopdQbhGIBZQeuxuLzjNDYSmrJRpKEUH3RkAL3Y/WdzMOnyvJbZvn6ZYKjqt9/wCu/nk8UWnsROgwe+7EREAREQBERAEREAq6UdVoVWY2UUahY9kBSSfKfMuVnJCjVw9Mo4pHD0tUMRdWRBezeGZv3njOv5acoaVFHw5TaNUpMrLewRXBXpHiQTl/qfPvabHCHCsCylCmuXu9juNyLXH2nL1Ck5Jx7FI/iPSYteLM+a7N7rtt97GH8DFbntYj3eam/iaiav0afbJ8G5GYsaNx9Ooz/wAirejUJy1A1irnqsGAN+F595m8ODLpskcmO4sS6m4eEpAS/NEdURERJLCInhNoBhWewlSZVHubzGQyjYiIkEFHSGN2ZThrgN4Nl/v4S9NRpjDhyVNwDY5d0uaLqs1JSxucwTxsxF/lKRe7RlCbc2mW4iJc1NFyloWK1B/ifqPvNXSqg+M6nSGH2lNl6yMvEZj5zjcfhKiMtMizOQALg3ubdRyzkosnsW0qA7s87d3w4ySKei8QosKeQ/uX8zJ8PVX3qZHxH5kktpG5OO1kFt5GfcesCVnz35+Mw0ZSJuGGqN+ds+4SKliemUORDEDvsfrObJd7nBmbUrvbsZ0sMFNwPmZbwtbVbuP/AC8injsALnIXtfqz3Skfy8EPLOUtcm2/L3NzEq4fFDVAY5/WWVqA7iD8Z0ppnXGafB7ERJLCIiAIiIAmu0/pVcLQaqczuRe2TuHh1nuBmxnKcvdD1cQtIockLBhew6VrN8rfGVnLTFsyzPIoP2SuXZe/g+cYqu9R2dySzEsSesmVqjWE6fTBw+2FOp0Vp0aajUzsxXWY/Ek598q4bRmCY54ot3ahX5m4nLKaR4C/Dsk8uhTjdtO5RTtc2m7e97q/fRy2nEvQbusfmPtefUP4RaaqYnR+rUJLUKmyDHeyBVZL94B1f+kTgeWOGp0adTZuChUap1rm+WWXXefR/wCF2gWwmj1FQEVKrms4O9NYAIh4HVUEjqJM2xO90el+H4Z4XPHNcNr3dvTwdlRW7S3IMMMryebo9VcCIiSSJXxD9XnJyZSY3kMiR5ERIKGFdyqkgXIG7j3TDCYlaiBl3HzU9YPfM6q3UjiJok/kOal2C63TXqscr24g5+cpKWl+4ynPRJXwW9N4sUyoI3pUa9+xY2+N/lNJorT5pUWtSuFNyxe2uztYZauWX0m25R6NqVwmpq5Bhmbe8VI6uCmcxpVNhhlRveNZta3FLjy3SWvzWWpKafd7fz6G8wfKvXYg0guV765Pw92XPb68PmfxOb0bowsoIZQSL2vdhfPNeqW6+j3UXFmA32yPkZHtIkLNjuja19OBlZR0SVIBF7rcbxlNFUpFmDNXYsLWJUkixuLG/GUm0igNjreUx9p0+/ympujaitVG7F1L9WRI+IJsfCbWrplG3jPiCfxOV9p0+/yj2nT7/KQ0nyHFNUzpfaNPv8/9SDE4tXy1rANfcSfOaH2pT7/KPadPv8pV4oNU0RCEYSUoqmu6s6KljlAtcnxvf6SanpVACCAwIsQb/icv7Tp9/lHtOn3+ULHFcIj2cdWqt/v4G+GIpDcWA4Bzb/xmaYxB/U58T/6znvadPv8AKPadP+7yj2cfBX2OP/idNg9JJSvqliOBa4HeOjlLXt9ez8z+Jx/tJP7vTPfaKcG9MmMFFUkbTbm9Ut2df7fXs/M/iXtHY0VVJAtY2+V/vOC9oJwb0zt9C4NqSENa5a+RvlYD7SSjSRsYiJBUTxlBFjmDPYEA+L6fa+LrcBUIHgpIHyE1zC87fSfI7Xqu6VrazM1jT3XJO8H7SgeRdbqqJ/3fic3B8tm6fPLJKVXbb7d2avkfhVbH0FqIHXXJsRcXVSym3cwB+E+yz59yU5PGni0Z3uU1jYDr1SMyerOfQZtj4PZ/DccoYmpKt/oi7TFgPCZRE1PUEREAixB6MqyxidwleQyj5EREggTW6boKaTZgEgjMgaxtlv65njaFZv6yF4KQD8Sc/K0oUtFapJ1WYntZ/WZzle1GGSVrTpN4BkvXu+OU4/l+VUU2A6Ws1uF7DpHvAt8uE2z4R1s6AqVNwB7rcVK7s5qP4iDo0f8AJ/oJaLvsXhLU91RxIcg3BN9975343nQ6H5REfy8R0lOWsd6/5cR375zsS7SfJtOKkqZ0mn9H6pDLuO493C/X1ec04od82YxhNE0mzsQUPDivhKcrBUqK4ouKpkWxHGe7ETJmA3mRnEDqBMuaGeyHCNmOEiOIPD5zzbNwHzgE+oOAnthK22bgJ7zhuAgFieyuMSOsESVKoO4wDOIiABPq4nygT6uJDKyEREgqJjVayk90ylXHv0bcT9P+CRJ0rKzdRbNZiauqpPXuHjPMJ7i+EhxCGpUSmOvf3cT8ADNhikAaw3AADwAtOetrOLTtZHo+n/8AIJ//AD+dwPpNxNbosdOoeARfqT9RNlNsf+p1YVUC5SN1EzkGGbePjJ5qdC4EREEkGJGQ8ZXlust1MqSGUfIiIkECImFWsii7MqjizADwzhtLklJt0iDHhtUhDYm1jwtvPjYTmuVKUkVOcF2uxAsb2IAvkbAbxu4ToKmkKLe7UQ7xkwO8H/U5fl3WFRaWowazOTbOwNrGZaoN3q9Sv+POU6qXqcpjNlf+Vr2/vA+VjK8RNzRKtjZTCq9pnMaiXgkrRPWUieQBERAEREATEoJlAgGdCqb2PwlmQ06WdzJoAE+rifKBPq4kMrIRESComvxz3a3AS+TNDjqx3D3mNgPGZ5XsYZ3SSJ9CprO9Tv1V+/2k+O9/4CWsFh9nTVeAz7yd5lXSBs1/7ZElUKInHTjoy0QOix41G+Vl+0vyroxLUU71v6jrfeWppFbI2gqikZU2sRLsoS6u4eEsjSJlERJLCUnSxtLsq4utTGTOqneNZgL+cEMiiV+f0f1af7i/mOf0f1af7i/mVM7RYmr5RpegT2WU+fR+8uc/o/q0/wBxfzK2ksXRai42tP3Db+Yu8Zjr7pj1EdeGcfKfyNunyaMsZeGvmciIZusyLnNPtL6hIMbi6Ypt0l90jeOvKfIY4rI1HzS/fY+unkUE5eN/2NCTfPjnPJhtl7S+Ynm2XtL5ifb2j4/UvJtp7IucJ219QjnCdtfUJFoWiSYmkJjzhO2vqEc4Ttr6hFoWgaI4zzYd/wAplzhO2vqEc4Ttr6hFoWjHYd/yjYd8y5wnbX1COcJ219Qi0LQFITMCYc4Ttr6hHOE7a+oRaFokiR84Ttr6hHOE7a+oRaFokE+rifJRiE7a+oT6lz+j+rT/AHF/MNorJosRK/P6P6tP9xfzK+P03hqNJqtSsgRBc2YMfAKuZPVYSLRW0WcfVCob9eUpaNwbF9rUFupAeocTOB0jylr6QZKmFqbDDpi6FLpWFWuXqDXNr9FVWxPHdxn0o4+j+rT/AHF/Myj+aWp8diJRxtpp2/T78+hYms0wCchvYao+Jt95b5/R/Vp/uL+ZzmmuV2GpY6hh2JZn1CGXVNMbRynSa+VtW58RJyv8u3lfP+C2nHPabpfqvh6nVKtgAOoW8p7K/P6P6tP9xfzHP6P6tP8AcX8zSxaLEu09w8JrExlEkDa0/wBxfzNrJRaIiIklhI3pqd4B8ReSRAItgnZX0iNgnZX0iSxAItgnZX0iNgnZX0iSxAItgnZX0iNinZXyEliTqZFLwRbFOyvkI2KdlfISWJBJHsU7K+QjYp2V8hJIgEexTsr5CQYp6NJS9QoijezWAHxMtzmuXGFD4dSamzCVVcsaTVFyBA1kUG4uRvFpEnSszyycIOSXHw/j5otaP03g65K06iFgCbHIkAXLAHqt5S+rUmXWQK41SRq6p1gOzx4T5zh9XE1BqYpWNMs1hhdQsFvclxTQKtsrG4HEyQ0q9MYfUwz7XDagZxWUAK7FtkyX67gX67zNZPccMOtk46qteV347R1cbvd9uUdzorG061FajU9kWLAI4Aa6kjK+/deXaQpsAyhCDuIsQfAz5zgsG4qVL6Od6auHSkcSCcOxFnub3IbI2PZE2+jNOVKVJKVHRzBNVmQDEKbgG7MCRc5nf3yVPyXxdXsta9JN7f8ATv8AVbJs6rE1qFP32ppkT0iouBvOfVKuKx6KaYSltg9QIxQAilxZyNwE5PS2Er4qlVr1VVGFBdjRFRSVVmF6jNe2YDAH8S5o7FV8EKqc32q7Ta6y10XV11ACMjG6m9vG8ai3+S9W6qPmnfflVtw/0/Y6qnXoM2qrUy2fRBUnI2OQ4Ge1a1FbKzIpJAAJUElvdAB6z1ThqOCxNBjWVF2mxqGwqIRSevU6Ckk2Jsb8DI8Do8mts0VatRcRhqtSoKus9RWUuztc21QSLWz3b73jW/A/yp7LTT+P8bvZ/pXnY+ibFOyvkI2CdlfSJLE0O0i2CdlfSI2CdlfSJLEAj2KdlfITzYJ2V9IksQCLYJ2V9InuxTsr5CSRAItgnZX0iNgnZX0iSxAItgnZX0iSxEAREQBERAEREAREQBERAEREAREQBERAI3QEEEAgixFsiD1WkL4Kkda6KdYgtdQdYr7pPG3VEQxSlySJQVSSqgFveIABbx47zMKeDprq2RRqghbKBqg7wvAGIgaUeNg6ZvempuoU9EZquaqe4XOU9fB021r01OtbWuoOtq2tfjaw8p5EUNEfB4+DpHWuinWtrXUHX1fd1ss7dUypYWmhJVFUkKpIUAkKLKCR1AZDhEQg4pO6LMREAREQBERAEREAREQBERAEREA//9k="
                                             class="w-7 h-7"
                                        />
                                    </span>
                                                                <div>
                                      <span
                                          class="text-blue-darkest dark:text-d-black-70 text-xs"
                                      >ماشین حساب</span
                                      >
                                                                </div>
                                                            </div>
                                                        </a
                                                        >
                                                    </div>
                                                </div>
                                                <div
                                                    class="bg-white item dark:bg-d-black-10 w-sq rounded-xl text-center border flex justify-center items-center p-5"
                                                >
                                                    <svg
                                                        width="24"
                                                        height="24"
                                                        viewBox="0 0 10 10"
                                                        fill="none"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                    >
                                                        <path
                                                            d="M1 5H9M5 9V1"
                                                            stroke="#BABDC6 "
                                                            stroke-width="1.5"
                                                            stroke-linecap="round"
                                                            stroke-linejoin="round"
                                                        ></path>
                                                    </svg>
                                                </div>
                                                <div
                                                    class="bg-white item dark:bg-d-black-10 w-sq rounded-xl text-center border flex justify-center items-center p-5"
                                                >
                                                    <svg
                                                        width="24"
                                                        height="24"
                                                        viewBox="0 0 10 10"
                                                        fill="none"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                    >
                                                        <path
                                                            d="M1 5H9M5 9V1"
                                                            stroke="#BABDC6 "
                                                            stroke-width="1.5"
                                                            stroke-linecap="round"
                                                            stroke-linejoin="round"
                                                        ></path>
                                                    </svg>
                                                </div>
                                                <div
                                                    class="bg-white item dark:bg-d-black-10 w-sq rounded-xl text-center border flex justify-center items-center p-5"
                                                >
                                                    <svg
                                                        width="24"
                                                        height="24"
                                                        viewBox="0 0 10 10"
                                                        fill="none"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                    >
                                                        <path
                                                            d="M1 5H9M5 9V1"
                                                            stroke="#BABDC6 "
                                                            stroke-width="1.5"
                                                            stroke-linecap="round"
                                                            stroke-linejoin="round"
                                                        ></path>
                                                    </svg>
                                                </div>
                                                <div
                                                    class="bg-white item dark:bg-d-black-10 w-sq rounded-xl text-center border flex justify-center items-center p-5"
                                                >
                                                    <svg
                                                        width="24"
                                                        height="24"
                                                        viewBox="0 0 10 10"
                                                        fill="none"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                    >
                                                        <path
                                                            d="M1 5H9M5 9V1"
                                                            stroke="#BABDC6 "
                                                            stroke-width="1.5"
                                                            stroke-linecap="round"
                                                            stroke-linejoin="round"
                                                        ></path>
                                                    </svg>
                                                </div>
                                                <div
                                                    class="bg-white item dark:bg-d-black-10 w-sq rounded-xl text-center border flex justify-center items-center p-5"
                                                >
                                                    <svg
                                                        width="24"
                                                        height="24"
                                                        viewBox="0 0 10 10"
                                                        fill="none"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                    >
                                                        <path
                                                            d="M1 5H9M5 9V1"
                                                            stroke="#BABDC6 "
                                                            stroke-width="1.5"
                                                            stroke-linecap="round"
                                                            stroke-linejoin="round"
                                                        ></path>
                                                    </svg>
                                                </div>
                                                <div
                                                    class="bg-white item dark:bg-d-black-10 w-sq rounded-xl text-center border flex justify-center items-center p-5"
                                                >
                                                    <svg
                                                        width="24"
                                                        height="24"
                                                        viewBox="0 0 10 10"
                                                        fill="none"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                    >
                                                        <path
                                                            d="M1 5H9M5 9V1"
                                                            stroke="#BABDC6 "
                                                            stroke-width="1.5"
                                                            stroke-linecap="round"
                                                            stroke-linejoin="round"
                                                        ></path>
                                                    </svg>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="flex flex-col col-span-12 md:col-span-3 gap-y-2">
                    <div>
                        <div
                            class="bg-white min-w-3sq divider-x dark:bg-d-black-10 dark:border-none lg:h-3sq rounded-xl text-center"
                        >
                            <div
                                class="flex flex-col h-full md:flex-row-reverse lg:flex-col"
                            >
                                <div
                                    class="flex-1 items-center xs:border-none md:border-r lg:border-none border-gray-100 dark:border-[#EEF0F512]"
                                >
                                    <div
                                        class="flex h-36 ltr:direction-ltr"
                                    >
                                        <div class="flex-1 p-2">
                                            <div
                                                class="flex flex-col items-center h-full justify-between"
                                            >
                                                <div class="h-2/5 flex items-center justify-center">
                              <span
                                  class="text-3xl rtl font-bold text-blue cursor-default"
                                  id="clock"
                              ></span
                              >
                                                </div>
                                                <div
                                                    class="h-2/5 flex flex-col justify-around gap-y-0.5"
                                                >
                              <span
                                  class="flex items-center justify-center text-base font-bold text-blue-darkest dark:text-d-black-80"
                              >۲۱ آذر</span
                              >
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="flex-1 p-2 rtl:border-r ltr:border-l ltr:!border-r-0 border-gray-100 dark:border-[#EEF0F512]"
                                        >
                                            <div
                                                class="flex flex-col items-center h-full justify-between"
                                            >
                                                <div class="h-2/5">
                                                    <div
                                                        class="flex items-center justify-center gap-x-2 direction-rtl"
                                                    >
                                <span
                                    class="text-3xl font-bold"
                                    style="color: rgb(157, 160, 175)"
                                >۸°</span
                                ><img
                                                            width="50"
                                                            src="{{asset('dast/image/03d.svg')}}"
                                                            alt=""
                                                        />
                                                    </div>
                                                </div>
                                                <div
                                                    class="h-2/5 flex flex-col items-center justify-around gap-y-0.5"
                                                >
                                                    <div
                                                        class="flex flex-row gap-x-2 text-xs dark:text-d-black-80"
                                                    >
                                <span class="font-bold truncate"
                                >شال گردن لازمه</span
                                ><span>🧣</span>
                                                    </div>
                                                    <div
                                                        class="flex gap-x-1 items-center justify-center direction-rtl"
                                                    >
                                                        <div class="flex gap-x-1">
                                  <span
                                      class="text-2xs font-bold text-gray-500 dark:text-d-black-70"
                                  >۱۲°</span
                                  ><span
                                                                class="text-2xs text-gray-500 dark:text-d-black-60"
                                                            >حداکثر</span
                                                            >
                                                        </div>
                                                        <span class="text-2xs">.</span>
                                                        <div class="flex gap-x-1">
                                  <span
                                      class="text-2xs font-bold text-gray-500 dark:text-d-black-70"
                                  >۲°</span
                                  ><span
                                                                class="text-2xs text-gray-500 dark:text-d-black-60"
                                                            >حداقل</span
                                                            >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="flex-1 border-t md:border-t-0 lg:border-t border-gray-100 dark:border-[#EEF0F512] h-full"
                                >
                                    <div class="h-full flex justify-between items-center gap-x-3">
                                        <div
                                            class="flex flex-row justify-between w-full h-full ltr:direction-ltr lg:h-32 "
                                        >
                                            <p class="w-3/5 text-white mt-10" style="font-size: 12px;">
                                                این متن جهت تست است و لطفا در سریع ترین زمان ممکن با متن واقعی جایگزین
                                                شود
                                            </p>
                                            <div

                                                class="w-2/5 h-full flex items-end pb-px cursor-pointer justify-center"
                                            >
                                                <a

                                                    href="#"
                                                    class="w-28 lg:w-full"
                                                >
                                                    <video

                                                        id="handVideo"
                                                        src="{{asset('dast/image/asstin.webm')}}"
                                                    ></video
                                                    >
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="p-3"
                                        style="display: none"
                                    >
                                        <div
                                            class="flex flex-col py-1 px-2 gap-y-2 items-center"
                                        >
                                            <div
                                                class="flex items-center space-x-4 mt-2"
                                                dir="ltr"
                                            >
                                                <div class="space-y-1">
                                                    <input
                                                        type="text"
                                                        max="24"
                                                        class="timer-text"
                                                    />
                                                    <p
                                                        class="text-3xs text-grayish-500 dark:text-d-black-70"
                                                    >
                                                        ساعت
                                                    </p>
                                                </div>
                                                <div class="space-y-1">
                                                    <input

                                                        type="text"
                                                        min="0"
                                                        max="59"
                                                        class="timer-text"
                                                    />
                                                    <p

                                                        class="text-3xs text-grayish-500 dark:text-d-black-70"
                                                    >
                                                        دقیقه
                                                    </p>
                                                </div>
                                                <div class="space-y-1">
                                                    <input

                                                        type="text"
                                                        max="59"
                                                        min="0"
                                                        class="timer-text"
                                                    />
                                                    <p

                                                        class="text-3xs text-grayish-500 dark:text-d-black-70"
                                                    >
                                                        ثانیه
                                                    </p>
                                                </div>
                                            </div>
                                            <div

                                                class="flex justify-center gap-x-3 items-center"
                                            >
                                                <button

                                                    class="bg-blue text-[10px] text-white py-2 rounded-md w-20"
                                                >
                                                    شروع
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input id="jalalidata" type="text" data-jdp="" placeholder="نمایش تاریخ از امروز"
                                       style="visibility: hidden;margin-top: -20px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('dast/js/plugin.js')}}"></script>
<script src="{{asset('dast/js/jalalidatepicker.js')}}"></script>
<script>
    jalaliDatepicker.startWatch({
        minDate: "attr",
        maxDate: "attr"
    });
    /* Below is a js demo | you don't need to use */
    setTimeout(function () {
        var elm = document.querySelector('#jalalidata');
        elm.focus();
        jalaliDatepicker.hide();
        jalaliDatepicker.show(elm);
        jalaliDatepicker.updateOptions({
            autoHide: false,
            hideAfterChange: false,
            autoShow: true
        });
    }, 1000);
</script>
<script>
    var bol = true;

    function showBlur() {
        var btnBlur = document.querySelector('.btnBlur');
        var boxBlur = document.querySelector('.boxBlur');
        if (bol == true) {
            bol = false;
            boxBlur.style.filter = 'blur(5px)'
            btnBlur.style.backgroundColor = '#0000c2';
        } else if (bol == false) {
            bol = true;
            boxBlur.style.filter = 'blur(0)'
            btnBlur.style.backgroundColor = '#1c1f2b';
        }
    }
</script>
</body>
</html>
