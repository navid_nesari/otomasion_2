@extends('layouts.admin.admin')
@section('title',' مدیریت درخواست مساعده   ')
@section('pageTitle',' ثبت درخواست مساعده ')
@section('content')
    <div id="kt_content_container" class="container-xxl">
        <form id="kt_ecommerce_add_category_form"
              class="form d-flex flex-column flex-lg-row fv-plugins-bootstrap5 fv-plugins-framework"
              data-kt-redirect="" action="{{route('admin.request-for-help.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <!--begin::Main column-->
            <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10" style="margin-left: 20px ">
                <!--begin::General options-->
                <div class="card card-flush py-4">
                    <!--begin::Card header-->
                    <div class="card-header">
                        <div class="card-title">
                            <h2>ثبت درخواست مساعده</h2>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{route('admin.request-for-help.index')}}" class="btn btn-sm btn-light-success "
                               style="margin-left: 5px">
                                برگشت
                            </a>
                        </div>
                    </div>

                    <!--end::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body pt-0 mt-2">
                        <!--begin::Input group-->
                        <p>پرسنل گرامی شما میتوانید تا سقفط 7 میلیون تومان درخواست مساعده نمایید</p>
                        <br/>
                        <br/>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-10 fv-row fv-plugins-icon-container">
                                    <!--begin::Label-->
                                    <label class="required form-label">عنوان </label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    @include('admin.__components.input-text', [ 'name' => 'title'])
                                    <!--end::Input-->
                                    <div class="fv-plugins-message-container invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-10 fv-row fv-plugins-icon-container">
                                    <!--begin::Label-->
                                    <label class="required form-label"> مبلغ به تومان </label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    @include('admin.__components.input-text', [ 'name' => 'price','type'=>'number'])
                                    <!--end::Input-->

                                    <div class="fv-plugins-message-container invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <button type="submit" id="kt_ecommerce_add_category_submit" class="btn btn-primary">
                            <span class="indicator-label">ثبت اطلاعات  </span>

                        </button>
                        <!--end::Input group-->
                    </div>
                    <!--end::Card header-->
                </div>
                <!--end::General options-->
                <!--begin::Automation-->
                <!--end::Automation-->
            </div>
            <!--end::Main column-->
            <br/>
            <!--begin::Aside column-->
            <!--end::Aside column-->
            <div></div>
        </form>
    </div>
@endsection
@section('scripts')
    @include('admin.message.alert');
@endsection

