@extends('layouts.admin.admin')
@section('title',' مدیریت مقام ها  ')
@section('pageTitle',' لیست مقام ها ')

@section('content')

    <div class="d-flex flex-end mb-5">
        <button type="button" class="btn btn-primary px-4 py-2 ml-4" data-bs-toggle="collapse"
                href="#student_filters" role="button"
                aria-expanded="false" aria-controls="collapseExample" style="margin-left: 5px!important;">
            فیلتر
            <i class="fa fa-filter p-0 m-0"></i>
        </button>
    </div>
    <div class="card mb-5 pb-3 mb-xl-8 collapse @if(request()->has('title')) show @else @endif" id="student_filters">
        <form action="" class="form remove-empty-values" method="get" id="remove-empty-values">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
                <h3 class="card-title align-items-start flex-column">
                        <span class="card-label fw-bolder fs-3 mb-1">
                            <i class="fa fa-search text-white pl-1"></i>
                            جستجوی پیشرفته
                        </span>
                </h3>
            </div>
            <div class="card-body py-3">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <div class="form-group">
                            <label class="form-label fs-6 fw-bolder text-dark">جستجو بر اساس عنوان
                            </label>
                            <input class="form-control form-control-lg form-control-solid" name="name"
                                placeholder=""
                                value="{{ request()->has('name') ? request()->get('name') : null }}"/>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="d-flex flex-end">
                    <a href="{{route('admin.roles.all')}}" class="btn btn-sm btn-light-danger mx-1">
                        <i class="fa fa-eraser p-0 m-0"></i>
                        حذف فیلترها
                    </a>
                    <button type="submit" class="btn btn-sm btn-light-primary mx-1">
                        <i class="fa fa-search"></i>
                        فیلتر
                    </button>
                </div>
            </div>
        </form>
    </div>


    <div class="card">
        <div class="card-header border-0 pt-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label fw-bolder fs-3 mb-1"> لیست مقام ها </span>
            </h3>
            <div class="card-toolbar">
                <a href="{{route('admin.roles.create')}}" class="btn btn-sm btn-light-success me-1">
                    ثبت مقام جدید
                </a>
                <a href="{{ route('admin.permissions.all') }}" class="btn btn-sm btn-light-primary">
                    بخش دسترسی ها
                </a>
            </div>
        </div>

        <div class="card-body py-4">
            <!--begin::Table-->
            <div class="table-responsive">
                <table class="table align-middle table-row-dashed fs-6 gy-5 no-footer" id="kt_table_users">
                    <!--begin::Table head-->
                    <thead>
                    <tr class="text-start fs-6 text-uppercase gs-0">
                        <th class="min-w-125px">عنوان مقام</th>
                        <th class="min-w-200px">توضیحات</th>
                        <th class="min-w-125px">تنظیمات</th>
                    </tr>
                    </thead>
                    <tbody class="text-gray-600 fw-semibold">
                        @if($roles->count() > 0)
                            @foreach($roles as $role)
                                <tr>
                                    <td>{{ $role->name }}</td>
                                    <td>{{ $role->label }}</td>
                                    <td>
                                        <a href="{{ route('admin.roles.edit', $role) }}"
                                            class="btn btn-icon btn-sm btn-clean btn-light-primary btn-active-primary"
                                            data-inbox="dismiss" data-toggle="tooltip" title="ویرایش">
                                            <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
                                            <span class="svg-icon svg-icon-2 p-1">
                                                <i class="fa fa-pencil-alt"></i>
                                            </span>
                                            <!--end::Svg Icon-->
                                        </a>


                                        <a href="javascript:;"
                                            data-role-id="{{$role->id}}"
                                            class="btn btn-icon btn-sm btn-clean btn-light-danger btn-active-danger delete_role"
                                            data-inbox="dismiss" data-toggle="tooltip" title="حذف">
                                            <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
                                            <span class="svg-icon svg-icon-2 p-1">
                                                <i class="fa fa-trash"></i>
                                            </span>
                                            <!--end::Svg Icon-->
                                        </a>
                                    </td>
                                    <!--end::Action=-->
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" style="text-align:center;color:red">
                                    <a class="btn btn-danger" href="">
                                    اطلاعات موجود نیست<i class="icon-warning2 mr-3 icon-1x"></i>
                                    </a>
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <!--end::Table-->
        </div>

    </div>

@endsection

@section('scripts')
    @include('admin.message.alert')

    <script>
        $('#remove-empty-values').submit(function () {
            $(this).find(':input').filter(function () {
                return !this.value;
            }).attr('disabled', 'disabled');
            return true;
        });
    </script>

    <script type="text/javascript">
        $('.delete_role').click(function (e) {
            e.preventDefault();
            // console.log('role id : ' + $(this).data('role-id'));
            let role_id = $(this).data('role-id');
            let url = "{{ route('admin.roles.logicalDeletion', ":role-id")}}";
            url = url.replace(":role-id", role_id)

            Swal.fire({
                title: 'مطمئن هستید که می خواهید حذف کنید؟',
                text: "در صورت حذف فایل قابل بازگشت نمی باشد.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'بله. حذف شود',
                cancelButtonText: 'خیر',
            }).then((willDelete) => {
                if (willDelete.isConfirmed) {
                    window.location = url;
                    Swal.fire(
                        'حذف شد',
                        'فرد مورد نظر با موفقیت حذف شد.',
                        'success'
                    )
                }
            })
        })

    </script>

@endsection


