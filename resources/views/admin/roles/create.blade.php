@extends('layouts.admin.admin')

@section('title',' مدیریت مقام ها  ')
@section('pageTitle',' ثبت مقام ')
@section('style')
 {{-- <link href="{{ asset('admin-assets/css/select2.min.css') }}" rel="stylesheet" type="text/css"/> --}}
@endsection
@section('content')
    <div id="kt_content_container" class="container-xxl">
        <form id="kt_ecommerce_add_category_form"
              class="form d-flex flex-column flex-lg-row fv-plugins-bootstrap5 fv-plugins-framework"
              data-kt-redirect="" action="{{ route('admin.roles.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <!--begin::Main column-->
            <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10" style="margin-left: 20px ">
                <!--begin::General options-->
                <div class="card card-flush py-4">
                    <!--begin::Card header-->
                    <div class="card-header">
                        <div class="card-title">
                            <h2>ثبت مقام جدید</h2>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{route('admin.roles.all')}}" class="btn btn-sm btn-light-success "
                               style="margin-left: 5px">
                                برگشت
                            </a>

                        </div>
                    </div>
                    <!--end::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body pt-0 mt-2">
                        <!--begin::Input group-->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-10 fv-row fv-plugins-icon-container">
                                    <!--begin::Label-->
                                    <label class="required form-label">عنوان</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                       @include('admin.__components.input-text', [ 'name' => 'name'])
                                     <!--end::Input-->
                                    <div class="fv-plugins-message-container invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-10 fv-row fv-plugins-icon-container">
                                    <label class="required form-label">اجازه دسترسی</label>
                                        {{-- <label for="permissions">اجازه دسترسی</label> --}}
                                        {{-- <select name="permissions[]" id="permissions" class="form-control mt-1" multiple>
                                            @foreach ($permissions as $permission)
                                                <option value="{{$permission->id}}">{{$permission->name}} - {{ $permission->label }}</option>
                                            @endforeach
                                        </select> --}}
                                        <select class="form-control select2"
                                                id="kt_select2_permissions" multiple="multiple"  name="permissions[]">
                                                @foreach ($permissions as $permission)
                                                    <option value="{{$permission->id}}">{{$permission->name}} - {{ $permission->label }}</option>
                                                @endforeach
                                        </select>
                                        @error('permissions')
                                        <p class="text-danger">{{$message}}</p>
                                        @enderror
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-10 fv-row fv-plugins-icon-container">
                                    <!--begin::Label-->
                                    <label class="required form-label">توضیحات</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    @include('admin.__components.textarea', [ 'name' => 'label'])
                                    <!--end::Input-->
                                    <div class="fv-plugins-message-container invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <button type="submit" id="kt_ecommerce_add_category_submit" class="btn btn-primary">
                            <span class="indicator-label">ثبت اطلاعات  </span>
                        </button>
                        <!--end::Input group-->
                    </div>
                    <!--end::Card header-->
                </div>
                <!--end::General options-->
                <!--begin::Automation-->
                <!--end::Automation-->
            </div>
            <!--end::Main column-->
            <br/>

            <div></div>
        </form>
    </div>
@endsection

@section('scripts')
{{-- <script src="{{ asset('admin-assets/js/select2.min.js') }}"></script> --}}
<script>
    $(document).ready(function () {

        // Format selection
        $('#kt_select2_permissions').selectpicker();

    })
</script>
@endsection

