@extends('layouts.admin.admin')
@section('title',' مدیریت درخواست ماموریت   ')
@section('pageTitle','فهرست درخواست  ماموریت ')
@section('content')

    <div class="card" data-select2-id="select2-data-131-rhmf">
        <div class="card-header border-0 pt-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label fw-bolder fs-3 mb-1"> فهرست درخواست های ماموریت من </span>
            </h3>
            <div class="card-toolbar">

                <a href="{{route('admin.missions.create')}}" class="btn btn-sm btn-light-success">ثبت درخواست جدید</a>
            </div>
        </div>
        <!--begin::Card body-->
        <div class="card-body pt-0">

            <!--begin::Table-->
            <div id="kt_customers_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <div class="table-responsive">
                    <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
                           id="kt_customers_table">
                        <!--begin::Table head-->
                        <thead>
                        <!--begin::Table row-->
                        <tr class="min-w-125px sorting">
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="Customer Name: activate to sort column ascending">نام و نام خانوادگی
                            </th>
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="Customer Name: activate to sort column ascending">بخش
                            </th>
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="IP Address: activate to sort column ascending"> محل ماموریت
                            </th>
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="Customer Name: activate to sort column ascending"> تاریخ
                            </th>
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="Customer Name: activate to sort column ascending"> ساعت شروع
                            </th>
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="Customer Name: activate to sort column ascending"> ساعت پایان
                            </th>
                            <th class="min-w-125px sorting" rowspan="1" colspan="1"
                                style="width: 162.9px;" aria-label="Actions"> وضعیت
                            </th>
                            <th class="min-w-125px sorting" rowspan="1" colspan="1"
                                style="width: 162.9px;" aria-label="Actions">نمایش پاسخ
                            </th>
                        </tr>
                        <!--end::Table row-->
                        </thead>
                        <!--end::Table head-->
                        <!--begin::Table body-->
                        <tbody class="fw-bold text-gray-600">
                        @if($data->count() > 0)
                            @foreach($data as $item)
                                <tr class="odd">
                                    <td>{{ $item->full_name }}</td>
                                    <td>{{ $item->part }}</td>
                                    <td>{{ $item->mission }}</td>
                                    <td>{{ jdate($item->date)->format('Y-m-d') }}</td>
                                    <td>{{ $item->date_start }}</td>
                                    <td>{{ $item->date_end }}</td>
                                    <td>{!! $item->webPresent()->status !!}</td>
                                    <td>{!! $item->answer !!}</td>
                                    <!--begin::Action=-->
                                    <!--end::Action=-->
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" style="text-align:center;color:red">
                                    <a class="btn btn-danger" href="">
                                       اطلاعات موجود نیست<i class="icon-warning2 mr-3 icon-1x"></i>
                                    </a>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                        <!--end::Table body-->
                    </table>
                </div>
                <div class="row">
                    <div
                        class="col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end">

                    </div>
                </div>
            </div>
            <!--end::Table-->
            {{-- {{$data->links('vendor.pagination.bootstrap-4') }} --}}
        </div>
        <!--end::Card body-->
    </div>
@endsection
@section('scripts')
    @include('admin.message.alert')
@endsection
