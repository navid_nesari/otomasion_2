@extends('layouts.admin.admin')

@section('title',' مدیریت مقام ها  ')
@section('pageTitle',' ثبت مقام ')
@section('style')
 {{-- <link href="{{ asset('admin-assets/css/select2.min.css') }}" rel="stylesheet" type="text/css"/> --}}
@endsection
@section('content')
    <div id="kt_content_container" class="container-xxl">
        <form id="kt_ecommerce_add_category_form"
              class="form d-flex flex-column flex-lg-row fv-plugins-bootstrap5 fv-plugins-framework"
              data-kt-redirect="" action="{{ route('admin.levels.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <!--begin::Main column-->
            <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10" style="margin-left: 20px ">
                <!--begin::General options-->
                <div class="card card-flush py-4">
                    <!--begin::Card header-->
                    <div class="card-header">
                        <div class="card-title">
                            <h2>ثبت مقام جدید</h2>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{route('admin.levels.all')}}" class="btn btn-sm btn-light-success "
                               style="margin-left: 5px">
                                برگشت
                            </a>

                        </div>
                    </div>
                    <!--end::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body pt-0 mt-2">
                        <!--begin::Input group-->
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group mb-8">
                                    <label class="required form-label">مدیران</label>
                                    {{-- <label for="user_id"> مدیران</label> --}}
                                    <select name="admins" id="admin_id" class="form-control mt-1" >
                                        @foreach ($admins as $admin)
                                            <option value="{{$admin->id}}">{{ $admin->email }} - {{$admin->first_name.' '.$admin->last_name}}</option>
                                        @endforeach
                                    </select>
                                    @error('admins')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="mb-10 fv-row fv-plugins-icon-container">
                                    <label class="required form-label">مقام ها</label>
                                    <select class="form-control select2" id="kt_select2_roles" name="roles">
                                            @foreach ($roles as $role)
                                                <option value="{{$role->id}}">{{$role->name}} - {{ $role->label }}</option>
                                            @endforeach
                                    </select>
                                    @error('roles')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                    <div class="fv-plugins-message-container invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <button type="submit" id="kt_ecommerce_add_category_submit" class="btn btn-primary">
                            <span class="indicator-label">ثبت اطلاعات  </span>
                        </button>
                        <!--end::Input group-->
                    </div>
                    <!--end::Card header-->
                </div>
                <!--end::General options-->
                <!--begin::Automation-->
                <!--end::Automation-->
            </div>
            <!--end::Main column-->
            <br/>

            <div></div>
        </form>
    </div>
@endsection

@section('scripts')
{{-- <script src="{{ asset('admin-assets/js/select2.min.js') }}"></script> --}}
<script>
    $(document).ready(function () {

        // Format selection
        $('#kt_select2_roles').selectpicker();

    })
</script>
<script>
    $(document).ready(function () {
        // Format selection
        $('#admin_id').selectpicker();
    })

</script>
@endsection

