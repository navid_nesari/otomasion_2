@extends('layouts.admin.admin')

@section('title',' مدیریت سطوح مدیران  ')
@section('pageTitle',' ویرایش سطوح مدیران ')
@section('content')
    <div id="kt_content_container" class="container-xxl">
        <div class="row mt-4">
            <form class="form d-flex flex-column flex-lg-row mb-3"
                  action="{{ route('admin.levels.update', $admin) }}" method="post"
                  enctype="multipart/form-data">
            @csrf
                <!--begin::Main column-->
                <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10 ms-lg-2" style="margin-left: 20px ">
                    <!--begin::General options-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>ویرایش  سطوح مدیر</h2>
                            </div>
                            <div class="card-toolbar">
                                <a href="{{route('admin.levels.all')}}" class="btn btn-sm btn-light-success "
                                   style="margin-left: 5px">
                                    بازگشت
                                </a>

                            </div>
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">

                            <!--begin::Input group-->
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <label class="required form-label">مقام - {{$admin->email}}</label>
                                        <select class="form-control select2" id="kt_select2_roles" name="roles">
                                                @foreach ($roles as $role)
                                                    <option value="{{$role->id}}" {{$admin->hasRole($role->name) ? 'selected' :''}}>
                                                        {{$role->name}} - {{ $role->label }}
                                                    </option>
                                                @endforeach
                                        </select>
                                        @error('roles')
                                        <p class="text-danger">{{$message}}</p>
                                        @enderror
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <!--begin::Button-->
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label">ثبت تغییرات</span>
                            </button>
                            <!--end::Button-->
                        </div>
                        <!--end::Card header-->
                    </div>
                    <!--end::General options-->

                </div>
                <!--end::Main column-->

            </form>
        </div>
    </div>
@endsection
@section('scripts')
<script>
    $(document).ready(function () {

        // Format selection
        $('#kt_select2_roles').selectpicker();

    })
</script>
@endsection
