@extends('layouts.admin.admin')
@section('title',' مدیریت دفاتر تلفن  ')
@section('pageTitle',' ویرایش  دفتر تلفن ')
@section('content')

    <div id="kt_content_container" class="container-xxl">
        <form id="" class=""
              data-kt-redirect="" action="{{route('admin.phonebooks.update', $phonebook)}}" method="post" enctype="multipart/form-data">
        @csrf
        <!--begin::Main column-->
            <div class="row">
                <div class="col">
                    <!--begin::General options-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>ویرایش دفتر تلفن</h2>
                            </div>
                            <div class="card-toolbar">
                                <a href="{{route('admin.phonebooks.all')}}" class="btn btn-sm btn-light-success "
                                   style="margin-left: 5px">
                                    برگشت
                                </a>

                            </div>
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">
                            <!--begin::Input group-->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">نام و نام خانوادگی </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                                'name' => 'full_name',
                                                'value'=>$phonebook->full_name
                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">شماره موبایل </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                                'name' => 'mobile',
                                                'value' => $phonebook->mobile
                                            ])
                                        <!--end::Input-->

                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">شماره تلفن </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                            'name' => 'phone',
                                            'value' => $phonebook->phone
                                        ])
                                        <!--end::Input-->

                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">شماره حساب</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                            'name' => 'account_number',
                                            'value' => $phonebook->account_number
                                        ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label"> آدرس</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                            'name' => 'address',
                                            'value' => $phonebook->address
                                        ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">توضیحات</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.textarea', [
                                                'name' => 'description',
                                                'value' => $phonebook->description
                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <button type="submit" id="kt_ecommerce_add_category_submit" class="btn btn-primary">
                                <span class="indicator-label">ثبت اطلاعات  </span>

                            </button>
                            <!--end::Input group-->
                        </div>
                        <!--end::Card header-->
                    </div>
                    <!--end::General options-->
                    <!--begin::Automation-->
                    <!--end::Automation-->
                </div>

            </div>
        </form>
    </div>
@endsection
@section('scripts')
    <script>

        // load select 2 for select Roles :

        $('#roles').select2({
            placeholder: 'نقش کاربر را انتخاب نمایید',
        });

        // load select 2 for select Permissions :

        $('#permissions').select2({
            dir: 'RTL',
            placeholder: 'مجوزهای کاربر را انتخاب نمایید',
            tags: true
        });

        $('#admins').select2({
            placeholder: '\ کاربر را انتخاب نمایید',
        });


    </script>
@endsection
