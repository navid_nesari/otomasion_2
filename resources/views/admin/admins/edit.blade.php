@extends('layouts.admin.admin')
@section('content')
    @include('admin.admins.index',[
        'active' =>'edit'
    ])
    <div id="kt_content_container" class="container-xxl">
        <form id="" class=""
              data-kt-redirect="" action="{{route('admin.admins.update', $admin)}}" method="post" enctype="multipart/form-data">
        @csrf
        <!--begin::Main column-->
            <div class="row">
                <div class="col-md-8">
                    <!--begin::General options-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>ثبت مدیر جدید</h2>
                            </div>
                            <div class="card-toolbar">
                                <a href="{{route('admin.admins.all')}}" class="btn btn-sm btn-light-success "
                                   style="margin-left: 5px">
                                    برگشت
                                </a>

                            </div>
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">
                            <!--begin::Input group-->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">نام  </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [ 'name' => 'f_name','value'=>$admin->first_name])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">نام خانوادگی </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [ 'name' => 'l_name','value'=>$admin->last_name])
                                        <!--end::Input-->

                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">شماره موبایل </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [ 'name' => 'mobile','value'=>$admin->mobile])
                                        <!--end::Input-->

                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">ایمیل</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [ 'name' => 'email','value'=>$admin->email])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">نام کاربری</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                            'name' => 'username',
                                            'value'=>$admin->username
                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">پسورد</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [ 'name' => 'password','type'=>'password'])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">نام پدر</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                           @include('admin.__components.input-text', [
                                             'name' => 'father_name',
                                             'value'=>$admin->father_name
                                            ])
                                         <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">شماره شناسنامه</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                       @include('admin.__components.input-text', [
                                        'name' => 'birth_certificate_number',
                                        'value' => $admin->birth_certificate_number
                                        ])
                                       <!--end::Input-->

                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">محل صدور</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                          @include('admin.__components.input-text', [
                                            'name' => 'issued_location',
                                            'value' => $admin->issued_location
                                            ])
                                         <!--end::Input-->

                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">تاریخ تولد</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                           @include('admin.__components.datepicker', [
                                            'name' => 'birth_date',
                                            'value' => $admin->birth_date
                                            ])
                                         <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">دین</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                       @include('admin.__components.input-text', [
                                        'name' => 'religion',
                                        'value' => $admin->religion
                                        ])
                                       <!--end::Input-->

                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">مذهب</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                          @include('admin.__components.input-text', [
                                            'name' => 'religious_branch',
                                            'value' => $admin->religious_branch
                                            ])
                                         <!--end::Input-->

                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">ملیت</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                           @include('admin.__components.input-text', [
                                            'name' => 'nationality',
                                            'value' => $admin->nationality
                                            ])
                                         <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">کد پستی</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                       @include('admin.__components.input-text', [
                                        'name' => 'postal_code',
                                        'value' => $admin->postal_code
                                        ])
                                       <!--end::Input-->

                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">آدرس</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                            @include('admin.__components.textarea', [
                                                'name' => 'address',
                                                'value' => $admin->address
                                                ])
                                            <!--end::Input-->

                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end::Card header-->
                    </div>
                    <div class="card card-flush py-4 mt-10">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>وضعیت تاهل</h2>
                            </div>

                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">
                            <!--begin::Input group-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">وضعیت تاهل</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                           @include('admin.__components.horizontal-radiobutton', [
                                             'activeKey' => $admin->marital_status,
                                             'name' => 'marital_status',
                                             'items' => $activityMaritalStatuses
                                            ])
                                         <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class=" form-label">تعداد فرزندان</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                        'name' => 'children_number',
                                        'value' => $admin->children_number,
                                        ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end::Card header-->
                    </div>
                    <div class="card card-flush py-4 mt-10">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>خدمت وظیفه</h2>
                            </div>

                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">
                            <!--begin::Input group-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">خدمت سربازی</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                           @include('admin.__components.horizontal-radiobutton', [
                                             'activeKey' => $admin->military_service,
                                             'name' => 'military_service',
                                             'items' => $activityMilitaryStatuses
                                            ])
                                         <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class=" form-label">مشخصات محل خدمت</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                        'name' => 'details_military_location',
                                        'value' => $admin->details_military_location
                                        ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class=" form-label">تاریخ پایان خدمت</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.datepicker', [
                                            'name' => 'until_date',
                                            'value' => $admin->until_date
                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class=" form-label">نوع معافیت</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                        'name' => 'exemption_type',
                                        'value' => $admin->exemption_type
                                        ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class=" form-label">نوع معافیت پزشکی</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                        'name' => 'medical_exemption_type',
                                        'value' => $admin->medical_exemption_type
                                        ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end::Card header-->
                    </div>
                    <div class="card card-flush py-4 mt-10">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>تحصیلات</h2>
                            </div>

                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">
                            <!--begin::Input group-->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">آخرین مدرک تحصیلی</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                        'name' => 'last_educational_certificate',
                                        'value' => $admin->last_educational_certificate
                                        ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label"> رشته تحصیلی</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                            'name' => 'study_field',
                                            'value' => $admin->study_field
                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label"> محل تحصیل</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                            'name' => 'education_place',
                                            'value' => $admin->education_place
                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end::Card header-->
                    </div>
                    <div class="card card-flush py-4 mt-10">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>مدارک حرفه ای و تخصصی</h2>
                            </div>

                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">
                            <!--begin::Input group-->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">نام آموزشگاه</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                        'name' => 'school_name',
                                        'value' => json_decode($admin->professional_documents, true)['school_name']
                                        ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">نوع تخصص</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                            'name' => 'specialization_type',
                                            'value' => json_decode($admin->professional_documents, true)['specialization_type']
                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">تاریخ اخذ مدرک</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.datepicker', [
                                            'name' => 'getting_degree_date',
                                            'value' => json_decode($admin->professional_documents, true)['getting_degree_date']
                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end::Card header-->
                    </div>
                    <div class="card card-flush py-4 mt-10">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>آشنایی با زبان خارجه</h2>
                            </div>

                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">
                            <!--begin::Input group-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">نوع زبان</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                        'name' => 'language_type',
                                        'value' => json_decode($admin->foreign_language_proficiency, true)['language_type']
                                        ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">سطح آشنایی</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                            'name' => 'mastery_level',
                                            'value' => json_decode($admin->foreign_language_proficiency, true)['mastery_level']
                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>

                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end::Card header-->
                    </div>
                    <div class="card card-flush py-4 mt-10">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>سوابق کاری</h2>
                            </div>

                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">
                            <!--begin::Input group-->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">نام محل کار</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                        'name' => 'workplace_name',
                                        'value' => json_decode($admin->resume, true)['workplace_name']
                                        ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">

                                        <!--begin::Label-->
                                        <label class="form-label">تاریخ شروع</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        {{-- @include('admin.__components.datepicker', [ 'name' => 'start_date']) --}}
                                        <input type="text" class="form-control" id="start_date" name="start_date"
                                            placeholder="{{ isset($placeholder) ? $placeholder : "تاریخ" }}"
                                            value="{{ old( 'start_date', json_decode($admin->resume, true)['start_date'] ) }}"/>
                                        <!--end::Input-->


                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">تاریخ ترک کار</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.datepicker', [
                                            'name' => 'end_date',
                                            'value' => json_decode($admin->resume, true)['end_date']
                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">تلفن محل کار</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                        'name' => 'office_phone',
                                        'value' => json_decode($admin->resume, true)['office_phone']
                                        ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">عنوان شغلی</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                            'name' => 'job_title',
                                            'value' => json_decode($admin->resume, true)['job_title']
                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">میزان آخرین حقوق</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                            'name' => 'amount_last_salary',
                                            'value' => json_decode($admin->resume, true)['amount_last_salary']
                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">علت ترک کار</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.textarea', [
                                            'name' => 'reason_leaving_work',
                                            'value' => json_decode($admin->resume, true)['reason_leaving_work']
                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end::Card header-->
                    </div>
                    <div class="card card-flush py-4 mt-10">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>وضعیت مسکن</h2>
                            </div>

                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">
                            <!--begin::Input group-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">وضعیت مسکن</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                           @include('admin.__components.horizontal-radiobutton', [
                                             'activeKey' => $admin->housing_situation,
                                             'name' => 'housing_situation',
                                             'items' => $activityHousingStatuses
                                            ])
                                         <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class=" form-label">مبلغ اجاره پرداختی</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                        'name' => 'rent_paid',
                                        'value' => $admin->rent_paid
                                         ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end::Card header-->
                    </div>
                    <div class="card card-flush py-4 mt-10">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>معرف یا ضامن</h2>
                            </div>

                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">
                            <!--begin::Input group-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">نام و نام خانوادگی</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                        'name' => 'full_name',
                                        'value' => $admin->guarantor['full_name']
                                        ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">شغل</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                            'name' => 'guarantor_job',
                                            'value' => $admin->guarantor['guarantor_job']
                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">نسبت</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                            'name' => 'ratio',
                                            'value' => $admin->guarantor['ratio']
                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">تلفن</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                            'name' => 'guarantor_phone',
                                            'value' => $admin->guarantor['guarantor_phone']
                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">آدرس محل کار یا منزل</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.textarea', [
                                            'name' => 'guarantor_address',
                                            'value' => $admin->guarantor['guarantor_address']
                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end::Card header-->
                    </div>
                    <div class="card card-flush py-4 mt-10">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>شغل دوم</h2>
                            </div>

                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">
                            <!--begin::Input group-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">شغل دوم</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                           @include('admin.__components.horizontal-radiobutton', [
                                             'activeKey' => $admin->second_job,
                                             'name' => 'second_job',
                                             'items' => $activityJobStatuses
                                            ])
                                         <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class=" form-label">نام سازمان</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                        'name' => 'organization',
                                        'value' => $admin->organization
                                        ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class=" form-label">نوع شغل دوم</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                        'name' => 'second_job_type',
                                        'value' => $admin->second_job_type
                                        ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class=" form-label">میزان حقوق</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                        'name' => 'salary_amount',
                                        'value' => $admin->salary_amount
                                        ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class=" form-label">ساعت کاری</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                        'name' => 'working_hours',
                                        'value' => $admin->working_hours
                                        ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class=" form-label">نوع شیفت کاری</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                        'name' => 'type_work_shift',
                                        'value' => $admin->type_work_shift
                                        ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class=" form-label">تلفن محل کار</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                        'name' => 'work_phone',
                                        'value' => $admin->work_phone
                                        ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="form-label">آدرس محل کار</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.textarea', [
                                            'name' => 'work_address',
                                            'value' => $admin->work_address
                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end::Card header-->
                    </div>
                    <div class="mt-10">
                        <!--begin::Button-->
                        <button type="submit" id="kt_ecommerce_add_category_submit" class="btn btn-primary">
                            <span class="indicator-label">ثبت اطلاعات</span>

                        </button>
                        <!--end::Button-->
                    </div>
                    <!--end::General options-->
                    <!--begin::Automation-->
                    <!--end::Automation-->
                </div>
                <div class="col-md-4">
                    <!--begin::Status-->
                    <div class="card card-flush py-4 mb-10">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <h2>وضعیت</h2>
                            </div>
                            <!--end::Card title-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0">
                            <!--begin::Select2-->
                            @include('admin.__components.horizontal-radiobutton', [
                              'activeKey' => $admin->status ,
                              'name' => 'status',
                              'items' => $activityStatuses
                              ])
                            <div class="d-none mt-10">
                                <label for="kt_ecommerce_add_category_status_datepicker" class="form-label">Select
                                    publishing date and time</label>
                                <input class="form-control flatpickr-input" id="kt_ecommerce_add_category_status_datepicker"
                                       placeholder="Pick date &amp; time" type="text" readonly="readonly">
                            </div>
                            <!--end::Datepicker-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Status-->
                    <!--begin::Thumbnail settings-->
                    <div class="card card-flush py-4 mb-10">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <h2>تصویر شناسنامه</h2>
                            </div>
                            <!--end::Card title-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body text-center pt-0">
                            <!--begin::Image input-->
                              @include('admin.__components.image-input', [
                                'name' => 'birth_certificate_image',
                                'imageUrl'=>$admin->webPresent()->birth_certificate_image
                                ])

                            <!--end::Image input-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <div class="card card-flush py-4 mb-10">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <h2>تصویر کارت ملی</h2>
                            </div>
                            <!--end::Card title-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body text-center pt-0">
                            <!--begin::Image input-->
                              @include('admin.__components.image-input', [
                                'name' => 'national_card_image',
                                'imageUrl'=>$admin->webPresent()->national_card_image
                                ])

                            <!--end::Image input-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <div class="card card-flush py-4 mb-10">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <h2>تصویر </h2>
                            </div>
                            <!--end::Card title-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body text-center pt-0">
                            <!--begin::Image input-->
                            @include('admin.__components.image-input', [
                                'name' => 'image',
                                'imageUrl'=>$admin->webPresent()->image
                                ])

                            <!--end::Image input-->
                        </div>
                        <!--end::Card body-->
                    </div>

                    <!--end::Thumbnail settings-->
                </div>
            </div>
        </form>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#start_date").pDatepicker({
                initialValue : true,
                format: 'L',
                // defaultDate: null
            });
        });
    </script>
    <script>

        // load select 2 for select Roles :

        $('#roles').select2({
            placeholder: 'نقش کاربر را انتخاب نمایید',
        });

        // load select 2 for select Permissions :

        $('#permissions').select2({
            dir: 'RTL',
            placeholder: 'مجوزهای کاربر را انتخاب نمایید',
            tags: true
        });

        $('#admins').select2({
            placeholder: '\ کاربر را انتخاب نمایید',
        });


    </script>
@endsection
