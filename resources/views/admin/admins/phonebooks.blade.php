@extends('layouts.admin.admin')
@section('content')
    @include('admin.admins.index',[
        'active' =>'phonebooks'
])
    <div class="card" data-select2-id="select2-data-131-rhmf">
        <div class="card-header border-0 pt-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label fw-bolder fs-3 mb-1"> لیست دفاتر تلفن </span>
            </h3>
{{--            <div class="card-toolbar">--}}

{{--                <a href="{{route('admin.phonebooks.create')}}" class="btn btn-sm btn-light-success">--}}
{{--                    ثبت دفتر تلفن جدید--}}
{{--                </a>--}}
{{--            </div>--}}
        </div>
        <!--begin::Card body-->
        <div class="card-body pt-0">

            <!--begin::Table-->
            <div id="kt_customers_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <div class="table-responsive">
                    <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
                           id="kt_customers_table">
                        <!--begin::Table head-->
                        <thead>
                        <!--begin::Table row-->
                        <tr class="min-w-125px sorting">
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="Customer Name: activate to sort column ascending">سازنده
                            </th>
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="Customer Name: activate to sort column ascending">نام و نام خانوادگی
                            </th>
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="IP Address: activate to sort column ascending">تلفن همراه
                            </th>
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="Customer Name: activate to sort column ascending">شماره حساب
                            </th>
                            <th class="min-w-125px sorting" rowspan="1" colspan="1"
                                style="width: 162.9px;" aria-label="Actions">تنظیمات
                            </th>
                        </tr>
                        <!--end::Table row-->
                        </thead>
                        <!--end::Table head-->
                        <!--begin::Table body-->
                        <tbody class="fw-bold text-gray-600">
                        @if($phonebooks->count() > 0)
                            @foreach($phonebooks as $phonebook)
                                <tr class="odd">
                                    <td>{{ $phonebook->admin->webPresent()->fullName }}</td>
                                    <td>
                                        <a href="#"
                                           class="text-gray-600 text-hover-primary mb-1">  {{$phonebook->full_name}}</a>
                                    </td>
                                    <td>{!! $phonebook->mobile !!}</td>
                                    <td>{!! $phonebook->account_number !!}</td>
                                    <!--begin::Action=-->
                                    <td class="">
                                        <a href="{{ route('admin.phonebooks.edit', $phonebook) }}"
                                           class="btn btn-icon btn-light-info btn-sm me-1">
                                             <span class="svg-icon svg-icon-3">
                                                <i class="fa fa-layer-group"></i>
                                            </span>
                                        </a>
                                        <a data-id={{ $phonebook->id }} href="javascript"
                                           class="btn btn-icon btn-light-danger btn-sm me-1 delete-confirm"
                                           data-kt-customer-table-filter="delete_row">
                                              <span class="svg-icon svg-icon-3">
                                                <i class="fa fa-trash"></i>
                                            </span>
                                        </a>
                                    </td>
                                    <!--end::Action=-->
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" style="text-align:center;color:red">
                                    <a class="btn btn-danger" href="">
                                        اطلاعات موجود نیست<i class="icon-warning2 mr-3 icon-1x"></i>
                                    </a>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                        <!--end::Table body-->
                    </table>
                </div>
                <div class="row">
                    <div
                        class="col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end">

                    </div>
                </div>
            </div>
            <!--end::Table-->
            {{-- {{$phonebooks->links('vendor.pagination.bootstrap-4') }} --}}
        </div>
        <!--end::Card body-->
    </div>

@endsection

