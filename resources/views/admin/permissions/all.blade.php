@extends('layouts.admin.admin')
@section('title',' مدیریت مجوز ها  ')
@section('pageTitle',' لیست مجوز ها ')

@section('content')

    <div class="card">
        <div class="card-header border-0 pt-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label fw-bolder fs-3 mb-1"> لیست مجوز ها </span>
            </h3>
            <div class="card-toolbar">
                <a href="{{route('admin.permissions.create')}}" class="btn btn-sm btn-light-success me-1">
                    ثبت مجوز جدید
                </a>
            </div>
        </div>

        <div class="card-body py-4">
            <!--begin::Table-->
            <div class="table-responsive">
                <table class="table align-middle table-row-dashed fs-6 gy-5 no-footer" id="kt_table_users">
                    <!--begin::Table head-->
                    <thead>
                    <tr class="text-start fs-6 text-uppercase gs-0">
                        <th class="min-w-125px">عنوان مجوز</th>
                        <th class="min-w-200px">توضیحات</th>
                        <th class="min-w-125px">تنظیمات</th>
                    </tr>
                    </thead>
                    <tbody class="text-gray-600 fw-semibold">
                        @if($permissions->count() > 0)
                            @foreach($permissions as $permission)
                                <tr>
                                    <td>{{ $permission->name }}</td>
                                    <td>{{ $permission->label }}</td>
                                    <td>
                                        <a href="{{ route('admin.permissions.edit', $permission) }}"
                                            class="btn btn-icon btn-sm btn-clean btn-light-primary btn-active-primary"
                                            data-inbox="dismiss" data-toggle="tooltip" title="ویرایش">
                                            <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
                                            <span class="svg-icon svg-icon-2 p-1">
                                                <i class="fa fa-pencil-alt"></i>
                                            </span>
                                            <!--end::Svg Icon-->
                                        </a>


                                        <a href="javascript:;"
                                            data-permission-id="{{$permission->id}}"
                                            class="btn btn-icon btn-sm btn-clean btn-light-danger btn-active-danger delete_permission"
                                            data-inbox="dismiss" data-toggle="tooltip" title="حذف">
                                            <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
                                            <span class="svg-icon svg-icon-2 p-1">
                                                <i class="fa fa-trash"></i>
                                            </span>
                                            <!--end::Svg Icon-->
                                        </a>
                                    </td>
                                    <!--end::Action=-->
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" style="text-align:center;color:red">
                                    <a class="btn btn-danger" href="">
                                    اطلاعات موجود نیست<i class="icon-warning2 mr-3 icon-1x"></i>
                                    </a>
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <!--end::Table-->
        </div>

    </div>

@endsection

@section('scripts')
    @include('admin.message.alert')

    <script>
        $('#remove-empty-values').submit(function () {
            $(this).find(':input').filter(function () {
                return !this.value;
            }).attr('disabled', 'disabled');
            return true;
        });
    </script>

    <script type="text/javascript">
        $('.delete_permission').click(function (e) {
            e.preventDefault();
            // console.log('permission id : ' + $(this).data('permission-id'));
            let permission_id = $(this).data('permission-id');
            let url = "{{ route('admin.permissions.logicalDeletion', ":permission-id")}}";
            url = url.replace(":permission-id", permission_id)

            Swal.fire({
                title: 'مطمئن هستید که می خواهید حذف کنید؟',
                text: "در صورت حذف فایل قابل بازگشت نمی باشد.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'بله. حذف شود',
                cancelButtonText: 'خیر',
            }).then((willDelete) => {
                if (willDelete.isConfirmed) {
                    window.location = url;
                    Swal.fire(
                        'حذف شد',
                        'فرد مورد نظر با موفقیت حذف شد.',
                        'success'
                    )
                }
            })
        })

    </script>

@endsection


