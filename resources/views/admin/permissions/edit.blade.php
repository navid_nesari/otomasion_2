@extends('layouts.admin.admin')

@section('title',' مدیریت مجوز ها  ')
@section('pageTitle',' ویرایش مجوز ها ')
@section('content')
    <div id="kt_content_container" class="container-xxl">
        <div class="row mt-4">
            <form class="form d-flex flex-column flex-lg-row mb-3"
                  action="{{ route('admin.permissions.update', $permission) }}" method="post"
                  enctype="multipart/form-data">
            @csrf
                <!--begin::Main column-->
                <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10 ms-lg-2" style="margin-left: 20px ">
                    <!--begin::General options-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>ویرایش  مجوز</h2>
                            </div>
                            <div class="card-toolbar">
                                <a href="{{route('admin.permissions.all')}}" class="btn btn-sm btn-light-success "
                                   style="margin-left: 5px">
                                    بازگشت
                                </a>

                            </div>
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">

                            <!--begin::Input group-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">عنوان مجوز</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                                        'name' => 'name',
                                                        'value' => $permission->name
                                                        ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">توضیحات</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.textarea', [
                                                        'name' => 'label',
                                                        'value' => $permission->label,
                                                        ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <!--begin::Button-->
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label">ثبت تغییرات</span>
                            </button>
                            <!--end::Button-->
                        </div>
                        <!--end::Card header-->
                    </div>
                    <!--end::General options-->

                </div>
                <!--end::Main column-->

            </form>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
