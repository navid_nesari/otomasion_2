@extends('layouts.admin.admin')

@section('title',' مدیریت دسته بندی ها  ')
@section('pageTitle',' ویرایش  دسته بندی ها ')
@section('content')
    <div id="kt_content_container" class="container-xxl">
        <div class="row mt-4">
            <form class="form d-flex flex-column flex-lg-row mb-3"
                  action="{{ route('admin.categories.update', $category) }}" method="post"
                  enctype="multipart/form-data">
            @csrf
                <!--begin::Main column-->
                <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10 ms-lg-2" style="margin-left: 20px ">
                    <!--begin::General options-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>ویرایش دسته بندی</h2>
                            </div>
                            <div class="card-toolbar">
                                <a href="{{route('admin.categories.all')}}" class="btn btn-sm btn-light-success "
                                   style="margin-left: 5px">
                                    بازگشت
                                </a>

                            </div>
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">

                            <!--begin::Input group-->
                            <div class="row">
                                <div class="col">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">عنوان</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                                        'name' => 'title',
                                                        'value' => $category->title
                                                        ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="required form-label">توضیحات</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.textarea', [
                                                        'name' => 'description',
                                                        'value' => $category->description,
                                                        ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <!--begin::Button-->
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label">ثبت تغییرات</span>
                            </button>
                            <!--end::Button-->
                        </div>
                        <!--end::Card header-->
                    </div>
                    <!--end::General options-->

                </div>
                <!--end::Main column-->
                <!--begin::Aside column-->
                <div class="d-flex flex-column gap-7 gap-lg-10 w-100 w-lg-350px mb-3 me-lg-2">
                    <!--begin::Status-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <h4>وضعیت</h4>
                            </div>
                            <!--end::Card title-->

                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0">
                            <!--begin::Select2-->
                            <div class="fv-row fv-plugins-icon-container">
                                @include('admin.__components.horizontal-radiobutton',[
                                    'items' => $activityStatuses,
                                    'name' => 'status',
                                    'activeKey' => $category->status
                                ])
                            </div>
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Status-->
                    <!--begin::Thumbnail settings-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <h2>تصویر </h2>
                            </div>
                            <!--end::Card title-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body text-center pt-0">
                            <!--begin::Image input-->
                            @include('admin.__components.image-input', [
                                'name' => 'image',
                                'imageUrl' => $category->webPresent()->image
                                ])

                            <!--end::Image input-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Thumbnail settings-->
                </div>
                <!--end::Aside column-->
            </form>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
