@extends('layouts.admin.admin')

@section('title',' مدیریت پروژه ها  ')
@section('pageTitle','لیست پروژه ها')
@section('content')
    @include('admin.projects.index', ['active' => 'bord'])
    <div class="tab-content">
        <!--begin::Tab pane-->
        <div id="kt_project_targets_card_pane" class="tab-pane fade active show" role="tabpanel">
            <!--begin::Row-->
            <div class="row g-9">
                <!--begin::Col-->
                @if(count($admins) > 0)
                    @foreach($admins as $admin)
                        <div class="col-md-2 col-lg-12 col-xl-4">
                            <!--begin::Col header-->
                            <div class="mb-9">
                                <div class="d-flex flex-stack">
                                    <div class="fw-bold fs-4">
                                        {{$admin->fist_name . ' '. $admin->last_name}} :
                                        <span class="fs-6 text-gray-400 ms-2">تعداد وظیفه  ({{count($admin->duties)}})</span>
                                    </div>
                                    <!--begin::Menu-->
                                    <div>
                                        <button type="button"
                                                class="btn btn-sm btn-icon btn-color-light-dark btn-active-light-primary"
                                                data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                            <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                                            <span class="svg-icon svg-icon-2">
																		<svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24px" height="24px"
                                                                             viewBox="0 0 24 24">
																			<g stroke="none" stroke-width="1"
                                                                               fill="none" fill-rule="evenodd">
																				<rect x="5" y="5" width="5" height="5"
                                                                                      rx="1" fill="currentColor"></rect>
																				<rect x="14" y="5" width="5" height="5"
                                                                                      rx="1" fill="currentColor"
                                                                                      opacity="0.3"></rect>
																				<rect x="5" y="14" width="5" height="5"
                                                                                      rx="1" fill="currentColor"
                                                                                      opacity="0.3"></rect>
																				<rect x="14" y="14" width="5" height="5"
                                                                                      rx="1" fill="currentColor"
                                                                                      opacity="0.3"></rect>
																			</g>
																		</svg>
																	</span>
                                            <!--end::Svg Icon-->
                                        </button>
                                        <!--begin::Menu 1-->
                                        <div class="menu menu-sub menu-sub-dropdown w-250px w-md-300px" data-kt-menu="true"
                                             id="kt_menu_62fe862464449">
                                            <!--begin::Header-->
                                            <div class="px-7 py-5">
                                                <div class="fs-5 text-dark fw-bold">Filter Options</div>
                                            </div>
                                            <!--end::Header-->
                                            <!--begin::Menu separator-->
                                            <div class="separator border-gray-200"></div>
                                            <!--end::Menu separator-->
                                            <!--begin::Form-->
                                            <div class="px-7 py-5">
                                                <!--begin::Input group-->
                                                <div class="mb-10">
                                                    <!--begin::Label-->
                                                    <label class="form-label fw-semibold">Status:</label>
                                                    <!--end::Label-->
                                                    <!--begin::Input-->
                                                    <div>
                                                        <select class="form-select form-select-solid select2-hidden-accessible"
                                                                data-kt-select2="true" data-placeholder="Select option"
                                                                data-dropdown-parent="#kt_menu_62fe862464449"
                                                                data-allow-clear="true" data-select2-id="select2-data-13-k8t1"
                                                                tabindex="-1" aria-hidden="true" data-kt-initialized="1">
                                                            <option data-select2-id="select2-data-15-35ew"></option>
                                                            <option value="1">Approved</option>
                                                            <option value="2">Pending</option>
                                                            <option value="2">In Process</option>
                                                            <option value="2">Rejected</option>
                                                        </select><span
                                                            class="select2 select2-container select2-container--bootstrap5"
                                                            dir="ltr" data-select2-id="select2-data-14-e4tr"
                                                            style="width: 100%;"><span class="selection"><span
                                                                    class="select2-selection select2-selection--single form-select form-select-solid"
                                                                    role="combobox" aria-haspopup="true" aria-expanded="false"
                                                                    tabindex="0" aria-disabled="false"
                                                                    aria-labelledby="select2-n2yy-container"
                                                                    aria-controls="select2-n2yy-container"><span
                                                                        class="select2-selection__rendered"
                                                                        id="select2-n2yy-container" role="textbox"
                                                                        aria-readonly="true" title="Select option"><span
                                                                            class="select2-selection__placeholder">Select option</span></span><span
                                                                        class="select2-selection__arrow" role="presentation"><b
                                                                            role="presentation"></b></span></span></span><span
                                                                class="dropdown-wrapper" aria-hidden="true"></span></span>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                <!--end::Input group-->
                                                <!--begin::Input group-->
                                                <div class="mb-10">
                                                    <!--begin::Label-->
                                                    <label class="form-label fw-semibold">Member Type:</label>
                                                    <!--end::Label-->
                                                    <!--begin::Options-->
                                                    <div class="d-flex">
                                                        <!--begin::Options-->
                                                        <label
                                                            class="form-check form-check-sm form-check-custom form-check-solid me-5">
                                                            <input class="form-check-input" type="checkbox" value="1">
                                                            <span class="form-check-label">Author</span>
                                                        </label>
                                                        <!--end::Options-->
                                                        <!--begin::Options-->
                                                        <label
                                                            class="form-check form-check-sm form-check-custom form-check-solid">
                                                            <input class="form-check-input" type="checkbox" value="2"
                                                                   checked="checked">
                                                            <span class="form-check-label">Customer</span>
                                                        </label>
                                                        <!--end::Options-->
                                                    </div>
                                                    <!--end::Options-->
                                                </div>
                                                <!--end::Input group-->
                                                <!--begin::Input group-->
                                                <div class="mb-10">
                                                    <!--begin::Label-->
                                                    <label class="form-label fw-semibold">Notifications:</label>
                                                    <!--end::Label-->
                                                    <!--begin::Switch-->
                                                    <div
                                                        class="form-check form-switch form-switch-sm form-check-custom form-check-solid">
                                                        <input class="form-check-input" type="checkbox" value=""
                                                               name="notifications" checked="checked">
                                                        <label class="form-check-label">Enabled</label>
                                                    </div>
                                                    <!--end::Switch-->
                                                </div>
                                                <!--end::Input group-->
                                                <!--begin::Actions-->
                                                <div class="d-flex justify-content-end">
                                                    <button type="reset"
                                                            class="btn btn-sm btn-light btn-active-light-primary me-2"
                                                            data-kt-menu-dismiss="true">Reset
                                                    </button>
                                                    <button type="submit" class="btn btn-sm btn-primary"
                                                            data-kt-menu-dismiss="true">Apply
                                                    </button>
                                                </div>
                                                <!--end::Actions-->
                                            </div>
                                            <!--end::Form-->
                                        </div>
                                        <!--end::Menu 1-->
                                    </div>
                                    <!--end::Menu-->
                                </div>
                                <div class="h-3px w-100 bg-warning"></div>
                            </div>
                            <!--end::Col header-->
                            <!--begin::Card-->
                            @if(count($admin->duties) > 0)
                                @foreach($admin->duties as $duty)
                                    <div class="card mb-6 mb-xl-9">
                                        <!--begin::Card body-->
                                        <div class="card-body">
                                            <!--begin::Header-->
                                            <div class="d-flex flex-stack mb-3">
                                                <!--begin::Badge-->
                                                {!! $duty->webPresent()->status !!}
                                                <!--end::Badge-->
                                                <!--begin::Menu-->
                                                <div>
                                                    <button type="button"
                                                            class="btn btn-sm btn-icon btn-color-light-dark btn-active-light-primary"
                                                            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                                        <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                                                        <span class="svg-icon svg-icon-2">
																			<svg xmlns="http://www.w3.org/2000/svg"
                                                                                 width="24px" height="24px"
                                                                                 viewBox="0 0 24 24">
																				<g stroke="none" stroke-width="1"
                                                                                   fill="none" fill-rule="evenodd">
																					<rect x="5" y="5" width="5"
                                                                                          height="5" rx="1"
                                                                                          fill="currentColor"></rect>
																					<rect x="14" y="5" width="5"
                                                                                          height="5" rx="1"
                                                                                          fill="currentColor"
                                                                                          opacity="0.3"></rect>
																					<rect x="5" y="14" width="5"
                                                                                          height="5" rx="1"
                                                                                          fill="currentColor"
                                                                                          opacity="0.3"></rect>
																					<rect x="14" y="14" width="5"
                                                                                          height="5" rx="1"
                                                                                          fill="currentColor"
                                                                                          opacity="0.3"></rect>
																				</g>
																			</svg>
																		</span>
                                                        <!--end::Svg Icon-->
                                                    </button>
                                                    <!--begin::Menu 3-->
                                                    <div
                                                        class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-200px py-3"
                                                        data-kt-menu="true">
                                                        <!--begin::Heading-->
                                                        <div class="menu-item px-3">
                                                            <div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">
                                                                Payments
                                                            </div>
                                                        </div>
                                                        <!--end::Heading-->
                                                        <!--begin::Menu item-->
                                                        <div class="menu-item px-3">
                                                            <a href="#" class="menu-link px-3">Create Invoice</a>
                                                        </div>
                                                        <!--end::Menu item-->
                                                        <!--begin::Menu item-->
                                                        <div class="menu-item px-3">
                                                            <a href="#" class="menu-link flex-stack px-3">Create Payment
                                                                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip"
                                                                   aria-label="Specify a target name for future usage and reference"
                                                                   data-kt-initialized="1"></i></a>
                                                        </div>
                                                        <!--end::Menu item-->
                                                        <!--begin::Menu item-->
                                                        <div class="menu-item px-3">
                                                            <a href="#" class="menu-link px-3">Generate Bill</a>
                                                        </div>
                                                        <!--end::Menu item-->
                                                        <!--begin::Menu item-->
                                                        <div class="menu-item px-3" data-kt-menu-trigger="hover"
                                                             data-kt-menu-placement="right-end">
                                                            <a href="#" class="menu-link px-3">
                                                                <span class="menu-title">Subscription</span>
                                                                <span class="menu-arrow"></span>
                                                            </a>
                                                            <!--begin::Menu sub-->
                                                            <div class="menu-sub menu-sub-dropdown w-175px py-4">
                                                                <!--begin::Menu item-->
                                                                <div class="menu-item px-3">
                                                                    <a href="#" class="menu-link px-3">Plans</a>
                                                                </div>
                                                                <!--end::Menu item-->
                                                                <!--begin::Menu item-->
                                                                <div class="menu-item px-3">
                                                                    <a href="#" class="menu-link px-3">Billing</a>
                                                                </div>
                                                                <!--end::Menu item-->
                                                                <!--begin::Menu item-->
                                                                <div class="menu-item px-3">
                                                                    <a href="#" class="menu-link px-3">Statements</a>
                                                                </div>
                                                                <!--end::Menu item-->
                                                                <!--begin::Menu separator-->
                                                                <div class="separator my-2"></div>
                                                                <!--end::Menu separator-->
                                                                <!--begin::Menu item-->
                                                                <div class="menu-item px-3">
                                                                    <div class="menu-content px-3">
                                                                        <!--begin::Switch-->
                                                                        <label
                                                                            class="form-check form-switch form-check-custom form-check-solid">
                                                                            <!--begin::Input-->
                                                                            <input class="form-check-input w-30px h-20px"
                                                                                   type="checkbox" value="1" checked="checked"
                                                                                   name="notifications">
                                                                            <!--end::Input-->
                                                                            <!--end::Label-->
                                                                            <span
                                                                                class="form-check-label text-muted fs-6">Recuring</span>
                                                                            <!--end::Label-->
                                                                        </label>
                                                                        <!--end::Switch-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Menu item-->
                                                            </div>
                                                            <!--end::Menu sub-->
                                                        </div>
                                                        <!--end::Menu item-->
                                                        <!--begin::Menu item-->
                                                        <div class="menu-item px-3 my-1">
                                                            <a href="#" class="menu-link px-3">Settings</a>
                                                        </div>
                                                        <!--end::Menu item-->
                                                    </div>
                                                    <!--end::Menu 3-->
                                                </div>
                                                <!--end::Menu-->
                                            </div>
                                            <!--end::Header-->
                                            <!--begin::Title-->
                                            <div class="mb-2">
                                                <a href="#" class="fs-4 fw-bold mb-1 text-gray-900 text-hover-primary">
                                                  عنوان وظیفه :   {{$duty->title}}
                                                </a>
                                            </div>
                                            <!--end::Title-->
                                            <!--begin::Content-->
                                            <div class="fs-6 fw-semibold text-gray-600 mb-5">
                                                {!! $duty->description !!}
                                            </div>
                                            <!--end::Content-->
                                            <!--begin::Footer-->
                                            <div class="d-flex flex-stack flex-wrapr">
                                                <!--begin::Users-->
                                                <div class="symbol-group symbol-hover my-1">
                                                    @foreach($duty->admins as $item)
                                                        <div class="symbol symbol-35px symbol-circle" data-bs-toggle="tooltip"
                                                             aria-label="Melody Macy" data-kt-initialized="1">
                                                            <img alt="Pic" src="{{ $item->webPresent()->image }}">
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <!--end::Users-->
                                                <!--begin::Stats-->
                                                <div class="d-flex my-1">
                                                    <!--begin::Stat-->
                                                    <div class="border border-dashed border-gray-300 rounded py-2 px-3">

                                                        <span class="ms-1 fs-7 fw-bold text-gray-600">{{ jdate($duty->start_date)->format('Y-m-d') }}</span>
                                                    </div>
                                                    <!--end::Stat-->
                                                    <!--begin::Stat-->
                                                    <div class="border border-dashed border-gray-300 rounded py-2 px-3 ms-3">
                                                        <span class="ms-1 fs-7 fw-bold text-gray-600">
                                                            {{ jdate($duty->end_date)->format('Y-m-d') }}
                                                        </span>
                                                    </div>
                                                    <!--end::Stat-->
                                                </div>
                                                <!--end::Stats-->
                                            </div>
                                            <!--end::Footer-->
                                        </div>
                                        <!--end::Card body-->
                                    </div>
                                @endforeach
                            @endif
                        </div>

                    @endforeach
                @endif
            </div>
            <!--end::Row-->
        </div>
        <!--end::Tab pane-->
        <!--begin::Tab pane-->
        <div id="kt_project_targets_table_pane" class="tab-pane fade" role="tabpanel">
            <div class="card card-flush">
                <div class="card-body pt-3">
                    <!--begin::Table-->
                    <div id="kt_profile_overview_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                        <div class="table-responsive">
                            <table id="kt_profile_overview_table"
                                   class="table table-row-bordered table-row-dashed gy-4 align-middle fw-bold dataTable no-footer">
                                <!--begin::Table head-->
                                <thead class="fs-7 text-gray-400 text-uppercase">
                                <tr>
                                    <th class="min-w-250px sorting" tabindex="0"
                                        aria-controls="kt_profile_overview_table" rowspan="1" colspan="1"
                                        style="width: 0px;" aria-label="Target: activate to sort column ascending">
                                        Target
                                    </th>
                                    <th class="min-w-90px sorting" tabindex="0"
                                        aria-controls="kt_profile_overview_table" rowspan="1" colspan="1"
                                        style="width: 0px;" aria-label="Section: activate to sort column ascending">
                                        Section
                                    </th>
                                    <th class="min-w-150px sorting" tabindex="0"
                                        aria-controls="kt_profile_overview_table" rowspan="1" colspan="1"
                                        style="width: 0px;" aria-label="Due Date: activate to sort column ascending">Due
                                        Date
                                    </th>
                                    <th class="min-w-90px sorting" tabindex="0"
                                        aria-controls="kt_profile_overview_table" rowspan="1" colspan="1"
                                        style="width: 0px;" aria-label="Members: activate to sort column ascending">
                                        Members
                                    </th>
                                    <th class="min-w-90px sorting" tabindex="0"
                                        aria-controls="kt_profile_overview_table" rowspan="1" colspan="1"
                                        style="width: 0px;" aria-label="Status: activate to sort column ascending">
                                        Status
                                    </th>
                                    <th class="min-w-50px sorting" tabindex="0"
                                        aria-controls="kt_profile_overview_table" rowspan="1" colspan="1"
                                        style="width: 0px;" aria-label=": activate to sort column ascending"></th>
                                </tr>
                                </thead>
                                <!--end::Table head-->
                                <!--begin::Table body-->
                                <tbody class="fs-6">
                                <!--begin::Table row-->

                                <!--end::Table row-->
                                <!--begin::Table row-->

                                <!--end::Table row-->
                                <!--begin::Table row-->

                                <!--end::Table row-->
                                <!--begin::Table row-->

                                <!--end::Table row-->
                                <!--begin::Table row-->

                                <!--end::Table row-->
                                <!--begin::Table row-->

                                <!--end::Table row-->
                                <!--begin::Table row-->

                                <!--end::Table row-->
                                <!--begin::Table row-->

                                <!--end::Table row-->
                                <tr class="odd">
                                    <td class="fw-bold">
                                        <a href="#" class="text-gray-900 text-hover-primary">Meeting with customer</a>
                                    </td>
                                    <td data-order="Invalid date">
                                        <span class="badge badge-light fw-semibold me-auto">UI Design</span>
                                    </td>
                                    <td>Aug 11, 2020</td>
                                    <td>
                                        <!--begin::Members-->
                                        <div class="symbol-group symbol-hover fs-8">
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 aria-label="Melody Macy" data-kt-initialized="1">
                                                <img alt="Pic" src="assets/media/avatars/300-2.jpg">
                                            </div>
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 aria-label="John Mixin" data-kt-initialized="1">
                                                <img alt="Pic" src="assets/media/avatars/300-14.jpg">
                                            </div>
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 data-kt-initialized="1">
                                                <span
                                                    class="symbol-label bg-primary text-inverse-primary fw-bold">S</span>
                                            </div>
                                        </div>
                                        <!--end::Members-->
                                    </td>
                                    <td>
                                        <span class="badge badge-light-primary fw-bold me-auto">In Progress</span>
                                    </td>
                                    <td class="text-end">
                                        <a href="#" class="btn btn-bg-light btn-active-color-primary btn-sm">View</a>
                                    </td>
                                </tr>
                                <tr class="even">
                                    <td class="fw-bold">
                                        <a href="#" class="text-gray-900 text-hover-primary">User Module Testing</a>
                                    </td>
                                    <td data-order="Invalid date">
                                        <span class="badge badge-light fw-semibold me-auto">Phase 2.6 QA</span>
                                    </td>
                                    <td>Dec 22, 2020</td>
                                    <td>
                                        <!--begin::Members-->
                                        <div class="symbol-group symbol-hover fs-8">
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 data-kt-initialized="1">
                                                <span
                                                    class="symbol-label bg-warning text-inverse-warning fw-bold">A</span>
                                            </div>
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 data-kt-initialized="1">
                                                <span
                                                    class="symbol-label bg-success text-inverse-success fw-bold">R</span>
                                            </div>
                                        </div>
                                        <!--end::Members-->
                                    </td>
                                    <td>
                                        <span class="badge badge-light-success fw-bold me-auto">Completed</span>
                                    </td>
                                    <td class="text-end">
                                        <a href="#" class="btn btn-bg-light btn-active-color-primary btn-sm">View</a>
                                    </td>
                                </tr>
                                <tr class="odd">
                                    <td class="fw-bold">
                                        <a href="#" class="text-gray-900 text-hover-primary">Sales report page</a>
                                    </td>
                                    <td data-order="Invalid date">
                                        <span class="badge badge-light fw-semibold me-auto">QA</span>
                                    </td>
                                    <td>Feb 15, 2020</td>
                                    <td>
                                        <!--begin::Members-->
                                        <div class="symbol-group symbol-hover fs-8">
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 aria-label="Melody Macy" data-kt-initialized="1">
                                                <img alt="Pic" src="assets/media/avatars/300-2.jpg">
                                            </div>
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 aria-label="Kristen Goodwin" data-kt-initialized="1">
                                                <img alt="Pic" src="assets/media/avatars/300-9.jpg">
                                            </div>
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 data-kt-initialized="1">
                                                <span class="symbol-label bg-info text-inverse-info fw-bold">M</span>
                                            </div>
                                        </div>
                                        <!--end::Members-->
                                    </td>
                                    <td>
                                        <span class="badge badge-light fw-bold me-auto">Yet to start</span>
                                    </td>
                                    <td class="text-end">
                                        <a href="#" class="btn btn-bg-light btn-active-color-primary btn-sm">View</a>
                                    </td>
                                </tr>
                                <tr class="even">
                                    <td class="fw-bold">
                                        <a href="#" class="text-gray-900 text-hover-primary">Meeting with customer</a>
                                    </td>
                                    <td data-order="Invalid date">
                                        <span class="badge badge-light fw-semibold me-auto">Prototype</span>
                                    </td>
                                    <td>Oct 9, 2020</td>
                                    <td>
                                        <!--begin::Members-->
                                        <div class="symbol-group symbol-hover fs-8">
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 data-kt-initialized="1">
                                                <span
                                                    class="symbol-label bg-success text-inverse-success fw-bold">R</span>
                                            </div>
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 aria-label="Brian Cox" data-kt-initialized="1">
                                                <img alt="Pic" src="assets/media/avatars/300-5.jpg">
                                            </div>
                                        </div>
                                        <!--end::Members-->
                                    </td>
                                    <td>
                                        <span class="badge badge-light-success fw-bold me-auto">Completed</span>
                                    </td>
                                    <td class="text-end">
                                        <a href="#" class="btn btn-bg-light btn-active-color-primary btn-sm">View</a>
                                    </td>
                                </tr>
                                <tr class="odd">
                                    <td class="fw-bold">
                                        <a href="#" class="text-gray-900 text-hover-primary">Design main Dashboard</a>
                                    </td>
                                    <td data-order="Invalid date">
                                        <span class="badge badge-light fw-semibold me-auto">UI Design</span>
                                    </td>
                                    <td>Dec 16, 2020</td>
                                    <td>
                                        <!--begin::Members-->
                                        <div class="symbol-group symbol-hover fs-8">
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 aria-label="Melody Macy" data-kt-initialized="1">
                                                <img alt="Pic" src="assets/media/avatars/300-2.jpg">
                                            </div>
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 aria-label="Emma Smith" data-kt-initialized="1">
                                                <img alt="Pic" src="assets/media/avatars/300-6.jpg">
                                            </div>
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 aria-label="Lucy Matthews" data-kt-initialized="1">
                                                <img alt="Pic" src="assets/media/avatars/300-21.jpg">
                                            </div>
                                        </div>
                                        <!--end::Members-->
                                    </td>
                                    <td>
                                        <span class="badge badge-light-success fw-bold me-auto">Completed</span>
                                    </td>
                                    <td class="text-end">
                                        <a href="#" class="btn btn-bg-light btn-active-color-primary btn-sm">View</a>
                                    </td>
                                </tr>
                                <tr class="even">
                                    <td class="fw-bold">
                                        <a href="#" class="text-gray-900 text-hover-primary">User Module Testing</a>
                                    </td>
                                    <td data-order="Invalid date">
                                        <span class="badge badge-light fw-semibold me-auto">Development</span>
                                    </td>
                                    <td>Jan 19, 2020</td>
                                    <td>
                                        <!--begin::Members-->
                                        <div class="symbol-group symbol-hover fs-8">
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 aria-label="Francis Mitcham" data-kt-initialized="1">
                                                <img alt="Pic" src="assets/media/avatars/300-20.jpg">
                                            </div>
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 aria-label="Deanna Taylor" data-kt-initialized="1">
                                                <img alt="Pic" src="assets/media/avatars/300-23.jpg">
                                            </div>
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 data-kt-initialized="1">
                                                <span class="symbol-label bg-info text-inverse-info fw-bold">M</span>
                                            </div>
                                        </div>
                                        <!--end::Members-->
                                    </td>
                                    <td>
                                        <span class="badge badge-light-primary fw-bold me-auto">In Progress</span>
                                    </td>
                                    <td class="text-end">
                                        <a href="#" class="btn btn-bg-light btn-active-color-primary btn-sm">View</a>
                                    </td>
                                </tr>
                                <tr class="odd">
                                    <td class="fw-bold">
                                        <a href="#" class="text-gray-900 text-hover-primary">To check User
                                            Management</a>
                                    </td>
                                    <td data-order="Invalid date">
                                        <span class="badge badge-light fw-semibold me-auto">Pahse 3.2</span>
                                    </td>
                                    <td>Jan 20, 2020</td>
                                    <td>
                                        <!--begin::Members-->
                                        <div class="symbol-group symbol-hover fs-8">
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 aria-label="Lucy Matthews" data-kt-initialized="1">
                                                <img alt="Pic" src="assets/media/avatars/300-21.jpg">
                                            </div>
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 aria-label="Kristen Goodwin" data-kt-initialized="1">
                                                <img alt="Pic" src="assets/media/avatars/300-9.jpg">
                                            </div>
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 aria-label="Michelle Swanston" data-kt-initialized="1">
                                                <img alt="Pic" src="assets/media/avatars/300-7.jpg">
                                            </div>
                                        </div>
                                        <!--end::Members-->
                                    </td>
                                    <td>
                                        <span class="badge badge-light fw-bold me-auto">Yet to start</span>
                                    </td>
                                    <td class="text-end">
                                        <a href="#" class="btn btn-bg-light btn-active-color-primary btn-sm">View</a>
                                    </td>
                                </tr>
                                <tr class="even">
                                    <td class="fw-bold">
                                        <a href="#" class="text-gray-900 text-hover-primary">Create Roles Module</a>
                                    </td>
                                    <td data-order="Invalid date">
                                        <span class="badge badge-light fw-semibold me-auto">Branding</span>
                                    </td>
                                    <td>Jul 21, 2020</td>
                                    <td>
                                        <!--begin::Members-->
                                        <div class="symbol-group symbol-hover fs-8">
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 aria-label="Michelle Swanston" data-kt-initialized="1">
                                                <img alt="Pic" src="assets/media/avatars/300-7.jpg">
                                            </div>
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 data-kt-initialized="1">
                                                <span
                                                    class="symbol-label bg-success text-inverse-success fw-bold">R</span>
                                            </div>
                                            <div class="symbol symbol-25px symbol-circle" data-bs-toggle="tooltip"
                                                 data-kt-initialized="1">
                                                <span
                                                    class="symbol-label bg-warning text-inverse-warning fw-bold">A</span>
                                            </div>
                                        </div>
                                        <!--end::Members-->
                                    </td>
                                    <td>
                                        <span class="badge badge-light fw-bold me-auto">Yet to start</span>
                                    </td>
                                    <td class="text-end">
                                        <a href="#" class="btn btn-bg-light btn-active-color-primary btn-sm">View</a>
                                    </td>
                                </tr>
                                </tbody>
                                <!--end::Table body-->
                            </table>
                        </div>
                        <div class="row">
                            <div
                                class="col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start"></div>
                            <div
                                class="col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end"></div>
                        </div>
                    </div>
                    <!--end::Table-->
                </div>
            </div>
        </div>
        <!--end::Tab pane-->
    </div>

@endsection
@section('scripts')
    @include('admin.message.alert')

@endsection
