@extends('layouts.admin.admin')

@section('style')
    <link rel="stylesheet" href="{{asset('admin-assets/css/persian-datepicker.min')}}"/>
@endsection
@section('content')
    @include('admin.projects.index')

        <form id="kt_ecommerce_add_category_form"
            class="form d-flex flex-column flex-lg-row fv-plugins-bootstrap5 fv-plugins-framework"
            data-kt-redirect="" action="{{route('admin.projects.update', $project)}}" method="post" enctype="multipart/form-data">
            @csrf
            <!--begin::Main column-->
            <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10" style="margin-left: 20px ">
                <!--begin::General options-->
                <div class="card card-flush py-4">
                    <!--begin::Card header-->
                    <div class="card-header">
                        <div class="card-title">
                            <h2>ویرایش پروژه </h2>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{route('admin.project-duties.all', $project)}}" class="btn btn-sm btn-light-success "
                               style="margin-left: 5px">
                                برگشت
                            </a>

                        </div>
                    </div>
                    <!--end::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body pt-0 mt-2">
                        <!--begin::Input group-->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-10 fv-row fv-plugins-icon-container">
                                    <!--begin::Label-->
                                    <label class="required form-label">عنوان</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                        'name' => 'title',
                                        'value'=>$project->title
                                        ])
                                    <!--end::Input-->
                                    <div class="fv-plugins-message-container invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-10 fv-row fv-plugins-icon-container">
                                    <!--begin::Label-->
                                    <label class="required form-label">مشارکت کننده ها </label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    @include('admin.__components.select-2-ajax', [
                                        'name' => 'admins',
                                        'isMultiple' => true,
                                        'selectedItems' => $selectedAdminItems,
                                        'url' =>route('admin.projects.ajax-search'),
                                    ])
                                    <!--end::Input-->
                                    <div class="fv-plugins-message-container invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-10 fv-row fv-plugins-icon-container">
                                    <!--begin::Label-->
                                    <label class="required form-label">تاریخ شروع پروژه</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                      @include('admin.__components.datepicker', [
                                        'name' => 'start_date',
                                        'value'=> $project->start_date
                                        ])
                                     <!--end::Input-->

                                    <div class="fv-plugins-message-container invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-10 fv-row fv-plugins-icon-container">
                                    <!--begin::Label-->
                                    <label class="required form-label">تاریخ پایان پروژه</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                @include('admin.__components.datepicker', [
                                    'name' => 'end_date',
                                    'value' => $project->end_date
                                    ])
                                <!--end::Input-->
                                    <div class="fv-plugins-message-container invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-10 fv-row fv-plugins-icon-container">
                                    <!--begin::Label-->
                                    <label class="required form-label">توضیحات</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                   @include('admin.__components.textarea', [
                                    'name' => 'description',
                                    'value' => $project->description
                                    ])
                                   <!--end::Input-->
                                   <div class="fv-plugins-message-container invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <button type="submit" id="kt_ecommerce_add_category_submit" class="btn btn-primary">
                            <span class="indicator-label">ویرایش اطلاعات  </span>

                        </button>
                        <!--end::Input group-->
                    </div>
                    <!--end::Card header-->
                </div>
                <!--end::General options-->
                <!--begin::Automation-->
                <!--end::Automation-->
            </div>
            <!--end::Main column-->
            <br/>
            <!--begin::Aside column-->
            <div class="d-flex flex-column gap-7 gap-lg-5 w-100 w-lg-350px mb-7 me-lg-1">
                <!--begin::Status-->
                <div class="card card-flush py-4">
                    <!--begin::Card header-->
                    <div class="card-header">
                        <!--begin::Card title-->
                        <div class="card-title">
                            <h2>وضعیت</h2>
                        </div>
                        <!--end::Card title-->

                    </div>
                    <!--end::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body pt-0">
                        <!--begin::Select2-->
                        @include('admin.__components.vertical-radiobutton', [
                          'name' => 'status',
                          'activeKey' => $project->status,
                          'items' => $activityStatuses
                          ])
                        <div class="d-none mt-10">
                            <label for="kt_ecommerce_add_category_status_datepicker" class="form-label">Select
                                publishing date and time</label>
                            <input class="form-control flatpickr-input" id="kt_ecommerce_add_category_status_datepicker"
                                   placeholder="Pick date &amp; time" type="text" readonly="readonly">
                        </div>
                        <!--end::Datepicker-->
                    </div>
                    <!--end::Card body-->
                </div>
                <!--end::Status-->
                <!--begin::Thumbnail settings-->
                <div class="card card-flush py-4">
                    <!--begin::Card header-->
                    <div class="card-header">
                        <!--begin::Card title-->
                        <div class="card-title">
                            <h2>تصویر</h2>
                        </div>
                        <!--end::Card title-->
                    </div>
                    <!--end::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body text-center pt-0">
                        <!--begin::Image input-->
                          @include('admin.__components.image-input', [
                            'name' => 'image',
                            'imageUrl'=>$project->webPresent()->image
                            ])

                        <!--end::Image input-->
                    </div>
                    <!--end::Card body-->
                </div>

                <!--end::Thumbnail settings-->
            </div>

            <!--end::Aside column-->
            <div></div>
        </form>

@endsection

@section('scripts')
    <script src="{{asset('admin-assets/js/persian-datepicker.min.js')}}"></script>
    <script src="{{asset('admin-assets/js/persian-date.min.js')}}"></script>
@endsection
