@extends('layouts.admin.admin')

@section('content')
    @include('admin.projects.index', ['active' => 'duties'])

    <div class="d-flex flex-wrap flex-stack pt-2 pb-8">
        <!--begin::Heading-->
        <h3 class="fw-bold my-2">لیست وظایف</h3>
        <!--end::Heading-->
        <!--begin::Controls-->
        <div class="d-flex flex-wrap my-1" data-select2-id="select2-data-134-47ch">

            <!--begin::Wrapper-->
            <div class="my-0">
                <!--begin::Select-->
                <a href="{{route('admin.project-duties.all', $project)}}" class="btn btn-sm btn-light-primary me-2 ">
                    نمایش به صورت دیگر
                </a>
                <a href="{{route('admin.project-duties.create', $project)}}" class="btn btn-sm btn-light-success">
                    ثبت وظیفه جدید
                 </a>
                <!--end::Select-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Controls-->
    </div>
    <div class="row g-9">
        @if($duties->count() > 0)
            @foreach($duties as $duty)
                <div class="col-md-4 col-lg-12 col-xl-4">
                    <!--begin::Card-->
                    <div class="card mb-6 mb-xl-9">
                        <!--begin::Card body-->
                        <div class="card-body">
                            <!--begin::Header-->
                            <div class="d-flex flex-stack mb-3">
                                <!--begin::Badge-->
                                {!! $duty->webPresent()->status !!}
                                <!--end::Badge-->
                            </div>
                            <!--end::Header-->
                            <!--begin::Title-->
                            <div class="mb-2">
                                <a href="#" class="fs-4 fw-bold mb-1 text-gray-900 text-hover-primary">{{$duty->title}}</a>
                            </div>
                            <!--end::Title-->
                            <!--begin::Content-->
                            <div class="fs-6 fw-semibold text-gray-600 mb-5">{{ Str::limit($duty->description, 50) }}</div>
                            <!--end::Content-->
                            <!--begin::Footer-->
                            <div class="">

                                <!--begin::Stats-->
                                <div class="d-flex flex-wrap">
                                    <!--begin::Stat-->
                                    <div class="border border-gray-300 border-dashed rounded w-100px py-3 px-4 me-6 mb-3">
                                        <!--begin::Number-->
                                        <div class="d-flex align-items-center">
                                            <div class="fs-4 fw-bold">{{ jdate($duty->start_date)->format('Y-m-d') }}</div>
                                        </div>
                                        <!--end::Number-->
                                        <!--begin::Label-->
                                        <div class="fw-semibold fs-6 text-gray-400">تاریخ شروع</div>
                                        <!--end::Label-->
                                    </div>
                                    <!--end::Stat-->
                                    <!--begin::Stat-->
                                    <div class="border border-gray-300 border-dashed rounded w-100px py-3 px-4 me-6 mb-3">
                                        <!--begin::Number-->
                                        <div class="d-flex align-items-center">
                                            <div class="fs-4 fw-bold">{{ jdate($duty->end_date)->format('Y-m-d') }}</div>
                                        </div>
                                        <!--end::Number-->
                                        <!--begin::Label-->
                                        <div class="fw-semibold fs-6 text-gray-400">تاریخ پایان</div>
                                        <!--end::Label-->
                                    </div>
                                    <!--end::Stat-->

                                </div>
                                <!--end::Stats-->
                                <!--begin::Users-->
                                <div class="symbol-group symbol-hover my-1">
                                    @foreach ( $duty->admins as $admin)
                                        <div class="symbol symbol-35px symbol-circle" data-bs-toggle="tooltip" title="{{$admin->webPresent()->fullName}}">
                                            <img alt="Pic" src="{{ $admin->webPresent()->image }}" />
                                        </div>
                                    @endforeach
                                </div>
                                <!--end::Users-->
                            </div>
                            <!--end::Footer-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Card-->
                </div>
            @endforeach
        @else
            <div colspan="6" style="text-align:center;color:red">
                <a class="btn btn-danger mt-8" href="">
                    اطلاعات موجود نیست<i class="icon-warning2 mr-3 icon-1x"></i>
                </a>
            </div>
        @endif
    </div>
@endsection
