@extends('layouts.admin.admin')

@section('content')
    @include('admin.projects.index', ['active' => 'duties'])

    <form id="kt_ecommerce_add_category_form"
            class="form d-flex flex-column flex-lg-row fv-plugins-bootstrap5 fv-plugins-framework"
            data-kt-redirect="" action="{{route('admin.project-duties.store', $project)}}" method="post" enctype="multipart/form-data">
        @csrf
        <!--begin::Main column-->
        <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10" style="margin-left: 20px ">
            <!--begin::General options-->
            <div class="card card-flush py-4">
                <!--begin::Card header-->
                <div class="card-header">
                    <div class="card-title">
                        <h2>ثبت وظیفه جدید</h2>
                    </div>
                    <div class="card-toolbar">
                        <a href="{{route('admin.project-duties.all', $project)}}" class="btn btn-sm btn-light-success "
                            style="margin-left: 5px">
                            برگشت
                        </a>

                    </div>
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0 mt-2">
                    <!--begin::Input group-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-10 fv-row fv-plugins-icon-container">
                                <!--begin::Label-->
                                <label class="required form-label">عنوان</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                    @include('admin.__components.input-text', [ 'name' => 'title'])
                                    <!--end::Input-->
                                <div class="fv-plugins-message-container invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-10 fv-row fv-plugins-icon-container">
                                <!--begin::Label-->
                                <label class="required form-label">وزن</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                @include('admin.__components.input-text', [ 'name' => 'weight', 'type' => 'number'])
                                <!--end::Input-->

                                <div class="fv-plugins-message-container invalid-feedback"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-10 fv-row fv-plugins-icon-container">
                                <!--begin::Label-->
                                <label class="required form-label">تاریخ شروع</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                    @include('admin.__components.datepicker', [ 'name' => 'start_date'])
                                    <!--end::Input-->

                                <div class="fv-plugins-message-container invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-10 fv-row fv-plugins-icon-container">
                                <!--begin::Label-->
                                <label class="required form-label">تاریخ پایان</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                            @include('admin.__components.datepicker', [ 'name' => 'end_date'])
                            <!--end::Input-->
                                <div class="fv-plugins-message-container invalid-feedback"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-10 fv-row fv-plugins-icon-container">
                                <!--begin::Label-->
                                <label class="required form-label">توضیحات</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                @include('admin.__components.textarea', [ 'name' => 'description'])
                                <!--end::Input-->
                                <div class="fv-plugins-message-container invalid-feedback"></div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <button type="submit" id="kt_ecommerce_add_category_submit" class="btn btn-primary">
                        <span class="indicator-label">ثبت اطلاعات  </span>
                    </button>
                    <!--end::Input group-->
                </div>
                <!--end::Card header-->
            </div>
            <!--end::General options-->
        </div>
        <!--end::Main column-->
        <br/>
        <!--begin::Aside column-->
        <div class="d-flex flex-column gap-7 gap-lg-5 w-100 w-lg-350px mb-7 me-lg-1">
            <!--begin::checkbox-->
            <div class="card card-flush py-4">
                <!--begin::Card header-->
                <div class="card-header">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <h2>مشارکت کنندگان پروژه</h2>
                    </div>
                    <!--end::Card title-->
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                    <!--begin::Checkboxes-->
                    <div class="mt-5">
                        @foreach($projectAdmins as $projectAdmin)
                            <div class="d-flex align-items-center">
                                <!--begin::Checkbox-->
                                <label class="form-check form-check-custom form-check-solid me-10 mb-5">
                                    <input class="form-check-input h-20px w-20px"
                                        type="checkbox" name="admins[]" value="{{$projectAdmin['id']}}"
                                        @if(isset($projectAdmin['isActive']) && $projectAdmin['isActive'])  checked="checked" @endif
                                        />

                                    <!--begin::Wrapper-->
                                    <div class="me-5 ms-5 position-relative">
                                        <!--begin::Avatar-->
                                        <div class="symbol symbol-35px symbol-circle">
                                            <img alt="Pic" src="{{ asset($projectAdmin['image']) }}">
                                        </div>
                                        <!--end::Avatar-->
                                        <!--begin::Online-->
                                        <div class="bg-success position-absolute h-8px w-8px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
                                        <!--end::Online-->
                                    </div>
                                    <!--end::Wrapper-->
                                    <!--begin::Info-->
                                    <div class="d-flex flex-column justify-content-center">
                                        <span class="form-check-label fw-bold">{{$projectAdmin['title']}}</span>
                                        <div class="fw-semibold text-gray-400">{{$projectAdmin['email']}}</div>
                                    </div>
                                    <!--end::Info-->

                                </label>
                                <!--end::Checkbox-->
                            </div>
                        @endforeach
                    </div>
                    <!--end::Checkboxes-->
                    @error('admins')
                    <p class="text-danger">{{$message}}</p>
                    @enderror

                    <div class="d-flex align-items-center">

                    </div>
                    <!--end::Datepicker-->
                </div>
                <!--end::Card body-->
            </div>
            <!--end::checkbox-->
            <!--begin::Status-->
            <div class="card card-flush py-4">
                <!--begin::Card header-->
                <div class="card-header">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <h2>وضعیت</h2>
                    </div>
                    <!--end::Card title-->
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                    <!--begin::Select2-->
                    @include('admin.__components.vertical-radiobutton', [
                        'activeKey' => $activityStatuses[0]['id'] ,
                        'name' => 'status',
                        'items' => $activityStatuses
                        ])
                    <div class="d-none mt-10">
                        <label for="kt_ecommerce_add_category_status_datepicker" class="form-label">Select
                            publishing date and time</label>
                        <input class="form-control flatpickr-input" id="kt_ecommerce_add_category_status_datepicker"
                                placeholder="Pick date &amp; time" type="text" readonly="readonly">
                    </div>
                    <!--end::Datepicker-->
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Status-->
           
        </div>

        <!--end::Aside column-->
        <div></div>
    </form>
@endsection

