@extends('layouts.admin.admin')

@section('content')
    @include('admin.projects.index', ['active' => 'duties'])

    <div class="card" data-select2-id="select2-data-131-rhmf">
        <div class="card-header border-0 pt-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label fw-bolder fs-3 mb-1"> لیست وظایف </span>
            </h3>
            <div class="card-toolbar">
                <a href="{{route('admin.project-duties.second-all', $project)}}" class="btn btn-sm btn-light-primary me-2">
                نمایش به صورت دیگر
                </a>
                <a href="{{route('admin.project-duties.create', $project)}}" class="btn btn-sm btn-light-success">
                   ثبت وظیفه جدید
                </a>
            </div>
        </div>
        <!--begin::Card body-->
        <div class="card-body pt-0">

            <!--begin::Table-->
            <div id="kt_customers_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <div class="table-responsive">
                    <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
                           id="kt_customers_table">
                        <!--begin::Table head-->
                        <thead>
                        <!--begin::Table row-->
                        <tr class="min-w-125px sorting">
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="Customer Name: activate to sort column ascending"> عنوان
                            </th>
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="IP Address: activate to sort column ascending"> تاریخ شروع
                            </th>
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="Customer Name: activate to sort column ascending"> تاریخ پایان
                            </th>
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="Customer Name: activate to sort column ascending">مشارکت کننده گان
                            </th>
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="Customer Name: activate to sort column ascending"> وضعیت
                            </th>
                            <th class="min-w-125px sorting" rowspan="1" colspan="1"
                                style="width: 162.9px;" aria-label="Actions">تنظیمات
                            </th>
                        </tr>
                        <!--end::Table row-->
                        </thead>
                        <!--end::Table head-->
                        <!--begin::Table body-->
                        <tbody class="fw-bold text-gray-600">
                        @if($duties->count() > 0)
                            @foreach($duties as $duty)
                                <tr class="odd">
                                    <td>
                                        <a href="#" class="text-gray-600 text-hover-primary mb-1">
                                            {{$duty->title}}
                                        </a>
                                    </td>
                                    <td>{{ jdate($duty->start_date)->format('Y-m-d') }}</td>
                                    <td>{{ jdate($duty->end_date)->format('Y-m-d') }}</td>
                                    <td>
                                        <!--begin::Users-->
                                        <div class="symbol-group symbol-hover mb-3">
                                            <!--begin::User-->
                                            @foreach ( $duty->admins as $admin)
                                                @if (is_null($admin->image) || $admin->image == "")
                                                    <!--begin::User-->
                                                    <div class="symbol symbol-35px symbol-circle" data-bs-toggle="tooltip" title="{{$admin->webPresent()->fullName}}">
                                                        <img alt="Pic" src="{{ $admin->webPresent()->image }}">
                                                    </div>
                                                @else
                                                {{-- aria-label="{{$admin->webPresent()->fullName}}" data-bs-original-title="{{$admin->webPresent()->fullName}}" --}}
                                                    <!--begin::User-->
                                                    <div class="symbol symbol-35px symbol-circle" data-bs-toggle="tooltip" title="{{$admin->webPresent()->fullName}}">
                                                        <img alt="Pic" src="{{ $admin->webPresent()->image }}">
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                        <!--end::Users-->
                                    </td>
                                    <td>{!! $duty->webPresent()->status !!}</td>
                                    <!--begin::Action=-->
                                    <td class="">
                                        <a href="{{ route('admin.project-duties.edit', ['project' => $project, 'duty' => $duty]) }}"
                                           class="btn btn-icon btn-light-info btn-sm me-1">
                                             <span class="svg-icon svg-icon-3">
                                                <i class="fa fa-layer-group"></i>
                                            </span>
                                        </a>
                                        <a data-id={{ $duty->id }} href="javascript"
                                           class="btn btn-icon btn-light-danger btn-sm me-1 delete-confirm"
                                           data-kt-customer-table-filter="delete_row">
                                              <span class="svg-icon svg-icon-3">
                                                <i class="fa fa-trash"></i>
                                            </span>
                                        </a>
                                    </td>
                                    <!--end::Action=-->
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" style="text-align:center;color:red">
                                    <a class="btn btn-danger" href="">
                                       اطلاعات موجود نیست<i class="icon-warning2 mr-3 icon-1x"></i>
                                    </a>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                        <!--end::Table body-->
                    </table>
                </div>
                <div class="row">
                    <div
                        class="col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end">

                    </div>
                </div>
            </div>
            <!--end::Table-->
            {{-- {{$duties->links('vendor.pagination.bootstrap-4') }} --}}
        </div>
        <!--end::Card body-->
    </div>
@endsection
@section('scripts')
    @include('admin.message.alert')
    <script>
        $('#remove-empty-values').submit(function () {
            $(this).find(':input').filter(function () {
                return !this.value;
            }).attr('disabled', 'disabled');
            return true;
        });
    </script>
    <script>

        $('.delete-confirm').on('click', function (event) {
            event.preventDefault();
            let admin = $(this).data("id");
            let url = "{{ route('admin.project-duties.logicalDeletion',[":admin", 'project' => $project]) }}";
            url = url.replace(":admin",admin);

            Swal.fire({
                title: 'حذف  !',
                text: "آیا مطمئن هستید ؟",
                icon: "error",
                showCancelButton: true,
                confirmButtonColor: 'red',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'بله,حذف کن',
                cancelButtonText: 'خیر',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    window.location.href = url;
                }
            });
        });
    </script>
@endsection
