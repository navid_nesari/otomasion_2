@extends('layouts.admin.admin')

@section('title',' مدیریت پروژه ها  ')
@section('pageTitle','لیست پروژه ها')
@section('style')
    <link rel="stylesheet" href="{{asset('admin-assets/css/persian-datepicker.min.css')}}"/>
@endsection
@section('content')
    <a href="{{route('admin.projects.second-all')}}" class="btn btn-sm btn-light-primary me-2">
        نمایش به صورت دیگر
    </a>
    <a href="{{route('admin.projects.create')}}" class="btn btn-sm btn-light-success me-2">
        ثبت پروژه جدید
    </a>
    <div class="d-flex flex-end mb-5">

        <button type="button" class="btn btn-primary px-4 py-2" data-bs-toggle="collapse"
                href="#student_filters" role="button"
                aria-expanded="false" aria-controls="collapseExample" style="margin-left: 5px!important;">
            فیلتر
            <i class="fa fa-filter p-0 m-0"></i>
        </button>
    </div>

    <div class="card mb-5 pb-3 mb-xl-8 collapse @if(request()->has('title')||request()->has('status')||request()->has('start_date')||request()->has('end_date') ) show @else @endif" id="student_filters">
        <form action="" class="form remove-empty-values" method="get" id="remove-empty-values">
        <!--begin::Header-->
        <div class="card-header border-0 pt-5">
            <h3 class="card-title align-items-start flex-column">
                    <span class="card-label fw-bolder fs-3 mb-1">
                        <i class="fa fa-search text-white pl-1"></i>
                        جستجوی پیشرفته
                    </span>
            </h3>
        </div>
        <div class="card-body py-3">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="form-group">
                        <label class="form-label fs-6 fw-bolder text-dark">جستجو بر اساس عنوان
                        </label>
                        <input class="form-control form-control-lg form-control-solid" name="title"
                               placeholder=""
                               value="{{ request()->has('title') ? request()->get('title') : null }}"/>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="form-group">
                        <label class="form-label fs-6 fw-bolder text-dark">جستجو بر اساس وضعیت</label>
                        <!--begin::Input-->
                        <select class="form-control" name="status">
                            <option selected="true" disabled="disabled">انتخاب کنید</option>
                            @foreach($activityStatuses as $activityStatuse)
                                <option value="{{ $activityStatuse['id'] }}">{{ $activityStatuse['title'] }}</option>
                            @endforeach
                        </select>
                        <!--end::Input-->
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="form-group">
                        <label class="form-label fs-6 fw-bolder text-dark">جستجو بر اساس تاریخ شروع
                        </label>
                        <!--begin::Input-->
                        {{-- @include('admin.__components.datepicker', ['name' => "start_date"]) --}}
                        <input type="text" class="form-control" id="start_date" name="start_date"
                            placeholder="{{ isset($placeholder) ? $placeholder : "تاریخ" }}"
                            @if(isset($value)) value="{{ old('start_date', $value) }}" @else value="{{old('start_date')}}" @endif />

                        <!--end::Input-->
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="form-group">
                        <label class="form-label fs-6 fw-bolder text-dark">جستجو بر اساس تاریخ پایان
                        </label>
                        <!--begin::Input-->
                        {{-- @include('admin.__components.datepicker', ['name' => "end_date"]) --}}
                        <input type="text" class="form-control" id="end_date" name="end_date"
                            placeholder="{{ isset($placeholder) ? $placeholder : "تاریخ" }}"
                            @if(isset($value)) value="{{ old('end_date', $value) }}" @else value="{{old('end_date')}}" @endif />
                        <!--end::Input-->
                    </div>
                </div>

            </div>
            <br/>
            <div class="d-flex  flex-end">
                <a href="{{route('admin.projects.all')}}" class="btn btn-sm btn-light-danger mx-1">
                    <i class="fa fa-eraser p-0 m-0"></i>
                    حذف فیلترها
                </a>
                <button type="submit" class="btn btn-sm btn-light-primary mx-1">
                    <i class="fa fa-search"></i>
                    فیلتر
                </button>
            </div>
        </div>
        </form>
    </div>

    <!--begin::Row-->
    <div class="row g-6 g-xl-9">
        @if($projects->count() > 0)
            @foreach ($projects as $project)
                <!--begin::Col-->
                <div class="col-md-6 col-xl-4">
                    <!--begin::Card-->
                    <a href="{{ route('admin.project.bord-project.all', $project) }}" class="card border-hover-primary">
                        <!--begin::Card header-->
                        <div class="card-header border-0 pt-9">
                            <!--begin::Card Title-->
                            <div class="card-title m-0">
                                <!--begin::Avatar-->
                                <div class="symbol symbol-50px w-50px bg-light">
                                    <img src="{{ $project->webPresent()->image }}" alt="image" class="p-3" />
                                </div>
                                <!--end::Avatar-->
                            </div>
                            <!--end::Car Title-->
                            <!--begin::Card toolbar-->
                            <div class="card-toolbar">
                                {!! $project->webPresent()->status !!}
                            </div>
                            <!--end::Card toolbar-->
                        </div>
                        <!--end:: Card header-->
                        <!--begin:: Card body-->
                        <div class="card-body p-9">
                            <!--begin::Name-->
                            <div class="fs-3 fw-bold text-dark">{{ $project->title }}</div>
                            <!--end::Name-->
                            <!--begin::Description-->
                            <p class="text-gray-400 fw-semibold fs-5 mt-1 mb-7">{{ Str::limit($project->description, 80) }}</p>
                            <!--end::Description-->
                            <!--begin::Info-->
                            <div class="d-flex flex-wrap mb-5">
                                <!--begin::Due-->
                                <div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-7 mb-3">
                                    <div class="fs-6 text-gray-800 fw-bold">{{ jdate($project->start_date)->format('Y-m-d') }}</div>
                                    <div class="fw-semibold text-gray-400">تاریخ شروع پروژه</div>
                                </div>
                                <!--end::Due-->
                                <!--begin::Budget-->
                                <div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 mb-3">
                                    <div class="fs-6 text-gray-800 fw-bold">{{ jdate($project->end_date)->format('Y-m-d') }}</div>
                                    <div class="fw-semibold text-gray-400">تاریخ پایان پروژه</div>
                                </div>
                                <!--end::Budget-->
                            </div>
                            <!--end::Info-->
                            <!--begin::Progress-->
                            <div class="h-4px w-100 bg-light mb-5" data-bs-toggle="tooltip" title="از این پروژه 60% انجام شده">
                                <div class="bg-info rounded h-4px" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <!--end::Progress-->
                            <!--begin::Users-->
                            <div class="symbol-group symbol-hover">
                                @foreach ( $project->admins as $admin)
                                    @if (is_null($admin->image) || $admin->image == "")
                                        <!--begin::User-->
                                        <div class="symbol symbol-35px symbol-circle" data-bs-toggle="tooltip" title="{{$admin->webPresent()->fullName}}">
                                            <span class="symbol-label bg-warning text-inverse-warning fw-bold">N</span>
                                        </div>
                                    @else
                                    {{-- aria-label="{{$admin->webPresent()->fullName}}" data-bs-original-title="{{$admin->webPresent()->fullName}}" --}}
                                        <!--begin::User-->
                                        <div class="symbol symbol-35px symbol-circle" data-bs-toggle="tooltip" title="{{$admin->webPresent()->fullName}}">
                                            <img alt="Pic" src="{{ $admin->webPresent()->image }}">
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <!--end::Users-->
                        </div>
                        <!--end:: Card body-->
                    </a>
                    <!--end::Card-->
                </div>
                <!--end::Col-->
            @endforeach
        @else
            <div colspan="6" style="text-align:center;color:red">
                <a class="btn btn-danger" href="">
                    اطلاعات موجود نیست<i class="icon-warning2 mr-3 icon-1x"></i>
                </a>
            </div>
        @endif
    </div>
    <!--end::Row-->



@endsection
@section('scripts')
    @include('admin.message.alert')
    <script src="{{asset('admin-assets/js/persian-datepicker.min.js')}}"></script>
    <script src="{{asset('admin-assets/js/persian-date.min.js')}}"></script>
    <script>
        $('#remove-empty-values').submit(function () {
            $(this).find(':input').filter(function () {
                return !this.value;
            }).attr('disabled', 'disabled');
            return true;
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#start_date").pDatepicker({
                initialValue : false,
                format: 'L',
                // defaultDate: null
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#end_date").pDatepicker({
                initialValue : false,
                format: 'L',
                // defaultDate: null
            });
        });
    </script>
    <script>

        $('.delete-confirm').on('click', function (event) {
            event.preventDefault();
            let admin = $(this).data("id");
            let url = "{{ route('admin.phonebooks.logicalDeletion',":admin") }}";
            url = url.replace(":admin",admin);

            Swal.fire({
                title: 'حذف  !',
                text: "آیا مطمئن هستید ؟",
                icon: "error",
                showCancelButton: true,
                confirmButtonColor: 'red',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'بله,حذف کن',
                cancelButtonText: 'خیر',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    window.location.href = url;
                }
            });
        });
    </script>
@endsection
