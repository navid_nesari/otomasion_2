@extends('layouts.admin.admin')

@section('title',' مدیریت پروژه ها  ')
@section('pageTitle','لیست پروژه ها')
@section('content')
    <a href="{{route('admin.projects.all')}}" class="btn btn-sm btn-light-primary w-150px me-2">
        نمایش به صورت دیگر
    </a>
    <a href="{{route('admin.projects.create')}}" class="btn btn-sm btn-light-success me-2">
        ثبت پروژه جدید
    </a>
    <div class="d-flex flex-end mb-5">

        <button type="button" class="btn btn-primary px-4 py-2" data-bs-toggle="collapse"
                href="#student_filters" role="button"
                aria-expanded="false" aria-controls="collapseExample" style="margin-left: 5px!important;">
            فیلتر
            <i class="fa fa-filter p-0 m-0"></i>
        </button>
    </div>

    <div class="card mb-5 pb-3 mb-xl-8 collapse @if(request()->has('title')||request()->has('status')||request()->has('start_date')||request()->has('end_date') ) show @else @endif" id="student_filters">
        <form action="" class="form remove-empty-values" method="get" id="remove-empty-values">
        <!--begin::Header-->
        <div class="card-header border-0 pt-5">
            <h3 class="card-title align-items-start flex-column">
                    <span class="card-label fw-bolder fs-3 mb-1">
                        <i class="fa fa-search text-white pl-1"></i>
                        جستجوی پیشرفته
                    </span>
            </h3>
        </div>
        <div class="card-body py-3">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="form-group">
                        <label class="form-label fs-6 fw-bolder text-dark">جستجو بر اساس عنوان
                        </label>
                        <input class="form-control form-control-lg form-control-solid" name="title"
                               placeholder=""
                               value="{{ request()->has('title') ? request()->get('title') : null }}"/>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="form-group">
                        <label class="form-label fs-6 fw-bolder text-dark">جستجو بر اساس وضعیت</label>
                        <!--begin::Input-->
                        <select class="form-control" name="status">
                            <option selected="true" disabled="disabled">انتخاب کنید</option>
                            @foreach($activityStatuses as $activityStatuse)
                                <option value="{{ $activityStatuse['id'] }}">{{ $activityStatuse['title'] }}</option>
                            @endforeach
                        </select>
                        <!--end::Input-->
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="form-group">
                        <label class="form-label fs-6 fw-bolder text-dark">جستجو بر اساس تاریخ شروع
                        </label>
                        <!--begin::Input-->
                        {{-- @include('admin.__components.datepicker', ['name' => "start_date"]) --}}
                        <input type="text" class="form-control" id="start_date" name="start_date"
                            placeholder="{{ isset($placeholder) ? $placeholder : "تاریخ" }}"
                            @if(isset($value)) value="{{ old('start_date', $value) }}" @else value="{{old('start_date')}}" @endif />

                        <!--end::Input-->
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="form-group">
                        <label class="form-label fs-6 fw-bolder text-dark">جستجو بر اساس تاریخ پایان
                        </label>
                        <!--begin::Input-->
                        {{-- @include('admin.__components.datepicker', ['name' => "end_date"]) --}}
                        <input type="text" class="form-control" id="end_date" name="end_date"
                            placeholder="{{ isset($placeholder) ? $placeholder : "تاریخ" }}"
                            @if(isset($value)) value="{{ old('end_date', $value) }}" @else value="{{old('end_date')}}" @endif />
                        <!--end::Input-->
                    </div>
                </div>

            </div>
            <br/>
            <div class="d-flex  flex-end">
                <a href="{{route('admin.projects.all')}}" class="btn btn-sm btn-light-danger mx-1">
                    <i class="fa fa-eraser p-0 m-0"></i>
                    حذف فیلترها
                </a>
                <button type="submit" class="btn btn-sm btn-light-primary mx-1">
                    <i class="fa fa-search"></i>
                    فیلتر
                </button>
            </div>
        </div>
        </form>
    </div>

    <div class="card">
        <div class="card-header border-0 pt-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label fw-bolder fs-3 mb-1"> لیست پروژه ها </span>
            </h3>
        </div>
        <!--begin::Card body-->
        <div class="card-body pt-0">
            <!--begin::Table-->
            <div id="kt_customers_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <div class="table-responsive">
                    <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
                           id="kt_customers_table">
                        <!--begin::Table head-->
                        <thead>
                        <!--begin::Table row-->
                        <tr class="min-w-125px sorting">
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="Customer Name: activate to sort column ascending"> عنوان
                            </th>
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="IP Address: activate to sort column ascending"> تاریخ شروع
                            </th>
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="Customer Name: activate to sort column ascending"> تاریخ پایان
                            </th>
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="Customer Name: activate to sort column ascending">مشارکت کننده گان
                            </th>
                            <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1"
                                colspan="1" style="width: 162.9px;"
                                aria-label="Customer Name: activate to sort column ascending"> وضعیت
                            </th>
                            <th class="min-w-125px sorting" rowspan="1" colspan="1"
                                style="width: 162.9px;" aria-label="Actions">تنظیمات
                            </th>
                        </tr>
                        <!--end::Table row-->
                        </thead>
                        <!--end::Table head-->
                        <!--begin::Table body-->
                        <tbody class="fw-bold text-gray-600">
                        @if($projects->count() > 0)
                            @foreach($projects as $project)
                                <tr class="odd">
                                    <td>
                                        <a href="#" class="text-gray-600 text-hover-primary mb-1">
                                            {{$project->title}}
                                        </a>
                                    </td>
                                    <td>{{ jdate($project->start_date)->format('Y-m-d') }}</td>
                                    <td>{{ jdate($project->end_date)->format('Y-m-d') }}</td>
                                    <td>
                                        <!--begin::Users-->
                                        <div class="symbol-group symbol-hover mb-3">
                                            <!--begin::User-->
                                            @foreach ( $project->admins as $admin)
                                                @if (is_null($admin->image) || $admin->image == "")
                                                    <!--begin::User-->
                                                    <div class="symbol symbol-35px symbol-circle" data-bs-toggle="tooltip" title="{{$admin->webPresent()->fullName}}">
                                                        <img alt="Pic" src="{{ $admin->webPresent()->image }}">
                                                    </div>
                                                @else
                                                {{-- aria-label="{{$admin->webPresent()->fullName}}" data-bs-original-title="{{$admin->webPresent()->fullName}}" --}}
                                                    <!--begin::User-->
                                                    <div class="symbol symbol-35px symbol-circle" data-bs-toggle="tooltip" title="{{$admin->webPresent()->fullName}}">
                                                        <img alt="Pic" src="{{ $admin->webPresent()->image }}">
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                        <!--end::Users-->
                                    </td>
                                    <td>{!! $project->webPresent()->status !!}</td>
                                    <!--begin::Action=-->
                                    <td class="">
                                        <a href="{{ route('admin.project.bord-project.all', ['project' => $project]) }}"
                                           class="btn btn-icon btn-light-info btn-sm me-1">
                                             <span class="svg-icon svg-icon-3">
                                                <i class="fa fa-layer-group"></i>
                                            </span>
                                        </a>

                                    </td>
                                    <!--end::Action=-->
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" style="text-align:center;color:red">
                                    <a class="btn btn-danger" href="">
                                       اطلاعات موجود نیست<i class="icon-warning2 mr-3 icon-1x"></i>
                                    </a>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                        <!--end::Table body-->
                    </table>
                </div>

            </div>
            <!--end::Table-->
            {{-- {{$projects->links('vendor.pagination.bootstrap-4') }} --}}
        </div>
        <!--end::Card body-->
    </div>

@endsection
@section('scripts')
    @include('admin.message.alert')
    <script>
        $('#remove-empty-values').submit(function () {
            $(this).find(':input').filter(function () {
                return !this.value;
            }).attr('disabled', 'disabled');
            return true;
        });
    </script>

@endsection
