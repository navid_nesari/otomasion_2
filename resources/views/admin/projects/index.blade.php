<!--begin::Navbar-->
<div class="card mb-9">
    <div class="card-body pt-9 pb-0">
        <!--begin::Details-->
        <div class="d-flex flex-wrap flex-sm-nowrap mb-6">
            <!--begin::Image-->
            <div class="d-flex flex-center flex-shrink-0 bg-light rounded w-100px h-100px w-lg-150px h-lg-150px me-7 mb-4">
                <img class="mw-50px mw-lg-75px" src="{{ $project->webPresent()->image }}" alt="image" />
            </div>
            <!--end::Image-->
            <!--begin::Wrapper-->
            <div class="flex-grow-1">
                <!--begin::Head-->
                <div class="d-flex justify-content-between align-items-start flex-wrap position-relative mb-2">
                    <!--begin::Details-->
                    <div class="d-flex flex-column">
                        <!--begin::Status-->
                        <div class="d-flex align-items-center mb-1">
                            <a href="#" class="text-gray-800 text-hover-primary fs-2 fw-bold me-3">{{ $project->title }}</a>
                            <span>{!! $project->webPresent()->status !!}</span>
                        </div>
                        <!--end::Status-->
                        <!--begin::Description-->
                        <div class="d-flex flex-wrap fw-semibold mb-4 mt-4 fs-5 text-gray-400">{{ $project->description }}</div>
                        <!--end::Description-->
                    </div>
                    <!--end::Details-->
                    <!--begin::Actions-->
                    <div class="d-flex position-absolute top-4 end-0 mb-4">
                        <a href="{{route('admin.projects.all')}}" class="btn btn-sm btn-light-success min-w-125px me-2">
                            برگشت
                        </a>
                        <a href="{{ route('admin.projects.users-project', $project) }}" class="btn btn-sm btn-bg-light btn-active-color-primary me-2" >کاربران</a>
                        <a href="{{ route('admin.projects.edit', $project) }}" class="btn btn-sm btn-primary me-2">تنظیمات</a>
                    </div>
                    <!--end::Actions-->
                </div>
                <!--end::Head-->
                <!--begin::Info-->
                <div class="d-flex flex-wrap justify-content-start">
                    <!--begin::Stats-->
                    <div class="d-flex flex-wrap">
                        <!--begin::Stat-->
                        <div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
                            <!--begin::Number-->
                            <div class="d-flex align-items-center">
                                <div class="fs-4 fw-bold">{{ jdate($project->start_date)->format('Y-m-d') }}</div>
                            </div>
                            <!--end::Number-->
                            <!--begin::Label-->
                            <div class="fw-semibold fs-6 text-gray-400">تاریخ شروع پروژه</div>
                            <!--end::Label-->
                        </div>
                        <!--end::Stat-->
                        <!--begin::Stat-->
                        <div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
                            <!--begin::Number-->
                            <div class="d-flex align-items-center">
                                <div class="fs-4 fw-bold">{{ jdate($project->end_date)->format('Y-m-d') }}</div>
                            </div>
                            <!--end::Number-->
                            <!--begin::Label-->
                            <div class="fw-semibold fs-6 text-gray-400">تاریخ پایان پروژه</div>
                            <!--end::Label-->
                        </div>
                        <!--end::Stat-->

                    </div>
                    <!--end::Stats-->
                    <!--begin::Users-->
                    <div class="symbol-group symbol-hover mb-3">
                        <!--begin::User-->
                        @foreach ( $project->admins as $admin)
                            @if (is_null($admin->image) || $admin->image == "")
                                <!--begin::User-->
                                <div class="symbol symbol-35px symbol-circle" data-bs-toggle="tooltip" title="{{$admin->webPresent()->fullName}}">
                                    <img alt="Pic" src="{{ $admin->webPresent()->image }}">
                                </div>
                            @else
                            {{-- aria-label="{{$admin->webPresent()->fullName}}" data-bs-original-title="{{$admin->webPresent()->fullName}}" --}}
                                <!--begin::User-->
                                <div class="symbol symbol-35px symbol-circle" data-bs-toggle="tooltip" title="{{$admin->webPresent()->fullName}}">
                                    <img alt="Pic" src="{{ $admin->webPresent()->image }}">
                                </div>
                            @endif
                        @endforeach

                    </div>
                    <!--end::Users-->
                </div>
                <!--end::Info-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Details-->
        <div class="separator"></div>
        <!--begin::Nav-->
        <ul class="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bold">
            <!--begin::Nav item-->
            {{-- <li class="nav-item">
                <a class="nav-link text-active-primary py-5 me-6 @if ( isset($active) && $active == 'edit' ) active @endif"
                href=""></a>
            </li> --}}
            <!--end::Nav item-->
            <!--begin::Nav item-->
            <li class="nav-item">
                <a class="nav-link text-active-primary py-5 me-6  @if ( isset($active) && $active == 'bord' ) active @endif"
                href="{{ route('admin.project.bord-project.all', $project) }}">بورد پروژه</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary py-5 me-6  @if ( isset($active) && $active == 'duties' ) active @endif"
                   href="{{ route('admin.project-duties.all', $project) }}">وظایف</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary py-5 me-6  @if ( isset($active) && $active == 'group-chat' ) active @endif"
                   href="{{ route('admin.project.group-chat.all', $project) }}">گروه پروژه </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary py-5 me-6  @if ( isset($active) && $active == 'nemodar' ) active @endif"
                   href="{{ route('admin.project.nemodar.all', $project) }}">نمودار پروژه </a>
            </li>
            <!--end::Nav item-->
        </ul>
        <!--end::Nav-->
    </div>
</div>
<!--end::Navbar-->
