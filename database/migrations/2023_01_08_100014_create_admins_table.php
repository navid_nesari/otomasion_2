<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Constants\Constant;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('username')->unique();
            $table->string('father_name')->nullable();
            $table->string('birth_certificate_number')->unique()->comment('شماره شناسنامه')->nullable();
            $table->string('birth_date')->comment('تاریخ تولد')->nullable();
            $table->string('issued_location')->comment('محل صدور')->nullable();
            $table->string('religion')->comment('دین')->nullable();
            $table->string('religious_branch')->comment('مذهب')->nullable();
            $table->string('nationality')->comment('ملیت')->nullable();
            $table->string('postal_code')->comment('کد پستی')->nullable();
            $table->text('address')->nullable();
            $table->string('mobile')->unique()->nullable();
            $table->string('email')->nullable()->nullable();
            $table->string('password');
            $table->enum('marital_status', [
                Constant::SINGLE, Constant::MARRIED,
                Constant::ENGAGEMENT, Constant::DIVORCED,
                Constant::DECEASED_SPOUSE
            ])->default(Constant::SINGLE)->comment('وضعیت تاهل');
            $table->string('children_number')->nullable();
            $table->enum('military_service', [
                Constant::INCLUDED,
                Constant::IN_MILITARY_SERVICE,
                Constant::EXEMPT
            ])->default(Constant::INCLUDED)->comment('خدمت سربازی');
            $table->text('details_military_location')->nullable()->comment('مشخصات محل خدمت')->nullable();
            $table->string('until_date')->nullable()->comment('تاریخ پایان خدمت')->nullable();
            $table->string('exemption_type')->nullable()->comment('نوع معافیت')->nullable();
            $table->string('medical_exemption_type')->nullable()->comment('نوع معافیت پزشکی')->nullable();
            $table->string('last_educational_certificate')->comment('آخرین مدرک تحصیلی')->nullable();
            $table->string('study_field')->comment('رشته تحصیلی')->nullable();
            $table->string('education_place')->comment('محل تحصیل')->nullable();
            $table->json('professional_documents')->comment('مدارک حرفه ای')->nullable();
            $table->json('foreign_language_proficiency')->comment('تسلط به زبان خارجی')->nullable();
            $table->json('resume')->comment('سوابق کاری')->nullable();
            $table->enum('housing_situation',[
                Constant::PERSONAL_HOUSE,
                Constant::LIVING_WITH_PARENTS,
                Constant::TENANT
            ])->default(Constant::PERSONAL_HOUSE)->comment('وضعیت مسکن');
            $table->string('rent_paid')->nullable()->comment('مبلغ اجاره پرداختی')->nullable();
            $table->json('guarantor')->comment('معرف یا ضامن')->nullable();
            $table->enum('second_job',[
                Constant::I_HAVE,
                Constant::I_HAVENT,
                Constant::RETIRED
            ])->default(Constant::I_HAVE)->comment('شغل دوم');
            $table->string('organization')->nullable()->comment('شاغل در سازمان')->nullable();
            $table->string('second_job_type')->nullable()->comment('نوع شغل دوم')->nullable();
            $table->string('salary_amount')->nullable()->comment('میزان حقوق')->nullable();
            $table->string('working_hours')->nullable()->comment('ساعات کاری')->nullable();
            $table->string('type_work_shift')->nullable()->comment('نوع شیفت کاری')->nullable();
            $table->string('work_address')->nullable()->comment('آدرس محل کار')->nullable();
            $table->string('work_phone')->nullable()->comment('تلفن محل کار')->nullable();
            $table->enum('status', [
                Constant::ACTIVE,
                Constant::IN_ACTIVE
            ])->default(Constant::ACTIVE);
            $table->string('image')->nullable();
            $table->string('birth_certificate_image')->nullable();
            $table->string('national_card_image')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
