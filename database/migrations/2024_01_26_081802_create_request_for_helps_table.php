<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestForHelpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_for_helps', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('admin_id');
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade')->cascadeOnUpdate();
            $table->string('title');
            $table->string('price');
            $table->dateTime('date');
            $table->string('answer')->nullable();
            $table->enum('status',[\App\Constants\Constant::REJECTED,\App\Constants\Constant::ACCEPTED,\App\Constants\Constant::AWAITING_CONFIRMATION])->default(\App\Constants\Constant::AWAITING_CONFIRMATION);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_for_helps');
    }
}
