<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('missions', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('admin_id');
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade')->cascadeOnUpdate();
            $table->string('full_name');
            $table->string('part');
            $table->string('mission');
            $table->dateTime('date');
            $table->string('date_start');
            $table->string('date_end');
            $table->string('answer')->nullable();
            $table->enum('status',[\App\Constants\Constant::REJECTED,\App\Constants\Constant::ACCEPTED,\App\Constants\Constant::AWAITING_CONFIRMATION])->default(\App\Constants\Constant::AWAITING_CONFIRMATION);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('missions');
    }
}
