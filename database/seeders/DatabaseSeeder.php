<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\Category;
use App\Models\Phonebook;
use App\Models\Project;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Admin::create([
            'first_name' => 'محمد',
            'last_name' => 'رضایی',
            'mobile' => '09054312425',
            'username' => 'admin',
            'father_name' => 'jhk',
            'birth_certificate_number' => '05566323234565',
            'birth_date' => '75/01/29',
            'issued_location' => 'hjgj',
            'religion' => 'hjgj',
            'religious_branch' => 'hjgj',
            'nationality' => 'hjgj',
            'postal_code' => 'hjgj',
            'address' => 'hjgjvdsdf srgd dgd',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'last_educational_certificate' => 'hjgj',
            'study_field' => 'hjgj',
            'education_place' => 'hjgj',
            'professional_documents' => json_encode([
                "house",
                "flat"
             ]),
            'foreign_language_proficiency' => json_encode(['hjgj']),
            'resume' => json_encode(['hjgj']),
            'guarantor' => json_encode(['hjgj']),
        ]);

        Category::create([
            'title' => 'دسته بندی 1',
            'description' => 'لبتل بلاتب بی بلالت لتل بلتل ب',
        ]);

        Phonebook::create([
            'admin_id' => 1,
            'full_name' => 'نوید نثاری' ,
            'address' => 'لاتبل لات لبت یبا قثقف ',
            'mobile' => '09363369392',
            'phone' => '09124567887',
            'account_number' => '8556636699555444',
            'description' => 'لتلب لت لت ا ثق ف قابی یبسقلی ثثقفلی سغف',
        ]);

        Project::create([
            'title' => 'فیتنس',
            'description' => 'فیتنس یک شاخه ورزشی از بدنسازی است',
            'start_date' => '2023-12-16 00:00:00',
            'end_date' => '2023-12-19 00:00:00',
        ]);
    }
}
